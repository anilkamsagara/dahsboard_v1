﻿using System.Globalization;
using Dashboard.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dashboard
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                Title = "Home";
                Session.Clear();
                if (!string.IsNullOrEmpty(Context.User.Identity.Name.ToUpper(CultureInfo.InvariantCulture)))
                {
                    RedirectRoleBasedLogin();
                }

            }
        }

        private void RedirectRoleBasedLogin()
        {
            var userController = new UserController();
            var userDetail = userController.Get(Context.User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            switch (userDetail.Role)
            {
                case Global.EmployeeType.BusinessAssociate:
                    Response.Redirect("~/Account/BA");
                    break;
                case Global.EmployeeType.StatetHead:
                    Response.Redirect("~/Account/SH");
                    break;
                case Global.EmployeeType.DistrictHead:
                    Response.Redirect("~/Account/DH");
                    break;
                case Global.EmployeeType.SuperAdmin:
                    Response.Redirect("~/Account/SA");
                    break;
            }
        }
        private string LoadImages(string imageUrl)
        {
            var imageList = new List<string>
            {
                "~/Images/truck.png",
                "~/Images/truck.png",
                "~/Images/truck.png"
            };
            //if (string.IsNullOrEmpty(imageUrl))
            //{
            return imageList.FirstOrDefault();
            //}
            //foreach (var image in imageList.Where(image => !image.Contains(imageUrl)))
            //{
            //    return image;
            //}
        }
    }
}