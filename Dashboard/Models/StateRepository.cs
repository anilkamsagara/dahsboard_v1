﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data.Entity.Migrations;

namespace Dashboard.Models
{
    public class StateRepository
    {
        private List<StateDetail> _stateDetails = new List<StateDetail>();

        public IEnumerable<StateDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _stateDetails = gatiManagementEntities.StateDetails.ToList();
            }
            return _stateDetails;
        }
        public string Create(StateDetail stateDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _stateDetails = gatiManagementEntities.StateDetails.ToList();
                stateDetail.StateId = int.Parse(_stateDetails.Count.ToString(CultureInfo.InvariantCulture));
                gatiManagementEntities.StateDetails.Add(stateDetail);
                gatiManagementEntities.SaveChanges();
            }
            _stateDetails.Add(stateDetail);
            return "District Details added";
        }
        public StateDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _stateDetails = gatiManagementEntities.StateDetails.ToList();
            }
            var stateDetail = _stateDetails.FirstOrDefault(x => x.StateId == id);
            return stateDetail;
        }
        public bool Update(StateDetail districtDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _stateDetails = gatiManagementEntities.StateDetails.ToList();

                var stateDetail = _stateDetails.Find(x => x.StateId == districtDetail.StateId);
                if (stateDetail == null) return true;
                gatiManagementEntities.StateDetails.AddOrUpdate(districtDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public StateDetail GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _stateDetails = gatiManagementEntities.StateDetails.ToList();
            }
            var stateDetail = _stateDetails.FirstOrDefault(x => x.StateId == id);
            return stateDetail;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var districtDetail = gatiManagementEntities.StateDetails.FirstOrDefault(x => x.StateId == id);
                gatiManagementEntities.StateDetails.Remove(districtDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}