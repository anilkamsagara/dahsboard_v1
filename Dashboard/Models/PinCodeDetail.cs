namespace Dashboard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PinCodeDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PinCodeId { get; set; }

        [StringLength(50)]
        public string PinCode { get; set; }

        [StringLength(50)]
        public string PinCodeLocation { get; set; }

        public int RTOId { get; set; }

        public virtual RTODetail RTODetail { get; set; }
    }
}
