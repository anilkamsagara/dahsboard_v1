namespace Dashboard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BankDetail
    {
        [Key]
        [StringLength(20)]
        public string BankId { get; set; }

        [StringLength(50)]
        public string AccountNumber { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(50)]
        public string MICRCode { get; set; }

        [StringLength(50)]
        public string IFSCCode { get; set; }

        public int UserId { get; set; }

        public virtual UserDetail UserDetail { get; set; }
    }
}
