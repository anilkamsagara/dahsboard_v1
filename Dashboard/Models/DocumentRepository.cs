﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;

namespace Dashboard.Models
{
    public class DocumentRepository 
    {
        private List<DocumentDetail> _documentDetails = new List<DocumentDetail>();

        public IEnumerable<DocumentDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _documentDetails = gatiManagementEntities.DocumentDetails.ToList();
            }
            return _documentDetails;
        }
        public string Create(DocumentDetail documentDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _documentDetails = gatiManagementEntities.DocumentDetails.ToList();
                documentDetail.DocumentId = _documentDetails.Count.ToString(CultureInfo.InvariantCulture); 
                gatiManagementEntities.DocumentDetails.Add(documentDetail);
                gatiManagementEntities.SaveChanges();
            }
            _documentDetails.Add(documentDetail);
            return "Document Details added";
        }

        public bool Update(DocumentDetail documentDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _documentDetails = gatiManagementEntities.DocumentDetails.ToList();
                var personalDetails = _documentDetails.Find(x => x.DocumentId == documentDetail.DocumentId);
                if (personalDetails == null) return true;
                gatiManagementEntities.DocumentDetails.AddOrUpdate(documentDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public DocumentDetail GetById(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _documentDetails = gatiManagementEntities.DocumentDetails.ToList();
            }
            var bankDetail = _documentDetails.FirstOrDefault(bank => bank.DocumentId == id);
            return bankDetail;
        }

        public DocumentDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _documentDetails = gatiManagementEntities.DocumentDetails.ToList();
            }
            var bankDetail = _documentDetails.FirstOrDefault(bank => bank.UserId == id);
            return bankDetail;
        }

        public void Delete(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var bankDetail = gatiManagementEntities.DocumentDetails.FirstOrDefault(bank => bank.DocumentId == id);
                gatiManagementEntities.DocumentDetails.Remove(bankDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}