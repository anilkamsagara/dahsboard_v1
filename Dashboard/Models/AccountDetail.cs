namespace Dashboard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AccountDetail
    {
        [Key]
        [StringLength(20)]
        public string AccountId { get; set; }

        [StringLength(50)]
        public string Funds { get; set; }

        [StringLength(50)]
        public string Bank { get; set; }

        public int UserId { get; set; }

        public virtual UserDetail UserDetail { get; set; }
    }
}
