namespace Dashboard.Models
{
    using System.Data.Entity;

    public partial class DatabaseModel : DbContext
    {
        public DatabaseModel()
            : base("name=DatabaseModel")
        {
        }

        public virtual DbSet<AccountDetail> AccountDetails { get; set; }
        public virtual DbSet<BankDetail> BankDetails { get; set; }
        public virtual DbSet<BanksInIndia> BanksInIndias { get; set; }
        public virtual DbSet<CommissionDetail> CommissionDetails { get; set; }
        public virtual DbSet<CompanyDetail> CompanyDetails { get; set; }
        public virtual DbSet<CountryDetail> CountryDetails { get; set; }
        public virtual DbSet<DistrictDetail> DistrictDetails { get; set; }
        public virtual DbSet<DocumentDetail> DocumentDetails { get; set; }
        public virtual DbSet<PartnerDetail> PartnerDetails { get; set; }
        public virtual DbSet<PersonalDetail> PersonalDetails { get; set; }
        public virtual DbSet<PinCodeDetail> PinCodeDetails { get; set; }
        public virtual DbSet<RTODetail> RTODetails { get; set; }
        public virtual DbSet<StateDetail> StateDetails { get; set; }
        public virtual DbSet<TicketDetail> TicketDetails { get; set; }
        public virtual DbSet<TonnageDetail> TonnageDetails { get; set; }
        public virtual DbSet<UserDetail> UserDetails { get; set; }
        public virtual DbSet<VehicleLocationDetail> VehicleLocationDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyDetail>()
                .HasMany(e => e.PartnerDetails)
                .WithRequired(e => e.CompanyDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CountryDetail>()
                .HasMany(e => e.StateDetails)
                .WithRequired(e => e.CountryDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DistrictDetail>()
                .HasMany(e => e.RTODetails)
                .WithRequired(e => e.DistrictDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RTODetail>()
                .HasMany(e => e.PinCodeDetails)
                .WithRequired(e => e.RTODetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StateDetail>()
                .HasMany(e => e.DistrictDetails)
                .WithRequired(e => e.StateDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TonnageDetail>()
                .HasMany(e => e.TicketDetails)
                .WithRequired(e => e.TonnageDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.AccountDetails)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.BankDetails)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.CommissionDetails)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.CompanyDetails)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.DocumentDetails)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.PartnerDetails)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.PersonalDetails)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.TicketDetails)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.VehicleLocationDetails)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);
        }
    }
}
