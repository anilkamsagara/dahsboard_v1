namespace Dashboard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TicketDetail
    {
        [Key]
        [StringLength(20)]
        public string TicketId { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        [StringLength(50)]
        public string CubicCapacity { get; set; }

        [StringLength(20)]
        public string Length { get; set; }

        [StringLength(20)]
        public string Width { get; set; }

        [StringLength(20)]
        public string Breadth { get; set; }

        [StringLength(20)]
        public string Height { get; set; }

        [StringLength(50)]
        public string Registration { get; set; }

        [StringLength(50)]
        public string EngineNumber { get; set; }

        [StringLength(50)]
        public string ChasisNumber { get; set; }

        [StringLength(50)]
        public string NOCRTO { get; set; }

        [StringLength(50)]
        public string NOCPolicy { get; set; }

        [StringLength(50)]
        public string Photo { get; set; }

        [StringLength(50)]
        public string PhotoFileName { get; set; }

        [StringLength(10)]
        public string PhotoType { get; set; }

        public byte[] PhotoContent { get; set; }

        [StringLength(50)]
        public string FCValidityFrom { get; set; }

        [StringLength(50)]
        public string FCValidity { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string District { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(50)]
        public string VehicleStatus { get; set; }

        [StringLength(50)]
        public string DriverName { get; set; }

        public int? DriverAge { get; set; }

        [StringLength(50)]
        public string DriverAddress { get; set; }

        [StringLength(50)]
        public string DriverCity { get; set; }

        [StringLength(50)]
        public string DriverDistrict { get; set; }

        [StringLength(50)]
        public string DriverState { get; set; }

        [StringLength(50)]
        public string DriverCountry { get; set; }

        [StringLength(50)]
        public string DriverDLNumber { get; set; }

        [StringLength(50)]
        public string DriverPhoto { get; set; }

        [StringLength(50)]
        public string DriverPhotoFileName { get; set; }

        [StringLength(10)]
        public string DriverPhotoType { get; set; }

        public byte[] DriverPhotoContent { get; set; }

        [StringLength(50)]
        public string DriverMobileNumber { get; set; }

        [StringLength(50)]
        public string DriverEmail { get; set; }

        [StringLength(50)]
        public string DriverPanCard { get; set; }

        [StringLength(50)]
        public string DriverAadharCardNumber { get; set; }

        [StringLength(50)]
        public string InsuranceType { get; set; }

        [StringLength(50)]
        public string InsuranceCompany { get; set; }

        [StringLength(50)]
        public string Validity { get; set; }

        [StringLength(50)]
        public string VehiclePhotoFront { get; set; }

        [StringLength(50)]
        public string VehiclePhotoBack { get; set; }

        [StringLength(50)]
        public string VehiclePhotoLeft { get; set; }

        [StringLength(50)]
        public string VehiclePhotoRight { get; set; }

        [StringLength(50)]
        public string ValidityFrom { get; set; }

        [StringLength(50)]
        public string BAPermission { get; set; }

        [StringLength(50)]
        public string BAComments { get; set; }

        [StringLength(50)]
        public string DHPermission { get; set; }

        [StringLength(50)]
        public string DHComments { get; set; }

        [StringLength(50)]
        public string SHPermission { get; set; }

        [StringLength(50)]
        public string SHComments { get; set; }

        [StringLength(50)]
        public string SAPermission { get; set; }

        [StringLength(50)]
        public string SAComments { get; set; }

        [StringLength(15)]
        public string TicketStatus { get; set; }

        [StringLength(25)]
        public string GoodsType { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CreatedDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TicketStartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TicketEndDate { get; set; }

        public int? RenewalCounter { get; set; }

        public int TonnageId { get; set; }

        public int UserId { get; set; }

        public virtual TonnageDetail TonnageDetail { get; set; }

        public virtual UserDetail UserDetail { get; set; }
    }
}
