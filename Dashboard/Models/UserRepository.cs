﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Dashboard.Models
{
    public class UserRepository 
    {
        private List<UserDetail> _userList = new List<UserDetail>();
        public IEnumerable<UserDetail> GetAllUsers()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _userList = gatiManagementEntities.UserDetails.ToList();
            }
            return _userList;
        }
        public string CreateUser(UserDetail user)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _userList = gatiManagementEntities.UserDetails.ToList();
                user.UserId = _userList.Count;
                gatiManagementEntities.UserDetails.Add(user);
                gatiManagementEntities.SaveChanges();
            }
            _userList.Add(user);
            return "User added";
        }
        public bool UpdateUser(UserDetail userDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _userList = gatiManagementEntities.UserDetails.ToList();
                var personalDetails = _userList.Find(x => x.Name == userDetail.Name);
                if (personalDetails == null) return true;
                gatiManagementEntities.UserDetails.AddOrUpdate(userDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }
        public UserDetail Get(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _userList = gatiManagementEntities.UserDetails.ToList();
            }
            var user = _userList.FirstOrDefault(users => users.Name == id);
            return user;
        }

        public UserDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _userList = gatiManagementEntities.UserDetails.ToList();
            }
            var user = _userList.FirstOrDefault(users => users.UserId == id);
            return user;
        }
        public void DeleteUser(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var user=gatiManagementEntities.UserDetails.FirstOrDefault(users => users.UserId == id);
                gatiManagementEntities.UserDetails.Remove(user);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}