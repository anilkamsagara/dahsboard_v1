﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Dashboard.Models
{
    public class PartnerRepository
    {
        private List<PartnerDetail> _partnerDetails = new List<PartnerDetail>();
        public IEnumerable<PartnerDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _partnerDetails = gatiManagementEntities.PartnerDetails.ToList();
            }
            return _partnerDetails;
        }

        public string Create(PartnerDetail partnerDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _partnerDetails = gatiManagementEntities.PartnerDetails.ToList();
                partnerDetail.PartnerId = (_partnerDetails.Count + 1).ToString();
                gatiManagementEntities.PartnerDetails.Add(partnerDetail);
                gatiManagementEntities.SaveChanges();
            }
            return "ticket Details added";
        }

        public PartnerDetail GetById(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _partnerDetails = gatiManagementEntities.PartnerDetails.ToList();
            }
            var partnerDetail = _partnerDetails.FirstOrDefault(x => x.PartnerId == id);
            return partnerDetail;
        }

        public PartnerDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _partnerDetails = gatiManagementEntities.PartnerDetails.ToList();
            }
            var partnerDetail = _partnerDetails.FirstOrDefault(x => x.PartnerId == id.ToString());
            return partnerDetail;
        }

        public void Delete(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var partnerDetails = gatiManagementEntities.PartnerDetails.FirstOrDefault(x => x.PartnerId == id);
                gatiManagementEntities.PartnerDetails.Remove(partnerDetails);
                gatiManagementEntities.SaveChanges();
            }
        }

        public bool Update(PartnerDetail ticketDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _partnerDetails = gatiManagementEntities.PartnerDetails.ToList();
                var partnerDetails = _partnerDetails.Find(x => x.PartnerId == ticketDetail.PartnerId);
                if (partnerDetails == null) return true;
                gatiManagementEntities.PartnerDetails.AddOrUpdate(ticketDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }
    }
}