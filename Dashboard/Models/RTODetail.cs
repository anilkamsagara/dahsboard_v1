namespace Dashboard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RTODetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RTODetail()
        {
            PinCodeDetails = new HashSet<PinCodeDetail>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RTOId { get; set; }

        [StringLength(50)]
        public string RTOCode { get; set; }

        [StringLength(50)]
        public string RTOLocation { get; set; }

        public int DistrictId { get; set; }

        public virtual DistrictDetail DistrictDetail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PinCodeDetail> PinCodeDetails { get; set; }
    }
}
