﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Dashboard.Models
{
    public class CompanyRepository 
    {
        private List<CompanyDetail> _companyDetails = new List<CompanyDetail>();
        public IEnumerable<CompanyDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _companyDetails = gatiManagementEntities.CompanyDetails.ToList();
            }
            return _companyDetails;
        }

        public string Create(CompanyDetail companyDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _companyDetails = gatiManagementEntities.CompanyDetails.ToList();
                companyDetail.CompanyId = _companyDetails.Count.ToString(CultureInfo.InvariantCulture);
                gatiManagementEntities.CompanyDetails.Add(companyDetail);
                gatiManagementEntities.SaveChanges();
            }
            return companyDetail.CompanyId;
        }

        public CompanyDetail GetById(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _companyDetails = gatiManagementEntities.CompanyDetails.ToList();
            }
            var companyDetail = _companyDetails.FirstOrDefault(company => company.CompanyId == id);
            return companyDetail;
        }

        public CompanyDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _companyDetails = gatiManagementEntities.CompanyDetails.ToList();
            }
            var companyDetail = _companyDetails.FirstOrDefault(company => company.UserId == id);
            return companyDetail;
        }

        public void Delete(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var companyDetail = gatiManagementEntities.CompanyDetails.FirstOrDefault(x => x.CompanyId == id);
                gatiManagementEntities.CompanyDetails.Remove(companyDetail);
                gatiManagementEntities.SaveChanges();
            }
        }

        public bool Update(CompanyDetail companyDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _companyDetails = gatiManagementEntities.CompanyDetails.ToList();
                var personalDetails = _companyDetails.Find(x => x.CompanyId == companyDetail.CompanyId);
                if (personalDetails == null) return true;
                gatiManagementEntities.CompanyDetails.AddOrUpdate(companyDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }
    }
}