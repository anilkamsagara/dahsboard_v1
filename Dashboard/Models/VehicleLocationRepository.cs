﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;

namespace Dashboard.Models
{
    public class VehicleLocationRepository
    {
        private List<VehicleLocationDetail> _vehicleLocationDetails = new List<VehicleLocationDetail>();

        public IEnumerable<VehicleLocationDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _vehicleLocationDetails = gatiManagementEntities.VehicleLocationDetails.ToList();
            }
            return _vehicleLocationDetails;
        }
        public string Create(VehicleLocationDetail vehiclelocationDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _vehicleLocationDetails = gatiManagementEntities.VehicleLocationDetails.ToList();
                vehiclelocationDetail.VehicleLocationId = _vehicleLocationDetails.Count.ToString(CultureInfo.InvariantCulture);
                gatiManagementEntities.VehicleLocationDetails.Add(vehiclelocationDetail);
                gatiManagementEntities.SaveChanges();
            }
            _vehicleLocationDetails.Add(vehiclelocationDetail);
            return "Vehicle location Details added";
        }

        public bool Update(VehicleLocationDetail vehiclelocationDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _vehicleLocationDetails = gatiManagementEntities.VehicleLocationDetails.ToList();

                var vehicleLocationDetails = _vehicleLocationDetails.Find(x => x.VehicleLocationId == vehiclelocationDetail.VehicleLocationId);
                if (vehicleLocationDetails == null) return true;
                gatiManagementEntities.VehicleLocationDetails.AddOrUpdate(vehiclelocationDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public VehicleLocationDetail GetById(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _vehicleLocationDetails = gatiManagementEntities.VehicleLocationDetails.ToList();
            }
            var vehicleLocationDetail = _vehicleLocationDetails.FirstOrDefault(x => x.VehicleLocationId == id);
            return vehicleLocationDetail;
        }

        public void Delete(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var vehicleLocationDetail = gatiManagementEntities.VehicleLocationDetails.FirstOrDefault(x => x.VehicleLocationId == id);
                gatiManagementEntities.VehicleLocationDetails.Remove(vehicleLocationDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}