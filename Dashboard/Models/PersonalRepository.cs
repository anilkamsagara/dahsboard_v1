﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;

namespace Dashboard.Models
{
    public class PersonalRepository
    {
        private List<PersonalDetail> _personalDetails = new List<PersonalDetail>();

        public IEnumerable<PersonalDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _personalDetails = gatiManagementEntities.PersonalDetails.ToList();
            }
            return _personalDetails;
        }
        public string Create(PersonalDetail personalDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _personalDetails = gatiManagementEntities.PersonalDetails.ToList();
                personalDetail.PersonalId = _personalDetails.Count.ToString(CultureInfo.InvariantCulture);
                gatiManagementEntities.PersonalDetails.Add(personalDetail);
                gatiManagementEntities.SaveChanges();
            }
            _personalDetails.Add(personalDetail);
            return "Personal Details added";
        }

        public bool Update(PersonalDetail personalDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _personalDetails = gatiManagementEntities.PersonalDetails.ToList();
                var personalDetails = _personalDetails.Find(x => x.PersonalId == personalDetail.PersonalId);
                if (personalDetails == null) return true;
                gatiManagementEntities.PersonalDetails.AddOrUpdate(personalDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public PersonalDetail GetById(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _personalDetails = gatiManagementEntities.PersonalDetails.ToList();
            }
            var user = _personalDetails.FirstOrDefault(x => x.PersonalId == id);
            return user;
        }

        public PersonalDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _personalDetails = gatiManagementEntities.PersonalDetails.ToList();
            }
            var user = _personalDetails.FirstOrDefault(x => x.UserId == id);
            return user;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var user = gatiManagementEntities.PersonalDetails.FirstOrDefault(users => users.UserId == id);
                gatiManagementEntities.PersonalDetails.Remove(user);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}