﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;

namespace Dashboard.Models
{
    public class BanksInIndiaRepository
    {
        private List<BanksInIndia> _banksInIndias = new List<BanksInIndia>();
        public IEnumerable<BanksInIndia> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _banksInIndias = gatiManagementEntities.BanksInIndias.ToList();
            }
            return _banksInIndias;
        }

        public string Create(BanksInIndia banksInIndia)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _banksInIndias = gatiManagementEntities.BanksInIndias.ToList();
                banksInIndia.Id = _banksInIndias.Count + 1;
                gatiManagementEntities.BanksInIndias.Add(banksInIndia);
                gatiManagementEntities.SaveChanges();
            }
            return banksInIndia.Id.ToString();
        }

        public BanksInIndia GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _banksInIndias = gatiManagementEntities.BanksInIndias.ToList();
            }
            var banksInIndia = _banksInIndias.FirstOrDefault(x => x.Id == id);
            return banksInIndia;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var companyDetail = gatiManagementEntities.BanksInIndias.FirstOrDefault(x => x.Id == id);
                gatiManagementEntities.BanksInIndias.Remove(companyDetail);
                gatiManagementEntities.SaveChanges();
            }
        }

        public bool Update(BanksInIndia banksInIndia)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _banksInIndias = gatiManagementEntities.BanksInIndias.ToList();
                var ticketDetails = _banksInIndias.Find(x => x.Id == banksInIndia.Id);
                if (ticketDetails == null) return true;
                gatiManagementEntities.BanksInIndias.AddOrUpdate(banksInIndia);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }
    }
}