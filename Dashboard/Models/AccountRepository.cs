﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data.Entity.Migrations;

namespace Dashboard.Models
{
    public class AccountRepository
    {
        private List<AccountDetail> _accountDetails = new List<AccountDetail>();

        public IEnumerable<AccountDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _accountDetails = gatiManagementEntities.AccountDetails.ToList();
            }
            return _accountDetails;
        }
        public string Create(AccountDetail accountDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _accountDetails = gatiManagementEntities.AccountDetails.ToList();
                accountDetail.AccountId = _accountDetails.Count.ToString(CultureInfo.InvariantCulture);
                gatiManagementEntities.AccountDetails.Add(accountDetail);
                gatiManagementEntities.SaveChanges();
            }
            _accountDetails.Add(accountDetail);
            return "Vehicle Driver Details added";
        }
        public AccountDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _accountDetails = gatiManagementEntities.AccountDetails.ToList();
            }
            var accountDetails = _accountDetails.FirstOrDefault(x => x.UserId == id);
            return accountDetails;
        }
        public bool Update(AccountDetail accountDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _accountDetails = gatiManagementEntities.AccountDetails.ToList();

                var vehicleDetails = _accountDetails.Find(x => x.AccountId == accountDetail.AccountId);
                if (vehicleDetails == null) return true;
                gatiManagementEntities.AccountDetails.AddOrUpdate(accountDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public AccountDetail GetById(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _accountDetails = gatiManagementEntities.AccountDetails.ToList();
            }
            var vehicleInsuranceDetail = _accountDetails.FirstOrDefault(x => x.AccountId == id);
            return vehicleInsuranceDetail;
        }

        public void Delete(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var vehicleDriverDetail = gatiManagementEntities.AccountDetails.FirstOrDefault(x => x.AccountId == id);
                gatiManagementEntities.AccountDetails.Remove(vehicleDriverDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}