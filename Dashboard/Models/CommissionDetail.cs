namespace Dashboard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CommissionDetail
    {
        [Key]
        [StringLength(20)]
        public string CommissionId { get; set; }

        [StringLength(50)]
        public string CommissionBA { get; set; }

        [StringLength(50)]
        public string CommissionDH { get; set; }

        [StringLength(50)]
        public string CommissionSH { get; set; }

        [StringLength(50)]
        public string RenewalCost { get; set; }

        public int UserId { get; set; }

        public virtual UserDetail UserDetail { get; set; }
    }
}
