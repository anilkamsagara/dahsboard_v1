namespace Dashboard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PartnerDetail
    {
        [Key]
        [StringLength(20)]
        public string PartnerId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Age { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        public int UserId { get; set; }

        [Required]
        [StringLength(20)]
        public string CompanyId { get; set; }

        public virtual CompanyDetail CompanyDetail { get; set; }

        public virtual UserDetail UserDetail { get; set; }
    }
}
