﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Dashboard.Models
{
    public class TicketRepository
    {
        private const string TicketTemplate = "SPAP{0}";
        private List<TicketDetail> _ticketDetails = new List<TicketDetail>();
        public IEnumerable<TicketDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _ticketDetails = gatiManagementEntities.TicketDetails.ToList();
            }
            return _ticketDetails;
        }

        public string Create(TicketDetail ticketDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _ticketDetails = gatiManagementEntities.TicketDetails.ToList();
                ticketDetail.TicketId = string.Format(CultureInfo.InvariantCulture, TicketTemplate, (_ticketDetails.Count + 1).ToString(CultureInfo.InvariantCulture));
                gatiManagementEntities.TicketDetails.Add(ticketDetail);
                gatiManagementEntities.SaveChanges();
            }
            return ticketDetail.TicketId;
        }

        public TicketDetail GetById(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _ticketDetails = gatiManagementEntities.TicketDetails.ToList();
            }
            var ticketDetail = _ticketDetails.FirstOrDefault(company => company.TicketId == id);
            return ticketDetail;
        }

        public TicketDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _ticketDetails = gatiManagementEntities.TicketDetails.ToList();
            }
            var ticketDetail = _ticketDetails.FirstOrDefault(company => company.UserId == id);
            return ticketDetail;
        }

        public void Delete(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var companyDetail = gatiManagementEntities.TicketDetails.FirstOrDefault(x => x.TicketId == id);
                gatiManagementEntities.TicketDetails.Remove(companyDetail);
                gatiManagementEntities.SaveChanges();
            }
        }

        public bool Update(TicketDetail ticketDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _ticketDetails = gatiManagementEntities.TicketDetails.ToList();
                var ticketDetails = _ticketDetails.Find(x => x.TicketId == ticketDetail.TicketId);
                if (ticketDetails == null) return true;
                gatiManagementEntities.TicketDetails.AddOrUpdate(ticketDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }
    }
}
