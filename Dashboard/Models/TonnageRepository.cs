﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data.Entity.Migrations;

namespace Dashboard.Models
{
    public class TonnageRepository
    {
        private List<TonnageDetail> _tonnageDetails = new List<TonnageDetail>();

        public IEnumerable<TonnageDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _tonnageDetails = gatiManagementEntities.TonnageDetails.ToList();
            }
            return _tonnageDetails;
        }
        public string Create(TonnageDetail tonnageDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _tonnageDetails = gatiManagementEntities.TonnageDetails.ToList();
                tonnageDetail.TonnageId = int.Parse(_tonnageDetails.Count.ToString(CultureInfo.InvariantCulture));
                gatiManagementEntities.TonnageDetails.Add(tonnageDetail);
                gatiManagementEntities.SaveChanges();
            }
            _tonnageDetails.Add(tonnageDetail);
            return "Tonnage Details added";
        }
        public TonnageDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _tonnageDetails = gatiManagementEntities.TonnageDetails.ToList();
            }
            var tonnageDetail = _tonnageDetails.FirstOrDefault(x => x.TonnageId == id);
            return tonnageDetail;
        }
        public bool Update(TonnageDetail tonnageDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _tonnageDetails = gatiManagementEntities.TonnageDetails.ToList();

                var tonnageDetails = _tonnageDetails.Find(x => x.TonnageId == tonnageDetail.TonnageId);
                if (tonnageDetails == null) return true;
                gatiManagementEntities.TonnageDetails.AddOrUpdate(tonnageDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public TonnageDetail GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _tonnageDetails = gatiManagementEntities.TonnageDetails.ToList();
            }
            var tonnageDetail = _tonnageDetails.FirstOrDefault(x => x.TonnageId == id);
            return tonnageDetail;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var tonnageDetail = gatiManagementEntities.TonnageDetails.FirstOrDefault(x => x.TonnageId == id);
                gatiManagementEntities.TonnageDetails.Remove(tonnageDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}