﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data.Entity.Migrations;

namespace Dashboard.Models
{
    public class RTORepository
    {
        private List<RTODetail> _rtoDetails = new List<RTODetail>();

        public IEnumerable<RTODetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _rtoDetails = gatiManagementEntities.RTODetails.ToList();
            }
            return _rtoDetails;
        }
        public string Create(RTODetail rtoDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _rtoDetails = gatiManagementEntities.RTODetails.ToList();
                rtoDetail.RTOId = int.Parse(_rtoDetails.Count.ToString(CultureInfo.InvariantCulture));
                gatiManagementEntities.RTODetails.Add(rtoDetail);
                gatiManagementEntities.SaveChanges();
            }
            _rtoDetails.Add(rtoDetail);
            return "RTO Details added";
        }
        public RTODetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _rtoDetails = gatiManagementEntities.RTODetails.ToList();
            }
            var rtoDetails = _rtoDetails.FirstOrDefault(x => x.RTOId == id);
            return rtoDetails;
        }
        public bool Update(RTODetail rtoDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _rtoDetails = gatiManagementEntities.RTODetails.ToList();

                var rtoDetails = _rtoDetails.Find(x => x.RTOId == rtoDetail.RTOId);
                if (rtoDetails == null) return true;
                gatiManagementEntities.RTODetails.AddOrUpdate(rtoDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public RTODetail GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _rtoDetails = gatiManagementEntities.RTODetails.ToList();
            }
            var rtoDetail = _rtoDetails.FirstOrDefault(x => x.RTOId == id);
            return rtoDetail;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var rtoDetail = gatiManagementEntities.RTODetails.FirstOrDefault(x => x.RTOId == id);
                gatiManagementEntities.RTODetails.Remove(rtoDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}