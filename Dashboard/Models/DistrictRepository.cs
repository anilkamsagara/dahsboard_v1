﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data.Entity.Migrations;

namespace Dashboard.Models
{
    public class DistrictRepository
    {
        private List<DistrictDetail> _districtDetails = new List<DistrictDetail>();

        public IEnumerable<DistrictDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _districtDetails = gatiManagementEntities.DistrictDetails.ToList();
            }
            return _districtDetails;
        }
        public string Create(DistrictDetail districtDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _districtDetails = gatiManagementEntities.DistrictDetails.ToList();
                districtDetail.DistrictId = int.Parse(_districtDetails.Count.ToString(CultureInfo.InvariantCulture));
                gatiManagementEntities.DistrictDetails.Add(districtDetail);
                gatiManagementEntities.SaveChanges();
            }
            _districtDetails.Add(districtDetail);
            return "District Details added";
        }
        public DistrictDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _districtDetails = gatiManagementEntities.DistrictDetails.ToList();
            }
            var rtoDetails = _districtDetails.FirstOrDefault(x => x.DistrictId == id);
            return rtoDetails;
        }
        public bool Update(DistrictDetail districtDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _districtDetails = gatiManagementEntities.DistrictDetails.ToList();

                var rtoDetails = _districtDetails.Find(x => x.DistrictId == districtDetail.DistrictId);
                if (rtoDetails == null) return true;
                gatiManagementEntities.DistrictDetails.AddOrUpdate(districtDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public DistrictDetail GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _districtDetails = gatiManagementEntities.DistrictDetails.ToList();
            }
            var districtDetail = _districtDetails.FirstOrDefault(x => x.DistrictId == id);
            return districtDetail;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var districtDetail = gatiManagementEntities.DistrictDetails.FirstOrDefault(x => x.DistrictId == id);
                gatiManagementEntities.DistrictDetails.Remove(districtDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}