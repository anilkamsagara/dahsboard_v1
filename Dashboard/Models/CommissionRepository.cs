﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;

namespace Dashboard.Models
{
    public class CommissionRepository
    {
        private List<CommissionDetail> _commissionDetails = new List<CommissionDetail>();

        public IEnumerable<CommissionDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _commissionDetails = gatiManagementEntities.CommissionDetails.ToList();
            }
            return _commissionDetails;
        }
        public string Create(CommissionDetail commissionDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _commissionDetails = gatiManagementEntities.CommissionDetails.ToList();
                commissionDetail.CommissionId = _commissionDetails.Count.ToString(CultureInfo.InvariantCulture);
                gatiManagementEntities.CommissionDetails.Add(commissionDetail);
                gatiManagementEntities.SaveChanges();
            }
            _commissionDetails.Add(commissionDetail);
            return "Vehicle Driver Details added";
        }

        public bool Update(CommissionDetail commissionDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _commissionDetails = gatiManagementEntities.CommissionDetails.ToList();

                var vehicleDetails = _commissionDetails.Find(x => x.CommissionId == commissionDetail.CommissionId);
                if (vehicleDetails == null) return true;
                gatiManagementEntities.CommissionDetails.AddOrUpdate(commissionDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public CommissionDetail GetById(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _commissionDetails = gatiManagementEntities.CommissionDetails.ToList();
            }
            var vehicleInsuranceDetail = _commissionDetails.FirstOrDefault(x => x.CommissionId == id);
            return vehicleInsuranceDetail;
        }

        public void Delete(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var vehicleDriverDetail = gatiManagementEntities.CommissionDetails.FirstOrDefault(x => x.CommissionId == id);
                gatiManagementEntities.CommissionDetails.Remove(vehicleDriverDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}