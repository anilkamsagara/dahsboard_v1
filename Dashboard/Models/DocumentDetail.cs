namespace Dashboard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DocumentDetail
    {
        [Key]
        [StringLength(20)]
        public string DocumentId { get; set; }

        [StringLength(50)]
        public string PanCardProof { get; set; }

        [StringLength(50)]
        public string AadharCardProof { get; set; }

        [StringLength(50)]
        public string CompanyRegistration { get; set; }

        [StringLength(50)]
        public string CompanyAddressProof { get; set; }

        [StringLength(50)]
        public string PartnerAddressProof { get; set; }

        [StringLength(50)]
        public string MobileBill { get; set; }

        [StringLength(50)]
        public string OfficeRentalDocument { get; set; }

        public int UserId { get; set; }

        public virtual UserDetail UserDetail { get; set; }
    }
}
