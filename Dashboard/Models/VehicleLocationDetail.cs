namespace Dashboard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VehicleLocationDetail
    {
        [Key]
        [StringLength(20)]
        public string VehicleLocationId { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string District { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        public int UserId { get; set; }

        [Required]
        [StringLength(20)]
        public string VehicleId { get; set; }

        public virtual UserDetail UserDetail { get; set; }
    }
}
