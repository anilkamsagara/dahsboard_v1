﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data.Entity.Migrations;

namespace Dashboard.Models
{
    public class PinCodeRepository
    {
        private List<PinCodeDetail> _pinCodeDetails = new List<PinCodeDetail>();

        public IEnumerable<PinCodeDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _pinCodeDetails = gatiManagementEntities.PinCodeDetails.ToList();
            }
            return _pinCodeDetails;
        }
        public string Create(PinCodeDetail pinCodeDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _pinCodeDetails = gatiManagementEntities.PinCodeDetails.ToList();
                pinCodeDetail.RTOId = int.Parse(_pinCodeDetails.Count.ToString(CultureInfo.InvariantCulture));
                gatiManagementEntities.PinCodeDetails.Add(pinCodeDetail);
                gatiManagementEntities.SaveChanges();
            }
            _pinCodeDetails.Add(pinCodeDetail);
            return "RTO Details added";
        }
        public PinCodeDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _pinCodeDetails = gatiManagementEntities.PinCodeDetails.ToList();
            }
            var pinCodeDetail = _pinCodeDetails.FirstOrDefault(x => x.RTOId == id);
            return pinCodeDetail;
        }
        public bool Update(PinCodeDetail pinCodeDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _pinCodeDetails = gatiManagementEntities.PinCodeDetails.ToList();

                var rtoDetails = _pinCodeDetails.Find(x => x.RTOId == pinCodeDetail.RTOId);
                if (rtoDetails == null) return true;
                gatiManagementEntities.PinCodeDetails.AddOrUpdate(pinCodeDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public PinCodeDetail GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _pinCodeDetails = gatiManagementEntities.PinCodeDetails.ToList();
            }
            var pinCodeDetail = _pinCodeDetails.FirstOrDefault(x => x.PinCodeId == id);
            return pinCodeDetail;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var rtoDetail = gatiManagementEntities.PinCodeDetails.FirstOrDefault(x => x.PinCodeId == id);
                gatiManagementEntities.PinCodeDetails.Remove(rtoDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}