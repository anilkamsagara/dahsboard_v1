﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;

namespace Dashboard.Models
{
    public class BankRepository 
    {
        private List<BankDetail> _bankDetails = new List<BankDetail>();

        public IEnumerable<BankDetail> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _bankDetails = gatiManagementEntities.BankDetails.ToList();
            }
            return _bankDetails;
        }
        public string Create(BankDetail bankDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _bankDetails = gatiManagementEntities.BankDetails.ToList();
                bankDetail.BankId = _bankDetails.Count.ToString(CultureInfo.InvariantCulture);
                gatiManagementEntities.BankDetails.Add(bankDetail);
                gatiManagementEntities.SaveChanges();
            }
            _bankDetails.Add(bankDetail);
            return "Personal Details added";
        }

        public bool Update(BankDetail bankDetail)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _bankDetails = gatiManagementEntities.BankDetails.ToList();

                var vehicleDetails = _bankDetails.Find(x => x.BankId == bankDetail.BankId);
                if (vehicleDetails == null) return true;
                gatiManagementEntities.BankDetails.AddOrUpdate(bankDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }

        public BankDetail GetById(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _bankDetails = gatiManagementEntities.BankDetails.ToList();
            }
            var bankDetail = _bankDetails.FirstOrDefault(bank => bank.Name == id);
            return bankDetail;
        }

        public BankDetail GetByUserId(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _bankDetails = gatiManagementEntities.BankDetails.ToList();
            }
            var bankDetail = _bankDetails.FirstOrDefault(bank => bank.UserId == id);
            return bankDetail;
        }

        public void Delete(string id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var bankDetail = gatiManagementEntities.BankDetails.FirstOrDefault(bank => bank.BankId == id);
                gatiManagementEntities.BankDetails.Remove(bankDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}