﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class StateController : ApiController
    {
        private readonly StateRepository _stateRepository = new StateRepository();

        // GET api/<controller>
        public IEnumerable<StateDetail> Get()
        {
            var stateDetails = _stateRepository.GetAll();
            return stateDetails;

        }
        public StateDetail GetByUserId(int userId)
        {
            var stateDetails = _stateRepository.GetByUserId(userId);
            return stateDetails;

        }
        // GET api/<controller>/5
        public StateDetail Get(int id)
        {
            var stateDetails = _stateRepository.GetById(id);
            return stateDetails;

        }

        // POST api/<controller>
        public void Post([FromBody]StateDetail value)
        {
            _stateRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]StateDetail value)
        {
            _stateRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _stateRepository.Delete(id);
        }
    }
}
