﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class TicketController : ApiController
    {
        private TicketRepository _ticketRepository = new TicketRepository();

        private string _ticketId = string.Empty;

        public string TicketId
        {
            get { return _ticketId; }
        }
        // GET api/<controller>
        public IEnumerable<TicketDetail> Get()
        {
            var users = _ticketRepository.GetAll();
            return users;

        }

        // GET api/<controller>/5
        public TicketDetail Get(string id)
        {
            var user = _ticketRepository.GetById(id);
            return user;

        }

        // GET api/<controller>/5
        public TicketDetail GetByUserId(int id)
        {
            var user = _ticketRepository.GetByUserId(id);
            return user;

        }

        // POST api/<controller>
        public void Post([FromBody]TicketDetail value)
        {
            _ticketId = _ticketRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]TicketDetail value)
        {
            _ticketRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(string id)
        {
            _ticketRepository.Delete(id);
        }
    }
}