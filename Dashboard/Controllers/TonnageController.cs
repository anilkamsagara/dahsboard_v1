﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class TonnageController : ApiController
    {
        private readonly TonnageRepository _tonnageRepository = new TonnageRepository();

        // GET api/<controller>
        public IEnumerable<TonnageDetail> Get()
        {
            var tonnageDetails = _tonnageRepository.GetAll();
            return tonnageDetails;

        }
        public TonnageDetail GetByUserId(int partnerId)
        {
            var tonnageDetails = _tonnageRepository.GetByUserId(partnerId);
            return tonnageDetails;

        }
        // GET api/<controller>/5
        public TonnageDetail Get(int id)
        {
            var tonnageDetails = _tonnageRepository.GetById(id);
            return tonnageDetails;

        }

        // POST api/<controller>
        public void Post([FromBody]TonnageDetail value)
        {
            _tonnageRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]TonnageDetail value)
        {
            _tonnageRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _tonnageRepository.Delete(id);
        }
    }
}
