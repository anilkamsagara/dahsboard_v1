﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class CommissionController : ApiController
    {
        private readonly CommissionRepository _commissionRepository = new CommissionRepository();

        // GET api/<controller>


        public IEnumerable<CommissionDetail> Get()
        {
            var vehicleDriverDetails = _commissionRepository.GetAll();
            return vehicleDriverDetails;

        }

        // GET api/<controller>/5
        public CommissionDetail Get(string id)
        {
            var vehicleDriverDetail = _commissionRepository.GetById(id);
            return vehicleDriverDetail;

        }

        // POST api/<controller>
        public void Post([FromBody]CommissionDetail value)
        {
            _commissionRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]CommissionDetail value)
        {
            _commissionRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(string id)
        {
            _commissionRepository.Delete(id);
        }
    }
}
