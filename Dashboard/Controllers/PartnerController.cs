﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class PartnerController : ApiController
    {

        private readonly PartnerRepository _partnerRepository = new PartnerRepository();

        // GET api/<controller>
        public IEnumerable<PartnerDetail> Get()
        {
            var partnerDetails = _partnerRepository.GetAll();
            return partnerDetails;

        }
        public PartnerDetail GetByUserId(int partnerId)
        {
            var partnerDetails = _partnerRepository.GetByUserId(partnerId);
            return partnerDetails;

        }
        // GET api/<controller>/5
        public PartnerDetail Get(string id)
        {
            var partnerDetail = _partnerRepository.GetById(id);
            return partnerDetail;

        }

        // POST api/<controller>
        public void Post([FromBody]PartnerDetail value)
        {
            _partnerRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]PartnerDetail value)
        {
            _partnerRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(string id)
        {
            _partnerRepository.Delete(id);
        }
    }
}
