﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class DistrictController : ApiController
    {
        private readonly DistrictRepository _districtRepository= new DistrictRepository();

        // GET api/<controller>
        public IEnumerable<DistrictDetail> Get()
        {
            var districtDetails = _districtRepository.GetAll();
            return districtDetails;

        }
        public DistrictDetail GetByUserId(int userId)
        {
            var districtDetails = _districtRepository.GetByUserId(userId);
            return districtDetails;

        }
        // GET api/<controller>/5
        public DistrictDetail Get(int id)
        {
            var districtDetails = _districtRepository.GetById(id);
            return districtDetails;

        }

        // POST api/<controller>
        public void Post([FromBody]DistrictDetail value)
        {
            _districtRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]DistrictDetail value)
        {
            _districtRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _districtRepository.Delete(id);
        }
    }
}
