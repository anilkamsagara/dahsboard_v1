﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class BankController : ApiController
    {
        private readonly BankRepository _repository = new BankRepository();

        // GET api/<controller>
        public IEnumerable<BankDetail> Get()
        {
            var bankDetails = _repository.GetAll();
            return bankDetails;

        }

        // GET api/<controller>/5
        public BankDetail Get(string id)
        {
            var bankDetail = _repository.GetById(id);
            return bankDetail;

        }

        // GET api/<controller>/5
        public BankDetail GetByUserId(int userId)
        {
            var bankDetail = _repository.GetByUserId(userId);
            return bankDetail;

        }

        // POST api/<controller>
        public void Post([FromBody]BankDetail value)
        {
            _repository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]BankDetail value)
        {
            _repository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(string id)
        {
            _repository.Delete(id);
        }
    }
}
