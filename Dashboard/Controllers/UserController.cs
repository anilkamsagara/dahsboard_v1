﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class UserController : ApiController
    {
        private readonly UserRepository _userRepository = new UserRepository();

        // GET api/<controller>
        public IEnumerable<UserDetail> Get()
        {
            var users = _userRepository.GetAllUsers();
            return users;

        }

        // GET api/<controller>/5
        public UserDetail Get(string id)
        {
            var user = _userRepository.Get(id);
            return user;

        }

        // GET api/<controller>/5
        public UserDetail GetByUserId(int id)
        {
            var user = _userRepository.GetByUserId(id);
            return user;

        }

        // POST api/<controller>
        public void Post([FromBody]UserDetail value)
        {
            _userRepository.CreateUser(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]UserDetail value)
        {
            _userRepository.UpdateUser(value);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _userRepository.DeleteUser(id);
        }
    }
}