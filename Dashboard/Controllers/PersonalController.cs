﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class PersonalController : ApiController
    {
        private readonly PersonalRepository _repository = new PersonalRepository();

        // GET api/<controller>
        public IEnumerable<PersonalDetail> Get()
        {
            var personalDetails = _repository.GetAll();
            return personalDetails;

        }

        // GET api/<controller>/5
        public PersonalDetail Get(string id)
        {
            var personalDetail = _repository.GetById(id);
            return personalDetail;

        }

        // GET api/<controller>/5
        public PersonalDetail GetByUserId(int userId)
        {
            var personalDetail = _repository.GetByUserId(userId);
            return personalDetail;

        }

        // POST api/<controller>
        public void Post([FromBody]PersonalDetail value)
        {
            _repository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]PersonalDetail value)
        {
            _repository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
