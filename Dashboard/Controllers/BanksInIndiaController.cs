﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class BanksInIndiaController : ApiController
    {
        private readonly BanksInIndiaRepository _banksInIndiaRepository = new BanksInIndiaRepository();

        // GET api/<controller>
        public IEnumerable<BanksInIndia> Get()
        {
            var tonnageDetails = _banksInIndiaRepository.GetAll();
            return tonnageDetails;

        }

        // GET api/<controller>/5
        public BanksInIndia Get(int id)
        {
            var tonnageDetails = _banksInIndiaRepository.GetById(id);
            return tonnageDetails;

        }

        // POST api/<controller>
        public void Post([FromBody]BanksInIndia value)
        {
            _banksInIndiaRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]BanksInIndia value)
        {
            _banksInIndiaRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _banksInIndiaRepository.Delete(id);
        }
    }
}
