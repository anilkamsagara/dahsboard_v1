﻿using Dashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Dashboard.Controllers
{
    public class AccountController : ApiController
    {
        private readonly AccountRepository _accountRepository = new AccountRepository();

        // GET api/<controller>
        public IEnumerable<AccountDetail> Get()
        {
            var accountDetails = _accountRepository.GetAll();
            return accountDetails;

        }
        public AccountDetail GetByUserId(int userId)
        {
            var accountDetails = _accountRepository.GetByUserId(userId);
            return accountDetails;

        }
        // GET api/<controller>/5
        public AccountDetail Get(string id)
        {
            var accountDetail = _accountRepository.GetById(id);
            return accountDetail;

        }

        // POST api/<controller>
        public void Post([FromBody]AccountDetail value)
        {
            _accountRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]AccountDetail value)
        {
            _accountRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(string id)
        {
            _accountRepository.Delete(id);
        }
    }
}
