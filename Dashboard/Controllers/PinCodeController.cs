﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class PinCodeController : ApiController
    {
        private readonly PinCodeRepository _pinCodeRepository = new PinCodeRepository();

        // GET api/<controller>
        public IEnumerable<PinCodeDetail> Get()
        {
            var pinCodeDetails = _pinCodeRepository.GetAll();
            return pinCodeDetails;

        }
        public PinCodeDetail GetByUserId(int userId)
        {
            var pinCodeDetails = _pinCodeRepository.GetByUserId(userId);
            return pinCodeDetails;

        }
        // GET api/<controller>/5
        public PinCodeDetail Get(int id)
        {
            var pinCodeDetails = _pinCodeRepository.GetById(id);
            return pinCodeDetails;

        }

        // POST api/<controller>
        public void Post([FromBody]PinCodeDetail value)
        {
            _pinCodeRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]PinCodeDetail value)
        {
            _pinCodeRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _pinCodeRepository.Delete(id);
        }
    }
}
