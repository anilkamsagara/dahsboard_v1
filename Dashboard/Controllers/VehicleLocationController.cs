﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class VehicleLocationController : ApiController
    {
        private readonly VehicleLocationRepository _vehicleLocationRepository = new VehicleLocationRepository();

        // GET api/<controller>
        public IEnumerable<VehicleLocationDetail> Get()
        {
            var vehicleLocationDetails = _vehicleLocationRepository.GetAll();
            return vehicleLocationDetails;

        }

        // GET api/<controller>/5
        public VehicleLocationDetail Get(string id)
        {
            var vehicleLocationDetail = _vehicleLocationRepository.GetById(id);
            return vehicleLocationDetail;

        }

        // POST api/<controller>
        public void Post([FromBody]VehicleLocationDetail value)
        {
            _vehicleLocationRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]VehicleLocationDetail value)
        {
            _vehicleLocationRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(string id)
        {
            _vehicleLocationRepository.Delete(id);
        }
    }
}
