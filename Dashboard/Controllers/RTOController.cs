﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class RTOController : ApiController
    {
        private readonly RTORepository _rtoRepository = new RTORepository();

        // GET api/<controller>
        public IEnumerable<RTODetail> Get()
        {
            var rtoDetails = _rtoRepository.GetAll();
            return rtoDetails;

        }
        public RTODetail GetByUserId(int userId)
        {
            var rtoDetails = _rtoRepository.GetByUserId(userId);
            return rtoDetails;

        }
        // GET api/<controller>/5
        public RTODetail Get(int id)
        {
            var rtoDetails = _rtoRepository.GetById(id);
            return rtoDetails;

        }

        // POST api/<controller>
        public void Post([FromBody]RTODetail value)
        {
            _rtoRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]RTODetail value)
        {
            _rtoRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            _rtoRepository.Delete(id);
        }
    }
}
