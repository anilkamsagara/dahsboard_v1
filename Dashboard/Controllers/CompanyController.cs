﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class CompanyController : ApiController
    {
        private CompanyRepository _companyRepository = new CompanyRepository();
        private string _companyId = string.Empty;

        public string CompanyId
        {
            get { return _companyId; }
        }

        // GET api/<controller>
        public IEnumerable<CompanyDetail> Get()
        {
            var users = _companyRepository.GetAll();
            return users;

        }

        // GET api/<controller>/5
        public CompanyDetail Get(string id)
        {
            var user = _companyRepository.GetById(id);
            return user;

        }

        // GET api/<controller>/5
        public CompanyDetail GetByUserId(int id)
        {
            var user = _companyRepository.GetByUserId(id);
            return user;

        }

        // POST api/<controller>
        public void Post([FromBody]CompanyDetail value)
        {
            _companyId = _companyRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]CompanyDetail value)
        {
            _companyRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(string id)
        {
            _companyRepository.Delete(id);
        }
    }
}
