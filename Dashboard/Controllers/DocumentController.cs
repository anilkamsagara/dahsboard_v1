﻿using System.Collections.Generic;
using System.Web.Http;
using Dashboard.Models;

namespace Dashboard.Controllers
{
    public class DocumentController : ApiController
    {
        private readonly DocumentRepository _documentRepository = new DocumentRepository();

        // GET api/<controller>
        public IEnumerable<DocumentDetail> Get()
        {
            var documentDetails = _documentRepository.GetAll();
            return documentDetails;

        }

        // GET api/<controller>/5
        public DocumentDetail Get(string id)
        {
            var documentDetail = _documentRepository.GetById(id);
            return documentDetail;

        }

        // GET api/<controller>/5
        public DocumentDetail GetByUserId(int id)
        {
            var documentDetail = _documentRepository.GetByUserId(id);
            return documentDetail;

        }

        // POST api/<controller>
        public void Post([FromBody]DocumentDetail value)
        {
            _documentRepository.Create(value);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]DocumentDetail value)
        {
            _documentRepository.Update(value);
        }

        // DELETE api/<controller>/5
        public void Delete(string id)
        {
            _documentRepository.Delete(id);
        }
    }
}
