﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Dashboard.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %></h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:UpdatePanel runat="server" ID="MessagePanel">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-success"><%: SuccessMessage %></p>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-danger"><%: FailureMessage %></p>
            </asp:PlaceHolder>
            <asp:Timer runat="server" Interval="1000" OnTick="MessagePanelClick"></asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel runat="server" ID="RegisterPanel" DefaultButton="registerNextButton">
        <div class="form-horizontal">
            <h4>Create a new account.</h4>
            <hr />
            <asp:ValidationSummary runat="server" CssClass="text-danger" />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-2 control-label">User name</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName" ValidationGroup="RegisterGroup"
                        CssClass="text-danger" ErrorMessage="The user name field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" ValidationGroup="RegisterGroup"
                        CssClass="text-danger" ErrorMessage="The password field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword" ValidationGroup="RegisterGroup"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                    <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" ValidationGroup="RegisterGroup"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button ID="registerNextButton" runat="server" OnClick="OnRegister" ValidationGroup="RegisterGroup" Text="Next" CssClass="btn btn-default" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PersonalDetailsPanel" Visible="False" DefaultButton="PersonalDetailsNextButton">
        <div class="form-horizontal">
            <h4>Personal Details.</h4>
            <hr />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Name</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                    <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Name"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The name field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-2 control-label">Address</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
                    <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Address"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The address field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-2 control-label">City</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="City" CssClass="form-control" />
                    <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="City"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The city field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="District" CssClass="col-md-2 control-label">District</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="District" CssClass="form-control" />
                    <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="District"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The district field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-2 control-label">State</asp:Label>
                <div class="col-md-10">
                    <asp:DropDownList runat="server" ID="State" CssClass="form-control" Width="280px" />
                    <asp:CompareValidator ValidationGroup="PersonalDetailsGroup" ID="StateComapareValidator" runat="server" CssClass="text-danger"
                        ControlToValidate="State" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Please select State"></asp:CompareValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Country" CssClass="col-md-2 control-label">Country</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Country" CssClass="form-control" />
                    <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Country"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The country field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Age" CssClass="col-md-2 control-label">Age</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Age" CssClass="form-control" />
                    <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Age"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The age field is required." />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorAge" ControlToValidate="Age" CssClass="text-danger" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+" />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmailId" CssClass="col-md-2 control-label">Email Id</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="EmailId" CssClass="form-control" />
                    <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="EmailId"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The email id field is required." />
                    <asp:RegularExpressionValidator ID="regEmail" CssClass="text-danger" runat="server" ControlToValidate="EmailId" ValidationExpression="^(.+)@([^\.].*)\.([a-z]{2,})$" Text="Enter a valid email" />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="MobileNumber" CssClass="col-md-2 control-label">Mobile Number</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="MobileNumber" CssClass="form-control" />
                    <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="MobileNumber"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Mobile Number field is required." />
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="MobileNumber"
                        CssClass="text-danger" ValidationGroup="PersonalDetailsGroup" Display="Dynamic" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$" ErrorMessage="Enter valid mobile number" />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="PanCard" CssClass="col-md-2 control-label">Pan card number</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="PanCard" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="PanCard"
                        CssClass="text-danger" DValidationGroup="PersonalDetailsGroup" isplay="Dynamic" ErrorMessage="The pan card field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="AadharCardNumber" CssClass="col-md-2 control-label">Aadhar Card Number</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="AadharCardNumber" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="AadharCardNumber"
                        CssClass="text-danger" ValidationGroup="PersonalDetailsGroup" Display="Dynamic" ErrorMessage="The Aadhar Card number field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmployeeType" CssClass="col-md-2 control-label">Employee Type</asp:Label>
                <div class="col-md-10">
                    <asp:DropDownList runat="server" ID="EmployeeType" CssClass="form-control" Width="280px" />
                    <asp:CompareValidator ID="CompareValidatorEmployeeType" runat="server" CssClass="text-danger"
                        ControlToValidate="EmployeeType" ValidationGroup="PersonalDetailsGroup" ValueToCompare="--Select Employee Type--" Operator="NotEqual" Type="String" ErrorMessage="Please select employee Type"></asp:CompareValidator>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" ID="PersonalDetailsNextButton" ValidationGroup="PersonalDetailsGroup" OnClick="OnPersonal" Text="Next" CssClass="btn btn-default" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="CompanyDetailsPanel" Visible="False" DefaultButton="companyDetailsButton">
        <div class="form-horizontal">
            <h4>Company Details.</h4>
            <hr />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="NameoftheFirm" CssClass="col-md-2 control-label">Name of the Firm</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="NameoftheFirm" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NameoftheFirm" ValidationGroup="CompanyGroup"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Name of the Firm field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="FirmType" CssClass="col-md-2 control-label">Firm Type</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="FirmType" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="FirmType" ValidationGroup="CompanyGroup"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Firm Type field is required." />
                </div>
                <asp:Button Text="Add Partner" OnClick="OnAddClick" runat="server" CssClass="btn btn-default" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="TelephoneNumber" CssClass="col-md-2 control-label">Telephone Number</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="TelephoneNumber" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="TelephoneNumber" ValidationGroup="CompanyGroup"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Telephone Number field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-2 control-label">Registration Number</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="RegistrationNumber" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="RegistrationNumber" ValidationGroup="CompanyGroup"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Registration Number is required." />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-3">
                    <asp:Button runat="server" OnClick="OnCompanyDetailsBack" CausesValidation="False" Text="Back" CssClass="btn btn-default" />
                    <asp:Button runat="server" ID="companyDetailsButton" OnClick="OnCompanyDetailsNext" ValidationGroup="CompanyGroup" Text="Next" CssClass="btn btn-default" />
                </div>
                <div class="col-md-3">
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="BankDetailsPanel" Visible="False" DefaultButton="bankNextButton">
        <div class="form-horizontal">
            <h4>Bank Details.</h4>
            <hr />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="NameoftheBank" CssClass="col-md-2 control-label">Name of the Bank</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="NameoftheBank" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NameoftheBank" ValidationGroup="BankGroup"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Name of the Bank field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="BankAddress" CssClass="col-md-2 control-label">Bank Address</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="BankAddress" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="BankAddress" ValidationGroup="BankGroup"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Bank Address field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="MICRCode" CssClass="col-md-2 control-label">MICR Code</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="MICRCode" CssClass="form-control" />
                    <asp:RegularExpressionValidator ValidationGroup="BankGroup" ControlToValidate="MICRCode" CssClass="text-danger" runat="server" ErrorMessage="Enter valid MICR code" ValidationExpression="\d+" />
                    <asp:RequiredFieldValidator ValidationGroup="BankGroup" runat="server" ControlToValidate="MICRCode"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The MICR Code field is required." />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="IFSCCode" CssClass="col-md-2 control-label">IFSC Code</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="IFSCCode" CssClass="form-control" />
                    <asp:RegularExpressionValidator ValidationGroup="BankGroup" runat="server" ControlToValidate="IFSCCode"
                        CssClass="text-danger" Display="Dynamic" ValidationExpression="^[^\s]{4}\d{7}$" ErrorMessage="Enter valid IFSC code" />
                    <asp:RequiredFieldValidator ValidationGroup="BankGroup" runat="server" ControlToValidate="IFSCCode"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The IFSC Code Number is required." />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="OnBankBack" CausesValidation="False" Text="Back" CssClass="btn btn-default" />
                    <asp:Button runat="server" ID="bankNextButton" OnClick="OnBankNext" ValidationGroup="BankGroup" Text="Next" CssClass="btn btn-default" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="DocumentDetailsPanel" Visible="False" DefaultButton="documentDetailsButton">
        <div class="form-horizontal">
            <h4>Create a new account.</h4>
            <hr />
            <asp:ValidationSummary runat="server" CssClass="text-danger" />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="PanCardProof" CssClass="col-md-2 control-label">Pan card proof</asp:Label>
                <asp:CheckBox runat="server" ID="PanCardProof" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="AadhrarCardProof" CssClass="col-md-2 control-label">Aadhar card proof</asp:Label>
                <asp:CheckBox runat="server" ID="AadhrarCardProof" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="CompanyRegistrationProof" CssClass="col-md-2 control-label">Company registration proof</asp:Label>
                <asp:CheckBox runat="server" ID="CompanyRegistrationProof" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="CompanyAddressProof" CssClass="col-md-2 control-label">Company address proof</asp:Label>
                <asp:CheckBox runat="server" ID="CompanyAddressProof" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="MobileBill" CssClass="col-md-2 control-label">Mobile bill</asp:Label>
                <asp:CheckBox runat="server" ID="MobileBill" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="OfficeRentalDocument" CssClass="col-md-2 control-label">Office rental document</asp:Label>
                <asp:CheckBox runat="server" ID="OfficeRentalDocument" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="PartnerAddressProof" CssClass="col-md-2 control-label">Office rental document</asp:Label>
                <asp:CheckBox runat="server" ID="PartnerAddressProof" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="OnDocumentDetailsBack" CausesValidation="False" Text="Back" CssClass="btn btn-default" />
                    <asp:Button runat="server" ID="documentDetailsButton" OnClick="OnDocumentDetailsNext" ValidationGroup="DocumentGroup" Text="Submit" CssClass="btn btn-default" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="modal fade" id="myModalComments" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModalComments" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="Label1" runat="server" Text="Comment"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:TextBox TextMode="MultiLine" Width="100%" ID="Comments" runat="server"></asp:TextBox>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" onserverclick="OnApprovalComments" data-dismiss="modal" aria-hidden="true">Ok</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="modal fade" id="myModalPartner" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModalPartner" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="lblModalTitle" runat="server" Text="Add Partner Details"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-2 control-label">Name</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="PartnerName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-2 control-label">Age</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="PartnerAge" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-2 control-label">Address</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="PartnerAddress" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" onserverclick="OnPartnerDetails" data-dismiss="modal" aria-hidden="true">Add</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>
