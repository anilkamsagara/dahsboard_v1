﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BA.aspx.cs" Inherits="Dashboard.Account.BA" EnableEventValidation="false" %>

<%@ Register Assembly="DatePickerControl" Namespace="DatePickerControl" TagPrefix="cc1" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h6></h6>
    <h3><%: Title %></h3>

    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:UpdatePanel runat="server" ID="MessagePanel">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-success"><%: SuccessMessage %></p>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-danger"><%: FailureMessage %></p>
            </asp:PlaceHolder>
            <asp:Timer ID="timer" runat="server" Interval="2000" OnTick="MessagePanelClick"></asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>

    <link rel="stylesheet" href="~/Content/datepicker.css">
    <link rel="stylesheet" href="~/Content/bootstrap.css">
    <%--Balance Place Holder--%>

    <div class="row" id="Tabs">
        <!-- Navigation Buttons -->
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" id="myTabs">
                <li class="active"><a href="#notification">Notification</a></li>
                <li><a href="#deposit">Pay Deposit Amount</a></li>
                <li><a href="#Commission" data-toggle="tab" runat="server">Check Commission Amount</a></li>
                <li><a href="#createticket" data-toggle="tab" runat="server">Create Ticket</a></li>
                <li><a href="#viewticket" data-toggle="tab" runat="server">View Tickets</a></li>
                <li><a href="#updatelocation" data-toggle="tab">Update Vehicle Location</a></li>
                <li><a href="#updatedriver" data-toggle="tab">Update Driver Details</a></li>
                <li><a href="#disablevehicle" data-toggle="tab">Enable or Disable Vehicle</a></li>
                <li><a href="#updatevehicle" runat="server" data-toggle="tab">Update Vehicle Details</a></li>
                <li><a href="#renewvehicle" data-toggle="tab">View Vehicles to renew</a></li>
                <li><a href="#viewvehicle" data-toggle="tab" runat="server">View Vehicles List</a></li>
                <%--<li><a href="#viewaccount" data-toggle="tab">View Account Details</a></li>--%>
            </ul>
        </div>

        <!-- Content -->
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="notification">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <asp:Panel ID="notificationPanel" runat="server">
                        <div class="form-horizontal">
                            <h4>Notifications</h4>
                            <br />
                            <asp:Panel ID="pendingNotificationPanel" runat="server" Visible="True">
                                <asp:Label runat="server" Text="No pending notification" CssClass="label-info"></asp:Label>
                            </asp:Panel>
                            <div class="form-group">
                                <asp:EntityDataSource ID="TicketEntityDataSource" OnQueryCreated="OnViewBusinessAssociateNotification" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails"></asp:EntityDataSource>
                                <asp:GridView ID="TicketGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" ValidationGroup="Viewticketnotification" OnSelectedIndexChanged="OnTicketNotificationGridChanged" runat="server" AutoGenerateColumns="False" DataSourceID="TicketEntityDataSource" DataKeyNames="TicketId">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" ValidationGroup="Viewticketnotification"></asp:CommandField>
                                        <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="True" SortExpression="TicketId"></asp:BoundField>
                                        <asp:BoundField DataField="Registration" HeaderText="Vehicle Number" SortExpression="Registration"></asp:BoundField>
                                        <asp:BoundField DataField="DHPermission" HeaderText="DH Approval" SortExpression="DHPermission"></asp:BoundField>
                                        <asp:BoundField DataField="DHComments" HeaderText="DH Comments" SortExpression="DHComments"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                <asp:LinkButton ID="PrintLinkButton" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='TicketGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                                <asp:LinkButton ID="ExportLinkButton" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='TicketGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="tab-pane " id="deposit">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <asp:Panel ID="DepositPanel" runat="server" DefaultButton="DepositButton">
                        <div class="form-horizontal">
                            <h4>Pay Deposit Amount</h4>
                            <br />
                            <div class="form-group">
                                <asp:Label ID="balancelabel" runat="server" Text="Account Balance :-" Font-Bold="True" CssClass="control-label"></asp:Label>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="depositTextbox" CssClass="col-md-3 control-label">Enter Amount</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="depositTextbox" CssClass="form-control" />
                                </div>
                                <asp:Button runat="server" ID="DepositButton" Text="submit" ValidationGroup="DepositGroup" CssClass="btn btn-default" OnClick="HttpContextButton_Click" />
                            </div>

                        </div>
                    </asp:Panel>
                </div>

                <div class="tab-pane" id="Commission">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Check Commission Amount</h4>

                        <div class="form-group">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="totalCommission">Start Date  </asp:Label>
                                <cc1:DatePicker DateFormat="dd/MM/yyyy" ID="DatePickerstart" Width="150px" runat="server" />
                                <asp:Label runat="server" AssociatedControlID="totalCommission">End Date  </asp:Label>
                                <cc1:DatePicker DateFormat="dd/MM/yyyy" ID="DatePickerEnd" Width="150px" runat="server" />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button runat="server" ID="commissionButton" Text="Submit" OnClick="commissionButton_OnClick" CssClass="btn btn-default" />
                                </div>
                            </div>
                            <br/>
                            <asp:GridView ID="ComissionGrid" runat="server" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" EnableSortingAndPagingCallbacks="True" ValidationGroup="ViewCommissionGroup" CssClass="mGrid" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="Registration" HeaderText="Registration" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="StarteDate" HeaderText="StarteDate" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="EndDate" HeaderText="EndDate" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" ReadOnly="True"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:LinkButton ID="commissionPrintLinkButton" Visible="False" runat="server" OnClick="commissionPrintLinkButton_OnClick" ValidationGroup="ViewCommissionGroup" ToolTip="Click to Print current page" Text="Print Current Page"></asp:LinkButton>
                            <asp:LinkButton ID="commissionExportLinkButton" Visible="False" runat="server" ValidationGroup="ViewCommissionGroup" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='ComissionGrid' OnCommand="ExportCurrentpage"></asp:LinkButton>
                        </div>
                    </div>
                    <br />
                    <asp:Panel runat="server" Visible="False" ID="totalCommissionPanel">
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="totalCommission" CssClass="col-md-3 control-label">Total Commission: </asp:Label>
                            <div class="col-md-5">
                                <asp:TextBox ReadOnly="True" runat="server" ID="totalCommission" CssClass="form-control" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="tab-pane" id="createticket">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <asp:Panel runat="server" ID="VehicleDetailsPanel" DefaultButton="VehicleDetailsButton">
                        <div class="form-horizontal">
                            <h4>Vehicle details</h4>
                            <br />
                            <div class="form-group">                                
                                <asp:Label runat="server" AssociatedControlID="GoodsType" CssClass="col-md-3 control-label">Goods Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="GoodsType" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                    ControlToValidate="GoodsType" ValidationGroup="VehicleTicketValidationGroup" ValueToCompare="--Select Goods Type--" Operator="NotEqual" Type="String" ErrorMessage="Please select goods type"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="tonnageCapacity" CssClass="col-md-3 control-label">Tonnage Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="tonnageCapacity" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                    ControlToValidate="tonnageCapacity" ValidationGroup="VehicleTicketValidationGroup" ValueToCompare="-- Select Range --" Operator="NotEqual" Type="String" ErrorMessage="Please select tonnage capacity"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="MakeOftheVehicle" CssClass="col-md-3 control-label">Make of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="MakeOftheVehicle" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="MakeOftheVehicle"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The make of the vehicle field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ModelOftheVehicle" CssClass="col-md-3 control-label">Model of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="ModelOftheVehicle" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ModelOftheVehicle"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The model of the vehicle field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="CubicCapacity" CssClass="col-md-3 control-label">Cubic Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="CubicCapacity" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="CubicCapacity"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The cubic capacity field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="LengthoftheVehicle" CssClass="col-md-3 control-label">Length of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="LengthoftheVehicle" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="LengthoftheVehicle"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The length of the vehicle field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="WidthoftheVehicle" CssClass="col-md-3 control-label">Width of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="WidthoftheVehicle" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="WidthoftheVehicle"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The width of the vehicle field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="BreadthoftheVehicle" CssClass="col-md-3 control-label">Breadth of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="BreadthoftheVehicle" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="BreadthoftheVehicle"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The breadth of the vehicle field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="HeightoftheVehicle" CssClass="col-md-3 control-label">Height of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="HeightoftheVehicle" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="HeightoftheVehicle"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The height of the vehicle field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-3 control-label">Registration Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="RegistrationNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="RegistrationNumber"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The registration number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="EngineNumber" CssClass="col-md-3 control-label">Engine Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="EngineNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="EngineNumber"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The engine number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ChasisNumber" CssClass="col-md-3 control-label">Chassis Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="ChasisNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ChasisNumber"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The chasis number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NOCRTO" CssClass="col-md-3 control-label">NOC from RTO</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="NOCRTO" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NOCPolice" CssClass="col-md-3 control-label">NOC from Police</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="NOCPolice" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoUploadFront" CssClass="col-md-3 control-label">Vehicle Photo Front</asp:Label>
                                <div class="col-md-5">
                                    <asp:FileUpload CssClass="form-control" ID="VehiclePhotoUploadFront" runat="server" Width="280px" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoUploadBack" CssClass="col-md-3 control-label">Vehicle Photo Back</asp:Label>
                                <div class="col-md-5">
                                    <asp:FileUpload CssClass="form-control" ID="VehiclePhotoUploadBack" runat="server" Width="280px" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoUploadLeftSide" CssClass="col-md-3 control-label">Vehicle Photo Side</asp:Label>
                                <div class="col-md-5">
                                    <asp:FileUpload CssClass="form-control" ID="VehiclePhotoUploadLeftSide" runat="server" Width="280px" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoUploadRightSide" CssClass="col-md-3 control-label">Vehicle Photo Side</asp:Label>
                                <div class="col-md-5">
                                    <asp:FileUpload CssClass="form-control" ID="VehiclePhotoUploadRightSide" runat="server" Width="280px" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FCValidityFrom" CssClass="col-md-3 control-label">FC Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="FCValidityFrom" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="FCValidityFrom"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The FC Validiy field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FCValidity" CssClass="col-md-3 control-label">FC Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="FCValidity" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="FCValidity"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The FC Validiy field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehicleOwnerName" CssClass="col-md-3 control-label">Owner Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="VehicleOwnerName" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="VehicleOwnerName"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The name field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Address"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The address field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="City" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="City"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The city field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="District" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="District" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="District"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The district field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="State" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator ID="StateComapareValidator" runat="server" CssClass="text-danger"
                                    ControlToValidate="State" ValidationGroup="VehicleTicketValidationGroup" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Please select state"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Country" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="Country" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Country"
                                    CssClass="text-danger" ValidationGroup="VehicleTicketValidationGroup" Display="Dynamic" ErrorMessage="The country field is required." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button runat="server" ID="VehicleDetailsButton" Text="Next" ValidationGroup="VehicleTicketValidationGroup" OnClick="OnCreateVehicleTicket" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="VehicleInsurancePanel" Visible="false" DefaultButton="VehicleInsuranceButton">
                        <div class="form-horizontal">
                            <h4>Enter Vehicle Insurance Details</h4>
                            <br>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="InsuranceType" CssClass="col-md-3 control-label">Insurance Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="InsuranceType" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="InsuranceType"
                                    CssClass="text-danger" ValidationGroup="InsuranceTicketValidationGroup" Display="Dynamic" ErrorMessage="The insurance Type field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="InsuranceCompany" CssClass="col-md-3 control-label">Insurer Company</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="InsuranceCompany" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="InsuranceCompany"
                                    CssClass="text-danger" ValidationGroup="InsuranceTicketValidationGroup" Display="Dynamic" ErrorMessage="The insurance Company field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ValidityFrom" CssClass="col-md-3 control-label">Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="ValidityFrom" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ValidityFrom"
                                    CssClass="text-danger" ValidationGroup="InsuranceTicketValidationGroup" Display="Dynamic" ErrorMessage="The Validate From field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ValidityTo" CssClass="col-md-3 control-label">Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="ValidityTo" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ValidityTo"
                                    CssClass="text-danger" ValidationGroup="InsuranceTicketValidationGroup" Display="Dynamic" ErrorMessage="The Validate To field is required." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button runat="server" Text="Back" ValidationGroup="VehicleTicketValidationGroup" CausesValidation="false" OnClick="OnBackInsuranceTicket" CssClass="btn btn-default" />
                                    <asp:Button runat="server" ID="VehicleInsuranceButton" Text="Next" ValidationGroup="VehicleTicketValidationGroup" OnClick="OnCreateInsuranceTicket" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="VehicleDriverPanel" Visible="false" DefaultButton="VehicleDriverButton">
                        <div class="form-horizontal">
                            <h4>Vehicle driver details</h4>
                            <br />
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverName" CssClass="col-md-3 control-label">Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverName" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverName"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The driver name field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverAge" CssClass="col-md-3 control-label">Age</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverAge" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverAge"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The driver age field is required." />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorAge" ControlToValidate="DriverAge" CssClass="text-danger" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverAddress" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverAddress"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The driver address field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverCity" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverCity"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The driver city field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverDistrict" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverDistrict"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The Driver District field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverState" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="DriverState" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator ID="CompareValidator5" runat="server" CssClass="text-danger"
                                    ControlToValidate="DriverState" ValidationGroup="DriverTicketValidationGroup" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Please select state"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverCountry" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverCountry"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The Driver Country field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverDLNumber" CssClass="col-md-3 control-label">DL Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverDLNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverDLNumber"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The Driver DL Number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverPhotoUpload" CssClass="col-md-3 control-label">Photo</asp:Label>
                                <div class="col-md-5">
                                    <asp:FileUpload CssClass="form-control" ID="DriverPhotoUpload" runat="server" Width="280px" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverMobileNumber" CssClass="col-md-3 control-label">Mobile Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverMobileNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverMobileNumber"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The Driver Mobile Number DL field is required." />
                                <asp:RegularExpressionValidator runat="server" ControlToValidate="DriverMobileNumber"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$" ErrorMessage="Enter valid mobile number" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverEmailId" CssClass="col-md-3 control-label">Email</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverEmailId" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverEmailId"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The Driver Email Id field is required." />
                                <asp:RegularExpressionValidator CssClass="text-danger" runat="server" ControlToValidate="DriverEmailId" ValidationExpression="^(.+)@([^\.].*)\.([a-z]{2,})$" Text="Enter a valid email" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverPanNumber" CssClass="col-md-3 control-label">Pan card number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverPanNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverPanNumber"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The Driver Pan Number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverAadharCardNumber" CssClass="col-md-3 control-label">Adhar Card Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="DriverAadharCardNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="DriverAadharCardNumber"
                                    CssClass="text-danger" ValidationGroup="DriverTicketValidationGroup" Display="Dynamic" ErrorMessage="The Driver Aadhar CardNumber field is required." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button runat="server" Text="Back" ValidationGroup="DriverTicketValidationGroup" CausesValidation="false" OnClick="OnBackDriverTicket" CssClass="btn btn-default" />
                                    <asp:Button runat="server" ID="VehicleDriverButton" Text="Submit Ticket" ValidationGroup="DriverTicketValidationGroup" OnClick="OnCreateDriverTicket" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="tab-pane" id="viewticket">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Tickets</h4>
                        <br />
                        <div class="form-group">
                            <asp:EntityDataSource ID="EntityDataSource4" runat="server" OnQueryCreated="EntityDataSource4_OnQueryCreated" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails" AutoGenerateOrderByClause="False" EntityTypeFilter=""></asp:EntityDataSource>
                            <asp:GridView ID="gridview4" runat="server"
                                DataSourceID="entitydatasource4"
                                ValidationGroup="ViewTicket"
                                GridLines="none"
                                AllowPaging="true"
                                CssClass="mGrid"
                                PagerStyle-CssClass="pgr"
                                AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="false" AllowSorting="True" EnableSortingAndPagingCallbacks="True">
                                <Columns>
                                    <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="true" SortExpression="TicketId"></asp:BoundField>
                                    <asp:BoundField DataField="Registration" HeaderText="Vehicle Number" ReadOnly="true" SortExpression="Registration"></asp:BoundField>
                                    <asp:BoundField DataField="BAPermission" HeaderText="BA Approval" ReadOnly="true" SortExpression="BAPermission"></asp:BoundField>
                                    <asp:BoundField DataField="DHPermission" HeaderText="DH Approval" ReadOnly="true" SortExpression="DHPermission"></asp:BoundField>
                                    <asp:BoundField DataField="SHPermission" HeaderText="SH Approval" ReadOnly="true" SortExpression="SHPermission"></asp:BoundField>
                                    <asp:BoundField DataField="SAPermission" HeaderText="SA Approval" ReadOnly="true" SortExpression="SAPermission"></asp:BoundField>
                                    <asp:BoundField DataField="TicketStatus" HeaderText="Ticket Status" ReadOnly="true" SortExpression="TicketStatus"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:LinkButton ID="LinkButton6" Visible="true" runat="server" ValidationGroup="ViewTicket" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='gridview4' OnCommand="PrintCurrentPage"></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton7" Visible="true" runat="server" ValidationGroup="ViewTicket" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='gridview4' OnClick="ExportCurrentpage"></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="updatelocation">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Update Vehicle Location</h4>
                        <%--               <asp:Panel runat="server" ID="UpdateLocationSubmitPanel" DefaultButton="UpdateLocationSubmitButton">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateLocationVehicleId" CssClass="col-md-3 control-label">Vehicle number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateLocationVehicleId" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateLocationVehicleId"
                                    CssClass="text-danger" ValidationGroup="UpdateLocationSearch" Display="Dynamic" ErrorMessage="The Update Location VehicleId field is required." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button ID="UpdateLocationSubmitButton" runat="server" Text="Submit" OnClick="OnUpdateLocationSubmit" ValidationGroup="UpdateLocationSearch" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </asp:Panel>--%>

                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="UpdateLocationVehicleId" CssClass="col-md-3 control-label">Vehicle number</asp:Label>
                            <div class="col-md-5">
                                <asp:DropDownList AutoPostBack="True" runat="server" OnSelectedIndexChanged="OnUpdateLocationSubmit" ID="UpdateLocationVehicleId" CssClass="form-control" Width="280px" />
                            </div>
                            <asp:CompareValidator runat="server" CssClass="text-danger"
                                ControlToValidate="UpdateLocationVehicleId" ValidationGroup="UpdateLocation" ValueToCompare="-- Select Vehicle No --" Operator="NotEqual" Type="String" ErrorMessage="Select vehicle no"></asp:CompareValidator>
                        </div>

                        <asp:Panel runat="server" ID="UpdateLocationPanel" Visible="false" DefaultButton="UpdateLocationButton">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateLocationAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="true" ID="UpdateLocationAddress" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateLocationCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="true" ID="UpdateLocationCity" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateLocationDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="true" ID="UpdateLocationDistrict" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleCurrentLocation" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="true" ID="UpdateVehicleCurrentLocation" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateLocationCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="true" ID="UpdateLocationCountry" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleLocation" CssClass="col-md-3 control-label">Current location</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleLocation" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleLocation"
                                    CssClass="text-danger" ValidationGroup="UpdateLocation" Display="Dynamic" ErrorMessage="The Update Vehicle Location field is required." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button runat="server" ID="UpdateLocationButton" Text="Update" OnClick="OnUpdateLocation" ValidationGroup="UpdateLocation" CssClass="btn btn-default" />
                                    <asp:Button runat="server" ID="UpdateLocationButtonCancel" Text="Cancel" CausesValidation="False" OnClick="OnUpdateLocationCancel" ValidationGroup="UpdateLocation" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="tab-pane" id="updatedriver">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Update Driver Details</h4>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="UpdateDriverVehicleId" CssClass="col-md-3 control-label">Vehicle number</asp:Label>
                            <div class="col-md-5">
                                <asp:DropDownList AutoPostBack="True" runat="server" OnSelectedIndexChanged="OnVehicleDriverIdChanged" ID="UpdateDriverVehicleId" CssClass="form-control" Width="280px" />
                            </div>
                            <asp:CompareValidator runat="server" CssClass="text-danger"
                                ControlToValidate="UpdateDriverVehicleId" ValidationGroup="UpdateDriverDetails" ValueToCompare="-- Select Vehicle No --" Operator="NotEqual" Type="String" ErrorMessage="Select vehicle no"></asp:CompareValidator>
                        </div>
                        <asp:Panel runat="server" ID="UpdateDriverDetailsPanel" Visible="false" DefaultButton="updateDriverDetailsButton">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverName" CssClass="col-md-3 control-label">Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverName" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverName"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The Driver Name field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverAge" CssClass="col-md-3 control-label">Age</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverAge" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverAge"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The age field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverAddress" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverAddress"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The Address field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverCity" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverCity"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The City field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverDistrict" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverDistrict"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The district field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverDetailDropDown" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="DriverDetailDropDown" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                    ControlToValidate="DriverDetailDropDown" ValidationGroup="UpdateDriverDetails" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Please select state"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverCoutry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverCoutry" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverCoutry"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The Country field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverMobileNumber" CssClass="col-md-3 control-label">Mobile Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverMobileNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverMobileNumber"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The Mobile Number field is required." />
                                <asp:RegularExpressionValidator runat="server" ControlToValidate="UpdateDriverMobileNumber"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$" ErrorMessage="Enter valid mobile number" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverEmailId" CssClass="col-md-3 control-label">Email Id</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverEmailId" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverEmailId"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The Email Id field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverDLNumber" CssClass="col-md-3 control-label">DL Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverDLNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverDLNumber"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The DL Number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverPhoto" CssClass="col-md-3 control-label">Photo</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="UpdateDriverPhoto" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverPanNumber" CssClass="col-md-3 control-label">Pan card Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverPanNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverPanNumber"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The Pan card number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateDriverAadharCardNumber" CssClass="col-md-3 control-label">Aadhar card number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateDriverAadharCardNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDriverAadharCardNumber"
                                    CssClass="text-danger" ValidationGroup="UpdateDriverDetails" Display="Dynamic" ErrorMessage="The Aadhar card number field is required." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button runat="server" ID="updateDriverDetailsButton" Text="Update" ValidationGroup="UpdateDriverDetails" OnClick="OnUpdateDriverDetails" CssClass="btn btn-default" />
                                    <asp:Button runat="server" ID="updateDriverDetailsButtonCancel" Text="Cancel" OnClick="OnUpdateDriverDetailsCancel" CausesValidation="False" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="tab-pane" id="disablevehicle">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Disable Vehicle</h4>
                        <asp:Panel runat="server" ID="disablePanel">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DisableVehicleId" CssClass="col-md-3 control-label">Vehicle number</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList Enabled="True" runat="server" ID="DisableVehicleId" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                    ControlToValidate="DisableVehicleId" ValidationGroup="DisableVehicleValidation" ValueToCompare="-- Select Vehicle No --" Operator="NotEqual" Type="String" ErrorMessage="Please select vehicle no."></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button ID="disableVehicleButtion" runat="server" Text="Disable" ValidationGroup="DisableVehicleValidation" OnClick="OnDisableVehicle" CssClass="btn btn-default" />
                                    <asp:Button ID="enbleVehicleButtion" runat="server" Text="Enable" ValidationGroup="DisableVehicleValidation" OnClick="OnEnableVehicle" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="tab-pane" id="updatevehicle">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Update Vehicle Details</h4>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="UpdateVehicleId" CssClass="col-md-3 control-label">Vehicle number</asp:Label>
                            <div class="col-md-5">
                                <asp:DropDownList AutoPostBack="True" runat="server" OnSelectedIndexChanged="OnVehicleIdSearch" ID="UpdateVehicleId" CssClass="form-control" Width="280px" />
                            </div>
                            <asp:CompareValidator runat="server" CssClass="text-danger"
                                ControlToValidate="UpdateVehicleId" ValidationGroup="UpdateVehicleDetails" ValueToCompare="-- Select Vehicle No --" Operator="NotEqual" Type="String" ErrorMessage="Select vehicle no"></asp:CompareValidator>
                        </div>
                        <asp:Panel runat="server" ID="UpdateVehiclePanel" Visible="false" DefaultButton="UpdateVehicleDetailsButton">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleGoodsType" CssClass="col-md-3 control-label">Goods Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="UpdateVehicleGoodsType" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                    ControlToValidate="UpdateVehicleGoodsType" ValidationGroup="UpdateVehicleDetails" ValueToCompare="--Select Goods Type--" Operator="NotEqual" Type="String" ErrorMessage="Please select goods type"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleTonnageCapacity" CssClass="col-md-3 control-label">Tonnage Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="UpdateVehicleTonnageCapacity" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                    ControlToValidate="UpdateVehicleTonnageCapacity" ValidationGroup="UpdateVehicleDetails" ValueToCompare="-- Select Range --" Operator="NotEqual" Type="String" ErrorMessage="Please select tonnage capacity"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleMakeoftheVehicle" CssClass="col-md-3 control-label">Make of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleMakeoftheVehicle" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleMakeoftheVehicle"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The Vehicle Make field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleModelofthevehicle" CssClass="col-md-3 control-label">Model of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleModelofthevehicle" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleModelofthevehicle"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The Vehicle Model field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleCC" CssClass="col-md-3 control-label">Cubic Capacity(CC)</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleCC" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleCC"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The Cubic Capacity field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleLength" CssClass="col-md-3 control-label">Length of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleLength" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleLength"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The vehicle lenght field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleWidth" CssClass="col-md-3 control-label">Width of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleWidth" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleWidth"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The vehicle dimension field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleBreadth" CssClass="col-md-3 control-label">Breadth of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleBreadth" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleBreadth"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The vehicle dimension field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleHeight" CssClass="col-md-3 control-label">Height of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleHeight" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleHeight"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The vehicle dimension field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleEngineNumber" CssClass="col-md-3 control-label">Engine Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleEngineNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleEngineNumber"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The engine number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleChassisNumber" CssClass="col-md-3 control-label">Chassis Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleChassisNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleChassisNumber"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The chassis number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleNOCFromRTO" CssClass="col-md-3 control-label">NOC from RTO</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="UpdateVehicleNOCFromRTO" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleNOCFromPolice" CssClass="col-md-3 control-label">NOC from Police</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="UpdateVehicleNOCFromPolice" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehiclePhoto" CssClass="col-md-3 control-label">Photo</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="UpdateVehiclePhoto" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleFCValidityFrom" CssClass="col-md-3 control-label">FC Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleFCValidityFrom" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleFCValidityFrom"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The FC validity from field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleFCValidityTo" CssClass="col-md-3 control-label">FC Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleFCValidityTo" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleFCValidityTo"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The FC validity to field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleOwnerName" CssClass="col-md-3 control-label">Owner Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleOwnerName" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleOwnerName"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The owner name field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleOwnerAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleOwnerAddress" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleOwnerAddress"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The address field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleCity" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleCity"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The city field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleDistrict" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleDistrict"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The district field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleDetailsDropDown" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="UpdateVehicleDetailsDropDown" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                    ControlToValidate="UpdateVehicleDetailsDropDown" ValidationGroup="UpdateVehicleDetails" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Please select state"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UpdateVehicleCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UpdateVehicleCountry" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateVehicleCountry"
                                    CssClass="text-danger" ValidationGroup="UpdateVehicleDetails" Display="Dynamic" ErrorMessage="The country field is required." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button runat="server" ID="UpdateVehicleDetailsButton" Text="Update" ValidationGroup="UpdateVehicleDetails" OnClick="OnUpdateVehicleDetails" CssClass="btn btn-default" />
                                    <asp:Button runat="server" ID="UpdateVehicleDetailsButtonCancel" Text="Cancel" OnClick="OnUpdateVehicleDetailsCancel"  CausesValidation="False" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="tab-pane" id="renewvehicle">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Vehicles to renew</h4>
                        <br />
                        <div class="form-group">
                            <asp:EntityDataSource ID="renewVehicleEntityDataSource" OnQueryCreated="renewVehicleEntityDataSource_OnQueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails" AutoGenerateOrderByClause="False" EntityTypeFilter="" />
                            <asp:GridView ID="renewVehicleGridview" runat="server" OnSelectedIndexChanged="OnRenewVehicleListGridChanged"
                                DataSourceID="renewVehicleEntityDataSource"
                                GridLines="none"
                                AllowPaging="true"
                                CssClass="mGrid"
                                PagerStyle-CssClass="pgr" OnPageIndexChanging="RenewVehicleGridView_OnPageIndexChanging"
                                AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="false" AllowSorting="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                                    <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="True" SortExpression="TicketId"></asp:BoundField>
                                    <asp:BoundField DataField="Registration" HeaderText="Vehicle number" ReadOnly="True" SortExpression="Registration"></asp:BoundField>
                                    <asp:BoundField DataField="DriverName" HeaderText="Driver Name" SortExpression="DriverName"></asp:BoundField>
                                    <asp:BoundField DataField="DriverAge" HeaderText="Driver Age" SortExpression="DriverAge"></asp:BoundField>
                                    <asp:BoundField DataField="DriverCity" HeaderText="Driver City" SortExpression="DriverCity"></asp:BoundField>
                                    <asp:BoundField DataField="DriverDistrict" HeaderText="Driver District" SortExpression="DriverDistrict"></asp:BoundField>
                                    <asp:BoundField DataField="DriverState" HeaderText="Driver State" SortExpression="DriverState"></asp:BoundField>
                                    <asp:BoundField DataField="DriverCountry" HeaderText="Driver Country" SortExpression="DriverCountry"></asp:BoundField>
                                    <asp:BoundField DataField="DriverDLNumber" HeaderText="Driver driving license number" SortExpression="DriverDLNumber"></asp:BoundField>
                                </Columns>
                            </asp:GridView>

                            <asp:LinkButton ID="lnkPrint" Visible="true" runat="server" ValidationGroup="renewvehicle" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument="renewVehicleGridview" OnCommand="PrintCurrentPage"></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton1" Visible="true" runat="server" ValidationGroup="renewvehicle" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument="renewVehicleGridview" OnClick="ExportCurrentpage"></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="viewvehicle">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Vehicles List</h4>
                        <br />
                        <div class="form-group">
                            <asp:EntityDataSource ID="ViewVehicleEntityDataSource" OnQueryCreated="ViewVehicleEntityDataSource_OnQueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails" />
                            <asp:GridView ID="ViewVehicleGridView" OnQueryCreated="OnViewBusinessAssociateNotification" OnLoad="OnViewVehicleSelectionChanged" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" ValidationGroup="viewvehicle" OnSelectedIndexChanged="OnViewVehicleListGridChanged" runat="server" AutoGenerateColumns="False"
                                DataSourceID="ViewVehicleEntityDataSource" OnPageIndexChanging="ViewVehicleGridView_OnPageIndexChanging" AllowPaging="True" AllowSorting="True" DataKeyNames="TicketId">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                                    <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="True" SortExpression="TicketId"></asp:BoundField>
                                    <asp:BoundField DataField="Registration" HeaderText="Vehicle number" ReadOnly="True" SortExpression="Registration"></asp:BoundField>
                                    <asp:BoundField DataField="DriverName" HeaderText="Driver Name" SortExpression="DriverName"></asp:BoundField>
                                    <asp:BoundField DataField="DriverAge" HeaderText="Driver Age" SortExpression="DriverAge"></asp:BoundField>
                                    <asp:BoundField DataField="DriverCity" HeaderText="Driver City" SortExpression="DriverCity"></asp:BoundField>
                                    <asp:BoundField DataField="DriverDistrict" HeaderText="Driver District" SortExpression="DriverDistrict"></asp:BoundField>
                                    <asp:BoundField DataField="DriverState" HeaderText="Driver State" SortExpression="DriverState"></asp:BoundField>
                                    <asp:BoundField DataField="DriverCountry" HeaderText="Driver Country" SortExpression="DriverCountry"></asp:BoundField>
                                    <asp:BoundField DataField="DriverDLNumber" HeaderText="Driver driving license number" SortExpression="DriverDLNumber"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:LinkButton ID="ExportViewVehicleLinkButton" Visible="true" runat="server" ValidationGroup="viewvehicle" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument="ViewVehicleGridView" OnCommand="PrintCurrentPage"></asp:LinkButton>
                            <asp:LinkButton ID="PrintViewVehicleLinkButton" Visible="true" runat="server" ValidationGroup="viewvehicle" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument="ViewVehicleGridView" OnClick="ExportCurrentpage"></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="viewaccount">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Account Details</h4>
                        <br />
                        <div class="form-group">
                            <asp:EntityDataSource ID="ViewAccountDetailsEntityDataSource" OnQueryCreated="ViewAccountDetailsEntityDataSource_QueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="AccountDetails" OrderBy="it.[UserId]"></asp:EntityDataSource>
                            <asp:GridView ID="ViewAccountDetailsGridView" runat="server"
                                AutoGenerateColumns="False"
                                GridLines="none"
                                AllowPaging="true"
                                CssClass="mGrid"
                                PagerStyle-CssClass="pgr"
                                AlternatingRowStyle-CssClass="alt"
                                DataSourceID="ViewAccountDetailsEntityDataSource" AllowSorting="True" EnableSortingAndPagingCallbacks="True">
                                <Columns>
                                    <asp:BoundField DataField="AccountId" HeaderText="Account Number" ReadOnly="True" SortExpression="AccountId"></asp:BoundField>
                                    <asp:BoundField DataField="Funds" HeaderText="Balance" ReadOnly="True" SortExpression="Funds"></asp:BoundField>
                                    <asp:BoundField DataField="Bank" HeaderText="Bank" ReadOnly="True" SortExpression="Bank"></asp:BoundField>
                                    <asp:BoundField DataField="UserId" HeaderText="UserId" ReadOnly="True" SortExpression="UserId"></asp:BoundField>
                                </Columns>
                            </asp:GridView>

                            <asp:LinkButton ID="viewAccountDetailsPrintLinkButton" Visible="true" runat="server" ValidationGroup="viewaccount" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='ViewAccountDetailsGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                            <asp:LinkButton ID="viewAccountDetailsExportLinkButton" Visible="true" runat="server" ValidationGroup="viewaccount" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='ViewAccountDetailsGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalDisableYesNo" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="YesNoUpdatePanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label runat="server" Text="Confirmation" Font-Bold="True" />
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" ForeColor="#3a87ad" Font-Size="20" Font-Bold="True" Width="100%">Do you want to disable vehicle?</asp:Label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" usesubmitbehavior="False" onserverclick="OnDisableConfirmationYesClick" data-dismiss="modal" aria-hidden="true">Yes</button>
                            <button class="btn btn-info" runat="server" usesubmitbehavior="False" data-dismiss="modal" aria-hidden="true">No</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="myModalTicketId" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="TicketUpdatePanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label runat="server" Text="Ticket Informations" Font-Bold="True" />
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label ID="GeneratedTicketIdLabel" runat="server" ForeColor="#3a87ad" Font-Size="20" Font-Bold="True" Width="100%" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" usesubmitbehavior="False" data-dismiss="modal" aria-hidden="true">Ok</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="viewVehicleListModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updateviewVehicleModel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="TicketDetails" runat="server" Text="Ticket Details"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketId" CssClass="col-md-3 control-label">Ticket Id</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketId" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketGoodsType" CssClass="col-md-3 control-label">Goods Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketGoodsType" CssClass="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketTonnage" CssClass="col-md-3 control-label">Tonnage Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketTonnage" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketMakeOftheVehicle" CssClass="col-md-3 control-label">Make of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketMakeOftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketModelOftheVehicle" CssClass="col-md-3 control-label">Model of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketModelOftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketCubicCapacity" CssClass="col-md-3 control-label">Cubic Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketCubicCapacity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketLengthoftheVehicle" CssClass="col-md-3 control-label">Length of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketLengthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketWidthoftheVehicle" CssClass="col-md-3 control-label">Width of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketWidthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketBreadthoftheVehicle" CssClass="col-md-3 control-label">Breadth of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketBreadthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketHeightoftheVehicle" CssClass="col-md-3 control-label">Height of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketHeightoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketRegistrationNumber" CssClass="col-md-3 control-label">Registration Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketRegistrationNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketEngineNumber" CssClass="col-md-3 control-label">Engine Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketEngineNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketChasisNumber" CssClass="col-md-3 control-label">Chassis Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketChasisNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketNOCRTO" CssClass="col-md-3 control-label">NOC from RTO</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="TicketNOCRTO" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketNOCPolice" CssClass="col-md-3 control-label">NOC from Police</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="TicketNOCPolice" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketFCValidityFrom" CssClass="col-md-3 control-label">FC Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketFCValidityFrom" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketFCValidity" CssClass="col-md-3 control-label">FC Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketFCValidity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketVehicleOwnerName" CssClass="col-md-3 control-label">Vehicle Owner Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketVehicleOwnerName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketAddress" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketCity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDistrict" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketState" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketState" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketCountry" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketInsuranceType" CssClass="col-md-3 control-label">Insurance Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketInsuranceType" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketInsuranceCompany" CssClass="col-md-3 control-label">Insurer Company</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketInsuranceCompany" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketValidityFrom" CssClass="col-md-3 control-label">Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketValidityFrom" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketValidityTo" CssClass="col-md-3 control-label">Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketValidityTo" CssClass="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverName" CssClass="col-md-3 control-label">Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverAge" CssClass="col-md-3 control-label">Age</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverAge" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverAddress" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverCity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverDistrict" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverState" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverState" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverCountry" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverDLNumber" CssClass="col-md-3 control-label">DL Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverDLNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverMobileNumber" CssClass="col-md-3 control-label">Mobile Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverMobileNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverEmailId" CssClass="col-md-3 control-label">Email</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverEmailId" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverPanNumber" CssClass="col-md-3 control-label">Pan card number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverPanNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverAadharCardNumber" CssClass="col-md-3 control-label">Aadhar Card Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverAadharCardNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="TicketApprove" class="btn btn-info" runat="server" UseSubmitBehavior="False" OnClick="OnViewVehiclePopUpOk" Text="Ok" data-dismiss="modal" aria-hidden="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="notificationTicketModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="notificationUpdatePanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="Label1" runat="server" Text="Ticket Details"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationTicketId" CssClass="col-md-3 control-label">Ticket Id</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="notificationTicketId" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationGoodsType" CssClass="col-md-3 control-label">Goods Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="notificationGoodsType" CssClass="form-control" Width="280px" />
                                    <asp:CompareValidator runat="server" CssClass="text-danger"
                                        ControlToValidate="notificationGoodsType" ValidationGroup="notificationTicketGroup" ValueToCompare="--Select Goods Type--" Operator="NotEqual" Type="String" ErrorMessage="Select goods type"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationTonnageCapacity" CssClass="col-md-3 control-label">Tonnage Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="notificationTonnageCapacity" CssClass="form-control" Width="280px" />

                                    <asp:CompareValidator runat="server" CssClass="text-danger"
                                        ControlToValidate="notificationTonnageCapacity" ValidationGroup="notificationTicketGroup" ValueToCompare="-- Select Range --" Operator="NotEqual" Type="String" ErrorMessage="Select tonnage capacity"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationMakeOftheVehicle" CssClass="col-md-3 control-label">Make of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationMakeOftheVehicle" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationMakeOftheVehicle"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationModelOftheVehicle" CssClass="col-md-3 control-label">Model of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationModelOftheVehicle" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationModelOftheVehicle"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationCubicCapacity" CssClass="col-md-3 control-label">Cubic Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationCubicCapacity" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationCubicCapacity"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationLengthsoftheVehicle" CssClass="col-md-3 control-label">Length of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationLengthsoftheVehicle" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationLengthsoftheVehicle"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationWidthsoftheVehicle" CssClass="col-md-3 control-label">Width of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationWidthsoftheVehicle" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationWidthsoftheVehicle"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationBreadthsoftheVehicle" CssClass="col-md-3 control-label">Breadth of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationBreadthsoftheVehicle" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationBreadthsoftheVehicle"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationHeigthsoftheVehicle" CssClass="col-md-3 control-label">Height of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationHeigthsoftheVehicle" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationHeigthsoftheVehicle"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationRegistrationNumber" CssClass="col-md-3 control-label">Registration Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationRegistrationNumber" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationRegistrationNumber"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationEngineNumber" CssClass="col-md-3 control-label">Engine Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationEngineNumber" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationEngineNumber"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationChasisNumber" CssClass="col-md-3 control-label">Chassis Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationChasisNumber" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationChasisNumber"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationNOCRTO" CssClass="col-md-3 control-label">NOC from RTO</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="notificationNOCRTO" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationNOCPolice" CssClass="col-md-3 control-label">NOC from Police</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="notificationNOCPolice" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationVehiclePhotoFront" CssClass="col-md-3 control-label">Vehicle Photo Front</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="notificationVehiclePhotoFront" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationVehiclePhotoBack" CssClass="col-md-3 control-label">Vehicle Photo Back</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="notificationVehiclePhotoBack" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationVehiclePhotoLeft" CssClass="col-md-3 control-label">Vehicle Photo Left</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="notificationVehiclePhotoLeft" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationVehiclePhotoRight" CssClass="col-md-3 control-label">Vehicle Photo Right</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="notificationVehiclePhotoRight" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationFCValidityFrom" CssClass="col-md-3 control-label">FC Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationFCValidityFrom" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationFCValidityFrom"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationFCValidity" CssClass="col-md-3 control-label">FC Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationFCValidity" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationFCValidity"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationVehicleOwnerName" CssClass="col-md-3 control-label">Owner Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationVehicleOwnerName" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationVehicleOwnerName"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationAddress" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationAddress"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationCity" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationCity"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDistrict" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDistrict"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationState" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="notificationState" CssClass="form-control" />

                                    <asp:CompareValidator ID="CompareValidator1" runat="server" CssClass="text-danger"
                                        ControlToValidate="notificationState" ValidationGroup="notificationTicketGroup" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Select state"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationCountry" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationCountry"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationInsuranceType" CssClass="col-md-3 control-label">Insurance Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationInsuranceType" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationInsuranceType"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationInsuranceCompany" CssClass="col-md-3 control-label">Insurer Company</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationInsuranceCompany" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationInsuranceCompany"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationValidityFrom" CssClass="col-md-3 control-label">Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationValidityFrom" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationValidityFrom"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationValidityTo" CssClass="col-md-3 control-label">Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationValidityTo" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationValidityTo"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverName" CssClass="col-md-3 control-label">Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverName" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverName"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverAge" CssClass="col-md-3 control-label">Age</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverAge" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverAge"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="DriverAge" CssClass="text-danger" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverAddress" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverAddress"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverCity" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverCity"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverDistrict" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverDistrict"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverState" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="notificationDriverState" CssClass="form-control" />

                                    <asp:CompareValidator ID="CompareValidator2" runat="server" CssClass="text-danger"
                                        ControlToValidate="notificationDriverState" ValidationGroup="notificationTicketGroup" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Select state"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverCountry" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverCountry"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverDLNumber" CssClass="col-md-3 control-label">DL Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverDLNumber" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverDLNumber"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverMobileNumber" CssClass="col-md-3 control-label">Mobile Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverMobileNumber" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverMobileNumber"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                    <asp:RegularExpressionValidator runat="server" ControlToValidate="notificationDriverMobileNumber"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$" ErrorMessage="Enter valid mobile number" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverEmailId" CssClass="col-md-3 control-label">Email</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverEmailId" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverEmailId"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                    <asp:RegularExpressionValidator CssClass="text-danger" runat="server" ControlToValidate="DriverEmailId" ValidationExpression="^(.+)@([^\.].*)\.([a-z]{2,})$" Text="Enter a valid email" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverPanNumber" CssClass="col-md-3 control-label">Pan card number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverPanNumber" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverPanNumber"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="notificationDriverAadharCardNumber" CssClass="col-md-3 control-label">Adhar Card Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="notificationDriverAadharCardNumber" CssClass="form-control" />

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="notificationDriverAadharCardNumber"
                                        CssClass="text-danger" ValidationGroup="notificationTicketGroup" Display="Dynamic" ErrorMessage="Mandatory field." />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button class="btn btn-info" runat="server" UseSubmitBehavior="False" OnClick="OnNotificationReSubmit" Text="Re Submit" data-dismiss="modal" aria-hidden="true" />
                            <asp:Button class="btn btn-info" runat="server" UseSubmitBehavior="False" Text="Cancel" data-dismiss="modal" aria-hidden="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="myModalEnableYesNo" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="disableUpdatePanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label runat="server" Text="Confirmation" Font-Bold="True" />
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" ForeColor="#3a87ad" Font-Size="20" Font-Bold="True" Width="100%">Do you want to enable vehicle?</asp:Label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" usesubmitbehavior="False" onserverclick="OnEnableConfirmationYesClick" data-dismiss="modal" aria-hidden="true">Yes</button>
                            <button class="btn btn-info" runat="server" usesubmitbehavior="False" data-dismiss="modal" aria-hidden="true">No</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    
    <asp:HiddenField ID="selected_tab" runat="server" />
    <asp:HiddenField ID="customeSelection_tab" runat="server" />
    <!-- Enable the tabs -->
    <script type="text/javascript">
        $('#myTabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $(function () {
            var tabName = $("[id*=selected_tab]").val() != "" ? $("[id*=selected_tab]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=selected_tab]").val($(this).attr("href").replace("#", ""));                
            });
        });
    </script>


</asp:Content>
