﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Dashboard.Account.Profile" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>Profile Details</h2>
    <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
        <p class="text-success"><%: SuccessMessage %></p>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
        <p class="text-danger"><%: FailureMessage %></p>
    </asp:PlaceHolder>
    <div class="row" id="Tabs">
        <!-- Navigation Buttons -->
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" id="myTabs">
                <li class="active"><a href="#Personal">Personal Details</a></li>
                <li><a href="#Company" data-toggle="tab">Company Details</a></li>
                <li><a href="#Bank" data-toggle="tab">Bank Details</a></li>
                <li><a href="#Document" data-toggle="tab">Document Details</a></li>
            </ul>
        </div>
        <!-- Content -->
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="Personal">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Personal details.</h4>
                        <hr />
                        <asp:Panel ID="personalPanel" runat="server" DefaultButton="personalDetailsUpdateButton">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Name</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdatePersonalGroup" runat="server" ID="Name" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="Name"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The name field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-2 control-label">Address</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdatePersonalGroup" runat="server" ID="Address" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="Address"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The address field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-2 control-label">City</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdatePersonalGroup" runat="server" ID="City" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="City"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The city field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="District" CssClass="col-md-2 control-label">District</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdatePersonalGroup" runat="server" ID="District" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="District"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The district field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-2 control-label">State</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList ValidationGroup="UpdatePersonalGroup" runat="server" ID="State" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator ValidationGroup="UpdatePersonalGroup" ID="StateComapareValidator" runat="server" CssClass="text-danger"
                                    ControlToValidate="State" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Please select State"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Country" CssClass="col-md-2 control-label">Country</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ValidationGroup="UpdatePersonalGroup" ID="Country" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="Country"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The country field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Age" CssClass="col-md-2 control-label">Age</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="Age" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="Age"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The age field is required." />
                                <asp:RegularExpressionValidator ValidationGroup="UpdatePersonalGroup" ID="RegularExpressionValidatorAge" ControlToValidate="Age" CssClass="text-danger" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="EmailId" CssClass="col-md-2 control-label">Email Id</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="EmailId" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="EmailId"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The email id field is required." />
                                <asp:RegularExpressionValidator ValidationGroup="UpdatePersonalGroup" ID="regEmail" CssClass="text-danger" runat="server" ControlToValidate="EmailId" ValidationExpression="^(.+)@([^\.].*)\.([a-z]{2,})$" Text="Enter a valid email" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="MobileNumber" CssClass="col-md-2 control-label">Mobile Number</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="MobileNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="MobileNumber"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Mobile Number field is required." />
                                <asp:RegularExpressionValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="MobileNumber"
                                    CssClass="text-danger" Display="Dynamic" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$" ErrorMessage="Enter valid mobile number" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PanCard" CssClass="col-md-2 control-label">Pan card number</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="PanCard" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="PanCard"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The pan card field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="AadharCardNumber" CssClass="col-md-2 control-label">Aadhar Card Number</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdatePersonalGroup" runat="server" ID="AadharCardNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdatePersonalGroup" runat="server" ControlToValidate="AadharCardNumber"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Aadhar Card number field is required." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <asp:Button runat="server" ID="personalDetailsUpdateButton" OnClick="OnUpdatePersonalDetails" ValidationGroup="UpdatePersonalGroup" Text="Update" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>

                <div class="tab-pane" id="Company">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Company details.</h4>
                        <hr />
                        <asp:Panel ID="companyPanel" runat="server" DefaultButton="companyDetailsUpdateButton">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NameoftheFirm" CssClass="col-md-2 control-label">Name of the Firm</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdateCompanyGroup" runat="server" ID="NameoftheFirm" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdateCompanyGroup" runat="server" ControlToValidate="NameoftheFirm"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Name of the Firm field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FirmType" CssClass="col-md-2 control-label">Firm Type</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdateCompanyGroup" runat="server" ID="FirmType" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdateCompanyGroup" runat="server" ControlToValidate="FirmType"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Firm Type field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TelephoneNumber" CssClass="col-md-2 control-label">Telephone Number</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdateCompanyGroup" runat="server" ID="TelephoneNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdateCompanyGroup" runat="server" ControlToValidate="TelephoneNumber"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Telephone Number field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-2 control-label">Registration Number</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdateCompanyGroup" runat="server" ID="RegistrationNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdateCompanyGroup" runat="server" ControlToValidate="RegistrationNumber"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Registration Number is required." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <asp:Button runat="server" ID="companyDetailsUpdateButton" OnClick="OnUpdateCompanyDetails" ValidationGroup="UpdateCompanyGroup" Text="Update" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="tab-pane" id="Bank">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <asp:Panel ID="bankPanel" runat="server" DefaultButton="bankDetailsUpdateButton">
                            <h4>Bank details.</h4>
                            <hr />
                              <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="AccountNumber" CssClass="col-md-2 control-label">Account number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="AccountNumber" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="AccountNumber" ValidationGroup="UpdateBankGroup"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Name of the Bank field is required." />
                                </div>
                            </div>
                         <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NameoftheBank" CssClass="col-md-2 control-label">Bank Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="NameoftheBank" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator ValidationGroup="UpdateBankGroup" runat="server" CssClass="text-danger"
                                    ControlToValidate="NameoftheBank" ValueToCompare="-- Select Bank --" Operator="NotEqual" Type="String" ErrorMessage="Please select bank"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="BankAddress" CssClass="col-md-2 control-label">Bank Address</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdateBankGroup" runat="server" ID="BankAddress" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="UpdateBankGroup" runat="server" ControlToValidate="BankAddress"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Bank Address field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="MICRCode" CssClass="col-md-2 control-label">MICR Code</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdateBankGroup" runat="server" ID="MICRCode" CssClass="form-control" />
                                </div>
                                <asp:RegularExpressionValidator ValidationGroup="UpdateBankGroup" ControlToValidate="MICRCode" CssClass="text-danger" runat="server" ErrorMessage="Enter valid MICR code" ValidationExpression="\d+" />
                                <asp:RequiredFieldValidator ValidationGroup="UpdateBankGroup" runat="server" ControlToValidate="MICRCode"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The MICR Code field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="IFSCCode" CssClass="col-md-2 control-label">IFSC Code</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox ValidationGroup="UpdateBankGroup" runat="server" ID="IFSCCode" CssClass="form-control" />
                                </div>
                                <asp:RegularExpressionValidator ValidationGroup="UpdateBankGroup" runat="server" ControlToValidate="IFSCCode"
                                    CssClass="text-danger" Display="Dynamic" ValidationExpression="^[^\s]{4}\d{7}$" ErrorMessage="Enter valid IFSC code" />
                                <asp:RequiredFieldValidator ValidationGroup="UpdateBankGroup" runat="server" ControlToValidate="IFSCCode"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The IFSC Code Number is required." />
                            </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <asp:Button runat="server" ID="bankDetailsUpdateButton" OnClick="OnUpdateBankDetails" ValidationGroup="UpdateBankGroup" Text="UPdate" CssClass="btn btn-default" />
                        </div>
                    </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="tab-pane" id="Document">
                <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                <div class="form-horizontal">
                    <h4>Document details.</h4>
                    <hr />
                    <asp:Panel ID="documentPanel" runat="server" DefaultButton="documentUpdateButton">
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="PanCardProof" CssClass="col-md-2 control-label">Pan card proof</asp:Label>
                            <asp:CheckBox runat="server" ID="PanCardProof" CssClass="form-control" Width="40px" />
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="AadhrarCardProof" CssClass="col-md-2 control-label">Aadhar card proof</asp:Label>
                            <asp:CheckBox runat="server" ID="AadhrarCardProof" TextMode="Password" CssClass="form-control" Width="40px" />
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="CompanyRegistrationProof" CssClass="col-md-2 control-label">Company registration proof</asp:Label>
                            <asp:CheckBox runat="server" ID="CompanyRegistrationProof" TextMode="Password" CssClass="form-control" Width="40px" />
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="CompanyAddressProof" CssClass="col-md-2 control-label">Company address proof</asp:Label>
                            <asp:CheckBox runat="server" ID="CompanyAddressProof" TextMode="Password" CssClass="form-control" Width="40px" />
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="MobileBill" CssClass="col-md-2 control-label">Mobile bill</asp:Label>
                            <asp:CheckBox runat="server" ID="MobileBill" TextMode="Password" CssClass="form-control" Width="40px" />
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="OfficeRentalDocument" CssClass="col-md-2 control-label">Office rental document</asp:Label>
                            <asp:CheckBox runat="server" ID="OfficeRentalDocument" TextMode="Password" CssClass="form-control" Width="40px" />
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="PartnerAddressProof" CssClass="col-md-2 control-label">Office rental document</asp:Label>
                            <asp:CheckBox runat="server" ValidationGroup="UpdateDocumentGroup" ID="PartnerAddressProof" TextMode="Password" CssClass="form-control" Width="40px" />
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" ID="documentUpdateButton" OnClick="OnUpdateDocumentDetails" ValidationGroup="UpdateDocumentGroup" Text="Update" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
    </div>
    <asp:HiddenField ID="selected_tab" runat="server" />
    <!-- Enable the tabs -->
    <script type="text/javascript">
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
        $(function () {
            var tabName = $("[id*=selected_tab]").val() != "" ? $("[id*=selected_tab]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=selected_tab]").val($(this).attr("href").replace("#", ""));
            });
        });
    </script>

</asp:Content>
