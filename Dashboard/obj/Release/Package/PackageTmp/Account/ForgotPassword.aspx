﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="Dashboard.Account.ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3> &nbsp;</h3>
    <h3> Forgot Password ? </h3>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:UpdatePanel runat="server" ID="MessagePanel">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-success"><%: SuccessMessage %></p>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-danger"><%: FailureMessage %></p>
            </asp:PlaceHolder>
            <asp:Timer runat="server" Interval="1000" OnTick="MessagePanelClick"></asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="form-group">

        <asp:Label ID="lblEmail" runat="server" Text="Enter UserName" CssClass="col-md-2 control-label" />
        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" />
        <asp:Button ID="btnPass" runat="server" CssClass="btn btn-default" Text="Submit" OnClick="btnPass_Click" />
    </div>


</asp:Content>
