﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SH.aspx.cs" Inherits="Dashboard.Account.SH" EnableEventValidation="True" %>

<%@ Register TagPrefix="cc1" Namespace="DatePickerControl" Assembly="DatePickerControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h6></h6>
    <%--    <div class="rowHeader">
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-2">
            <asp:HyperLink runat="server" Text="Create User" NavigateUrl="~/Account/Register"></asp:HyperLink>
        </div>
    </div>--%>
    <h3><%: Title %></h3>

    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:UpdatePanel runat="server" ID="MessagePanel">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-success"><%: SuccessMessage %></p>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-danger"><%: FailureMessage %></p>
            </asp:PlaceHolder>
            <asp:Timer runat="server" Interval="1000" OnTick="MessagePanelClick"></asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="row" id="Tabs">
        <!-- Navigation Buttons -->
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" id="myTabs">
                <li class="active"><a href="#Notification">Notification</a></li>
                <li><a href="#ViewDH">View Team</a></li>
                <%--<li><a href="#ViewBA">View Business Associates</a></li>--%>
                <%--<li><a href="#ViewSP">View Service Partners</a></li>--%>
                <%--<li><a href="#Commission">View Commission Report</a></li>--%>
                <%--<li><a href="#ViewRemainderList">View Remainder List</a></li>--%>
                <li><a href="#Viewcompliantreport">View Complaint Report</a></li>
                <li><a href="#Commission1">View Team Commission Report</a></li>
            </ul>
        </div>
        <!-- Content -->
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="Notification">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <br />
                        <h4>Pending notifications</h4>
                        <br />
                        <asp:Panel ID="pendingNotificationPanel" runat="server" Visible="True">
                            <asp:Label runat="server" Text="No pending notification" CssClass="label-info"></asp:Label>
                        </asp:Panel>
                        <asp:EntityDataSource ID="TicketEntityDataSource" OnQueryCreated="TicketEntityDataSource_OnQueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails"></asp:EntityDataSource>
                        <asp:GridView ID="TicketGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" ValidationGroup="Viewticketnotification" OnSelectedIndexChanged="OnTicketNotificationGridChanged" runat="server" AutoGenerateColumns="False" DataSourceID="TicketEntityDataSource" DataKeyNames="TicketId">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ValidationGroup="ApproveVehicleGroup"></asp:CommandField>
                                <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="True" SortExpression="TicketId"></asp:BoundField>
                                <asp:BoundField DataField="Registration" HeaderText="Vehicle Number" SortExpression="Registration"></asp:BoundField>
                                <asp:BoundField DataField="DHPermission" HeaderText="DH Approval" SortExpression="DHPermission"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:LinkButton ID="PrintLinkButton" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='TicketGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="ExportLinkButton" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='TicketGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>
                        <br />
                        <h4>Rejected notifications</h4>
                        <br />
                        <asp:Panel ID="rejectedNotificationPanel" runat="server" Visible="True">
                            <asp:Label runat="server" Text="No rejected notification" CssClass="label-info"></asp:Label>
                        </asp:Panel>
                        <asp:EntityDataSource ID="RejectedEntityDataSource" OnQueryCreated="TicketEntityDataSource_OnRejectQueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails"></asp:EntityDataSource>
                        <asp:GridView ID="RejectedGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" ValidationGroup="Viewticketnotification" OnSelectedIndexChanged="OnTicketRejected" runat="server" AutoGenerateColumns="False" DataSourceID="RejectedEntityDataSource" DataKeyNames="TicketId">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ValidationGroup="ApproveVehicleGroup"></asp:CommandField>
                                <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="True" SortExpression="TicketId"></asp:BoundField>
                                <asp:BoundField DataField="Registration" HeaderText="Vehicle Number" SortExpression="Registration"></asp:BoundField>
                                <asp:BoundField DataField="BAPermission" HeaderText="BA Approval" SortExpression="BAPermission"></asp:BoundField>
                                <asp:BoundField DataField="DHPermission" HeaderText="DH Approval" SortExpression="DHPermission"></asp:BoundField>
                                <asp:BoundField DataField="SHPermission" HeaderText="SH Approval" SortExpression="SHPermission"></asp:BoundField>
                            </Columns>

                        </asp:GridView>
                        <asp:LinkButton ID="PrintRejectLinkButton" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='RejectedGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="ExportRejectLinkButton" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='RejectedGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>
                    </div>
                </div>
                <div class="tab-pane" id="ViewDH">
                    <h4>View District Head</h4>
                    <br />
                    <asp:EntityDataSource ID="ViewDHEntityDataSource" runat="server" OnQueryCreated="OnViewDistrictHeads" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="PersonalDetails">
                    </asp:EntityDataSource>
                    <asp:GridView ID="ViewDHGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" OnSelectedIndexChanged="ViewDHGridView_OnSelectedIndexChanged" AlternatingRowStyle-CssClass="alt" runat="server" AutoGenerateColumns="False" DataSourceID="ViewDHEntityDataSource" DataKeyNames="PersonalId" AllowPaging="True" AllowSorting="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" SelectText="View Business Associates"></asp:CommandField>
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City"></asp:BoundField>
                            <asp:BoundField DataField="District" HeaderText="District" SortExpression="District"></asp:BoundField>
                            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State"></asp:BoundField>
                            <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId"></asp:BoundField>
                        </Columns>
                    </asp:GridView>

                    <asp:LinkButton ID="viewDHPrintLinkButton" Visible="true" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='ViewDHGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                    <asp:LinkButton ID="viewDHExportLinkButton" Visible="true" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='ViewDHGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>
                    <br />
                    <h4>View Business Associates</h4>
                    <br />
                    <asp:EntityDataSource ID="ViewBAEntityDataSource" OnQueryCreated="ViewBAEntityDataSource_OnQueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="PersonalDetails">
                    </asp:EntityDataSource>
                    <asp:GridView ID="ViewBAGridView" CssClass="mGrid" OnSelectedIndexChanging="ViewBAGridView_OnSelectedIndexChanging" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" runat="server" AutoGenerateColumns="False" DataSourceID="ViewBAEntityDataSource" DataKeyNames="PersonalId" AllowPaging="True" AllowSorting="True">
                        <Columns>

                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City"></asp:BoundField>
                            <asp:BoundField DataField="District" HeaderText="District" SortExpression="District"></asp:BoundField>
                            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State"></asp:BoundField>
                            <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId"></asp:BoundField>
                        </Columns>
                    </asp:GridView>

                    <asp:LinkButton ID="viewBAPrintLinkButton" Visible="False" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='ViewBAGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                    <asp:LinkButton ID="viewBAExportLinkButton" Visible="False" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='ViewBAGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>
                </div>

                <%-- <div class="tab-pane" id="ViewSP">
                    <h1>View Service Partners</h1>
                    <br />
                    <asp:EntityDataSource ID="EntityDataSource5" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="CompanyDetails"></asp:EntityDataSource>
                    <asp:GridView ID="GridView5" CssClass="mGrid" runat="server" AutoGenerateColumns="False" DataKeyNames="CompanyId" DataSourceID="EntityDataSource5" AllowPaging="True" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="CompanyId" HeaderText="CompanyId" ReadOnly="True" SortExpression="CompanyId"></asp:BoundField>
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                            <asp:BoundField DataField="FirmType" HeaderText="FirmType" SortExpression="FirmType"></asp:BoundField>
                            <asp:BoundField DataField="TelephoneNumber" HeaderText="TelephoneNumber" SortExpression="TelephoneNumber"></asp:BoundField>
                            <asp:BoundField DataField="RegistrationNumber" HeaderText="RegistrationNumber" SortExpression="RegistrationNumber"></asp:BoundField>
                            <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId"></asp:BoundField>
                        </Columns>
                  </asp:GridView>
                    <asp:LinkButton Visible="true" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='GridView5' OnCommand="PrintCurrentPage"></asp:LinkButton>
                    <asp:LinkButton Visible="true" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='GridView5' OnCommand="ExportCurrentpage"></asp:LinkButton>

                </div>--%>
                <%--<div class="tab-pane" id="Commission">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Commission Report</h4>
                    </div>
                </div>--%>
                <%--<div class="tab-pane" id="ViewBA">
                    <h4>View Business Associate</h4>
                    <br />
                    <asp:EntityDataSource ID="EntityDataSource4" OnQueryCreated="OnViewBusinessAssociate" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="PersonalDetails" Where="it.EmployeeType='Business Associate'">
                    </asp:EntityDataSource>
                    <asp:GridView ID="GridView4" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" runat="server" AutoGenerateColumns="False" DataSourceID="EntityDataSource4" DataKeyNames="PersonalId" AllowPaging="True" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="PersonalId" HeaderText="PersonalId" ReadOnly="True" SortExpression="PersonalId"></asp:BoundField>
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address"></asp:BoundField>
                            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City"></asp:BoundField>
                            <asp:BoundField DataField="District" HeaderText="District" SortExpression="District"></asp:BoundField>
                            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State"></asp:BoundField>
                            <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country"></asp:BoundField>
                            <asp:BoundField DataField="Age" HeaderText="Age" SortExpression="Age"></asp:BoundField>
                            <asp:BoundField DataField="EmailId" HeaderText="EmailId" SortExpression="EmailId"></asp:BoundField>
                            <asp:BoundField DataField="MobileNumber" HeaderText="MobileNumber" SortExpression="MobileNumber"></asp:BoundField>
                            <asp:BoundField DataField="PanCard" HeaderText="PanCard" SortExpression="PanCard"></asp:BoundField>
                            <asp:BoundField DataField="AadharCardNumber" HeaderText="AadharCardNumber" SortExpression="AadharCardNumber"></asp:BoundField>
                            <asp:BoundField DataField="EmployeeType" HeaderText="EmployeeType" SortExpression="EmployeeType"></asp:BoundField>
                            <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <asp:LinkButton Visible="true" runat="server" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='GridView4' OnCommand="PrintCurrentPage"></asp:LinkButton>
                    <asp:LinkButton Visible="true" runat="server" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='GridView4' OnCommand="ExportCurrentpage"></asp:LinkButton>

                </div>--%>

                <%--<div class="tab-pane" id="ViewRemainderList">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Remainder List</h4>
                        <br />
                        <asp:EntityDataSource ID="EntityDataSource8" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="AccountDetails" EnableDelete="True" EnableInsert="True" EnableUpdate="True"></asp:EntityDataSource>
                        <asp:GridView ID="GridView8" CssClass="mGrid"  PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" runat="server" DataSourceID="EntityDataSource8" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="AccountId">
                            <Columns>
                                <asp:BoundField DataField="AccountId" HeaderText="AccountId" ReadOnly="True" SortExpression="AccountId"></asp:BoundField>
                                <asp:BoundField DataField="Funds" HeaderText="Funds" SortExpression="Funds"></asp:BoundField>
                                <asp:BoundField DataField="Bank" HeaderText="Bank" SortExpression="Bank"></asp:BoundField>
                                <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId"></asp:BoundField>
                            </Columns>

                        </asp:GridView>

                        <asp:LinkButton Visible="true" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='GridView8' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton Visible="true" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='GridView8' OnCommand="ExportCurrentpage"></asp:LinkButton>

                    </div>
                </div>--%>

                <div class="tab-pane" id="Viewcompliantreport">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Compliant Report</h4>
                    </div>
                </div>

                <div class="tab-pane" id="Commission1">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Team Commission Report</h4>
                        <br />
                        <div class="form-group">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="totalCommission">Start Date  </asp:Label>
                                <cc1:datepicker dateformat="dd/MM/yyyy" id="DatePickerstart" width="150px" runat="server" />
                                <asp:Label runat="server" AssociatedControlID="totalCommission">End Date  </asp:Label>
                                <cc1:datepicker dateformat="dd/MM/yyyy" id="DatePickerEnd" width="150px" runat="server" />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button runat="server" ID="commissionButton" Text="Submit" OnClick="commissionButton_OnClick" CssClass="btn btn-default" />
                                </div>
                            </div>
                            <br />
                            <asp:GridView ID="ComissionGrid" runat="server" PagerStyle-CssClass="pgr" CssClass="mGrid" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="DHName" HeaderText="DH Name" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="DHCity" HeaderText="DH City" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="DHMobileNumber" HeaderText="DH MobileNumber" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" ReadOnly="True"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:LinkButton ID="commissionPrintLinkButton" Visible="False" runat="server" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='ComissionGrid' OnCommand="PrintCurrentPage"></asp:LinkButton>
                            <asp:LinkButton ID="commissionExportLinkButton" Visible="False" runat="server" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='ComissionGrid' OnCommand="ExportCurrentpage"></asp:LinkButton>
                        </div>
                        <br />
                        <asp:Panel runat="server" Visible="False" ID="totalCommissionPanel">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="totalCommission" CssClass="col-md-3 control-label">Total Commission: </asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox ReadOnly="True" runat="server" ID="totalCommission" CssClass="form-control" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <asp:HiddenField ID="selected_tab" runat="server" />
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="lblModalTitle" runat="server" Text="Comment"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:TextBox TextMode="MultiLine" Width="100%" ID="Comments" runat="server"></asp:TextBox>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" onserverclick="ApprovalComments" data-dismiss="modal" aria-hidden="true">Ok</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="ticketModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updateTicketModel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="TicketDetails" runat="server" Text="Ticket Details"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketId" CssClass="col-md-3 control-label">Ticket Id</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketId" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="GoodsType" CssClass="col-md-3 control-label">Goods Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="GoodsType" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Tonnage" CssClass="col-md-3 control-label">Tonnage Range</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="Tonnage" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="MakeOftheVehicle" CssClass="col-md-3 control-label">Make of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="MakeOftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ModelOftheVehicle" CssClass="col-md-3 control-label">Model of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="ModelOftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="CubicCapacity" CssClass="col-md-3 control-label">Cubic Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="CubicCapacity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="LengthoftheVehicle" CssClass="col-md-3 control-label">Length of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="LengthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="WidthoftheVehicle" CssClass="col-md-3 control-label">Width of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="WidthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="BreadthoftheVehicle" CssClass="col-md-3 control-label">Breadth of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="BreadthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="HeightoftheVehicle" CssClass="col-md-3 control-label">Height of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="HeightoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-3 control-label">Registration Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="RegistrationNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="EngineNumber" CssClass="col-md-3 control-label">Engine Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="EngineNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ChasisNumber" CssClass="col-md-3 control-label">Chassis Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="ChasisNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NOCRTO" CssClass="col-md-3 control-label">NOC from RTO</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="NOCRTO" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NOCPolice" CssClass="col-md-3 control-label">NOC from Police</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="NOCPolice" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoFront" CssClass="col-md-3 control-label">Vehicle Photo Front</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="VehiclePhotoFront" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoBack" CssClass="col-md-3 control-label">Vehicle Photo Back</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="VehiclePhotoBack" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoLeft" CssClass="col-md-3 control-label">Vehicle Photo Left</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="VehiclePhotoLeft" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoRight" CssClass="col-md-3 control-label">Vehicle Photo Right</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="VehiclePhotoRight" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FCValidityFrom" CssClass="col-md-3 control-label">FC Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="FCValidityFrom" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FCValidity" CssClass="col-md-3 control-label">FC Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="FCValidity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehicleOwnerName" CssClass="col-md-3 control-label">Vehicle Owner Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="VehicleOwnerName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="Address" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="City" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="District" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="District" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="State" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Country" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="Country" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="InsuranceType" CssClass="col-md-3 control-label">Insurance Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="InsuranceType" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="InsuranceCompany" CssClass="col-md-3 control-label">Insurer Company</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="InsuranceCompany" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ValidityFrom" CssClass="col-md-3 control-label">Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="ValidityFrom" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ValidityTo" CssClass="col-md-3 control-label">Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="ValidityTo" CssClass="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverName" CssClass="col-md-3 control-label">Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverAge" CssClass="col-md-3 control-label">Age</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverAge" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverAddress" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverCity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverDistrict" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverState" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverState" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverCountry" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverPhoto" CssClass="col-md-3 control-label">Vehicle Photo Right</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="DriverPhoto" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverDLNumber" CssClass="col-md-3 control-label">DL Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverDLNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverMobileNumber" CssClass="col-md-3 control-label">Mobile Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverMobileNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverEmailId" CssClass="col-md-3 control-label">Email</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverEmailId" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverPanNumber" CssClass="col-md-3 control-label">Pan card number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverPanNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverAadharCardNumber" CssClass="col-md-3 control-label">Adhar Card Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverAadharCardNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="TicketApprove" class="btn btn-info" runat="server" UseSubmitBehavior="false" OnClick="OnApproveClick" Text="Approve" data-dismiss="modal" aria-hidden="true" />
                            <asp:Button ID="TicketReject" class="btn btn-info" runat="server" UseSubmitBehavior="false" OnClick="OnRejectClick" Text="Reject" data-dismiss="modal" aria-hidden="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <!-- Enable the tabs -->
    <script type="text/javascript">
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        $(function () {
            var tabName = $("[id*=selected_tab]").val() != "" ? $("[id*=selected_tab]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=selected_tab]").val($(this).attr("href").replace("#", ""));
            });
        });
    </script>
</asp:Content>
