﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SA.aspx.cs" Inherits="Dashboard.Account.SA" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h6></h6>
    <h3><%: Title %></h3>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:UpdatePanel runat="server" ID="MessagePanel">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-success"><%: SuccessMessage %></p>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-danger"><%: FailureMessage %></p>
            </asp:PlaceHolder>
            <asp:Timer runat="server" Interval="1000" OnTick="MessagePanelClick"></asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="row" id="Tabs">
        <!-- Navigation Buttons -->
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" id="myTabs">
                <li class="active"><a href="#Notification">Notification</a></li>
                <li><a href="#createUser" data-toggle="tab" runat="server">Create user</a></li>
                <li><a href="#pendingTicketList" data-toggle="tab" runat="server">Pending vehicle addition</a></li>
                <li><a href="#ViewEmployees" data-toggle="tab" runat="server">View Employees</a></li>
                <li><a href="#Changecommission" data-toggle="tab" runat="server">Change commission in (%)</a></li>
                <li><a href="#ChangeRenewalAmount" data-toggle="tab" runat="server">Change Tonnage Cost</a></li>
                <li><a href="#SendBulkSMS" data-toggle="tab" runat="server">Send Bulk Notification</a></li>
                <li><a href="#CommissionUploadFile" data-toggle="tab" runat="server">Generate Settlement Report</a></li>
                <li><a href="#TDSAmount" data-toggle="tab" runat="server">TDS Settlement Report</a></li>
            </ul>
        </div>
        <!-- Content -->
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="Notification">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Notifications</h4>
                        <br />
                        <asp:Panel ID="pendingNotificationPanel" runat="server" Visible="True">
                            <asp:Label runat="server" Text="No pending notification" CssClass="label-info"></asp:Label>
                        </asp:Panel>
                        <asp:EntityDataSource ID="TicketEntityDataSource" OnQueryCreated="TicketEntityDataSource_QueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails"></asp:EntityDataSource>
                        <asp:GridView ID="TicketGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" ValidationGroup="ViewNotificationGroup" OnSelectedIndexChanged="OnTicketNotificationGridChanged" runat="server" AutoGenerateColumns="False" DataSourceID="TicketEntityDataSource" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ValidationGroup="ViewNotificationGroup"></asp:CommandField>
                                <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="True" SortExpression="TicketId"></asp:BoundField>
                                <asp:BoundField DataField="Registration" HeaderText="Vehicle Number" SortExpression="Registration"></asp:BoundField>
                                <asp:BoundField DataField="BAPermission" HeaderText="BA Approval" SortExpression="BAPermission"></asp:BoundField>
                                <asp:BoundField DataField="DHPermission" HeaderText="DH Approval" SortExpression="DHPermission"></asp:BoundField>
                                <asp:BoundField DataField="SHPermission" HeaderText="SH Approval" SortExpression="SHPermission"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:LinkButton ID="PrintLinkButton" Visible="true" runat="server" ValidationGroup="ViewNotificationGroup" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='TicketGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="ExportLinkButton" Visible="true" runat="server" ValidationGroup="ViewNotificationGroup" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='TicketGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>

                    </div>
                </div>

                <div class="tab-pane" id="createUser">
                    <asp:Panel runat="server" ID="RegisterPanel" DefaultButton="registerNextButton">
                        <div class="form-horizontal">
                            <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                            <h4>Create a new account.</h4>
                            <hr />
                            <asp:ValidationSummary runat="server" CssClass="text-danger" />
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-2 control-label">User name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName" ValidationGroup="RegisterGroup"
                                    CssClass="text-danger" ErrorMessage="The user name field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" ValidationGroup="RegisterGroup"
                                    CssClass="text-danger" ErrorMessage="The password field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword" ValidationGroup="RegisterGroup"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" ValidationGroup="RegisterGroup"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-5">
                                    <asp:Button ID="registerNextButton" runat="server" OnClick="OnRegister" ValidationGroup="RegisterGroup" Text="Next" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" Visible="False" ID="EmployeeTypePanel">
                        <asp:UpdatePanel runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel runat="server" DefaultButton="employeeTypeNext">
                                    <div class="form-horizontal">
                                        <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                                        <h4>Employee Type.</h4>
                                        <hr />
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="EmployeeType" CssClass="col-md-2 control-label">Employee Type</asp:Label>
                                            <div class="col-md-10">
                                                <asp:DropDownList AutoPostBack="True" runat="server" OnSelectedIndexChanged="OnMapEmployee" ID="EmployeeType" CssClass="form-control" Width="280px" />
                                            </div>
                                            <asp:CompareValidator ID="CompareValidatorEmployeeType" runat="server" CssClass="text-danger"
                                                ControlToValidate="EmployeeType" ValidationGroup="EmployeeTypeDetailsGroup" ValueToCompare="--Select Employee Type--" Operator="NotEqual" Type="String" ErrorMessage="Please select employee Type"></asp:CompareValidator>
                                            <%--<asp:Button ValidationGroup="PersonalDetailsGroup" Text="Map Employee" CausesValidation="False" OnClick="OnMapEmployee" runat="server" CssClass="btn btn-default" />--%>
                                        </div>
                                    </div>
                                    <asp:Panel runat="server" ID="StateEmployeePanel" Visible="False" GroupingText="State head details">
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="SHDropdownList" CssClass="col-md-2 control-label">Select State</asp:Label>
                                            <div class="col-md-10">
                                                <asp:DropDownList runat="server" ID="SHDropdownList" CssClass="form-control" Width="280px" />
                                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                                    ControlToValidate="SHDropdownList" ValueToCompare="-- Select State --" Operator="NotEqual" Type="String" ErrorMessage="Please select state"></asp:CompareValidator>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="DistrictEmployeePanel" Visible="False" GroupingText="District head details">
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="DHDHDropdownList" CssClass="col-md-2 control-label">Select District</asp:Label>
                                            <div class="col-md-10">
                                                <asp:DropDownList AutoPostBack="True" OnSelectedIndexChanged="DHDHDropdownList_OnSelectedIndexChanged" runat="server" ID="DHDHDropdownList" CssClass="form-control" Width="280px" />
                                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                                    ControlToValidate="DHDHDropdownList" ValueToCompare="-- Select District --" Operator="NotEqual" Type="String" ErrorMessage="Please select district"></asp:CompareValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="DHRTODropdownList" CssClass="col-md-2 control-label">Select RTO</asp:Label>
                                            <div class="col-md-10">
                                                <asp:DropDownList runat="server" ID="DHRTODropdownList" CssClass="form-control" Width="280px" />
                                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                                    ControlToValidate="DHRTODropdownList" ValueToCompare="-- Select RTO --" Operator="NotEqual" Type="String" ErrorMessage="Please select respective RTO"></asp:CompareValidator>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="BusinessEmployeePanel" Visible="False" GroupingText="Business associate details">
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="BADHDropdownList" CssClass="col-md-2 control-label">Select District</asp:Label>
                                            <div class="col-md-10">
                                                <asp:DropDownList AutoPostBack="True" OnSelectedIndexChanged="BADHDropdownList_OnSelectedIndexChanged" runat="server" ID="BADHDropdownList" CssClass="form-control" Width="280px" />
                                                <asp:CompareValidator runat="server" CssClass="text-danger" ValidationGroup="DistrictSelectionGroup"
                                                    ControlToValidate="DHDHDropdownList" ValueToCompare="-- Select District --" Operator="NotEqual" Type="String" ErrorMessage="Please select district"></asp:CompareValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="BARTODropdownList" CssClass="col-md-2 control-label">Select RTO</asp:Label>
                                            <div class="col-md-10">
                                                <asp:DropDownList AutoPostBack="True" OnSelectedIndexChanged="BARTODropdownList_OnSelectedIndexChanged" runat="server" ID="BARTODropdownList" CssClass="form-control" Width="280px" />
                                                <asp:CompareValidator runat="server" CssClass="text-danger" ValidationGroup="DistrictSelectionGroup"
                                                    ControlToValidate="BARTODropdownList" ValueToCompare="-- Select RTO --" Operator="NotEqual" Type="String" ErrorMessage="Please select respective RTO"></asp:CompareValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="BAPinCodeDropdownlist" CssClass="col-md-2 control-label">Select Pincode</asp:Label>
                                            <div class="col-md-10">
                                                <asp:DropDownList runat="server" ID="BAPinCodeDropdownlist" CssClass="form-control" Width="280px" />
                                                <asp:CompareValidator runat="server" CssClass="text-danger"
                                                    ControlToValidate="BAPinCodeDropdownlist" ValidationGroup="RTOSelectionGroup" ValueToCompare="-- Select PinCode --" Operator="NotEqual" Type="String" ErrorMessage="Please select pincode"></asp:CompareValidator>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button ID="employeeTypeNext" runat="server" ValidationGroup="EmployeeTypeDetailsGroup" OnClick="OnEmployee" Text="Next" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="PersonalDetailsPanel" Visible="False" DefaultButton="PersonalDetailsNextButton">
                        <div class="form-horizontal">
                            <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                            <h4>Personal Details.</h4>
                            <hr />
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Name"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The name field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PersonalAddress" CssClass="col-md-2 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="PersonalAddress" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="PersonalAddress"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The address field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PersonalCity" CssClass="col-md-2 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="PersonalCity" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="PersonalCity"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The city field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PersonalDistrict" CssClass="col-md-2 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="PersonalDistrict" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="PersonalDistrict"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The district field is required." />
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-2 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="State" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator ValidationGroup="PersonalDetailsGroup" ID="StateComapareValidator" runat="server" CssClass="text-danger"
                                    ControlToValidate="State" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Please select State"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PersonalCountry" CssClass="col-md-2 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="PersonalCountry" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="PersonalCountry"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The country field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Age" CssClass="col-md-2 control-label">Age</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="Age" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Age"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The age field is required." />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorAge" ControlToValidate="Age" CssClass="text-danger" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="EmailId" CssClass="col-md-2 control-label">Email Id</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="EmailId" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="EmailId"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The email id field is required." />
                                <asp:RegularExpressionValidator ID="regEmail" CssClass="text-danger" runat="server" ControlToValidate="EmailId" ValidationExpression="^(.+)@([^\.].*)\.([a-z]{2,})$" Text="Enter a valid email" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="MobileNumber" CssClass="col-md-2 control-label">Mobile Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="MobileNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="MobileNumber"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Mobile Number field is required." />
                                <asp:RegularExpressionValidator runat="server" ControlToValidate="MobileNumber"
                                    CssClass="text-danger" ValidationGroup="PersonalDetailsGroup" Display="Dynamic" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$" ErrorMessage="Enter valid mobile number" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PanCard" CssClass="col-md-2 control-label">Pan card number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="PanCard" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="PanCard"
                                    CssClass="text-danger" ValidationGroup="PersonalDetailsGroup" isplay="Dynamic" ErrorMessage="The pan card field is required." />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="AadharCardNumber" CssClass="col-md-2 control-label">Aadhar Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="AadharCardNumber" CssClass="form-control" />
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="AadharCardNumber"
                                    CssClass="text-danger" ValidationGroup="PersonalDetailsGroup" Display="Dynamic" ErrorMessage="The Aadhar Card number field is required." />
                            </div>
                            <%--<asp:Panel runat="server" Visible="False" ID="pinCodePanel">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="BARTODropdownlist" CssClass="col-md-2 control-label">Select Pincode</asp:Label>
                                    <div class="col-md-5">
                                        <asp:DropDownList runat="server" ID="BARTODropdownlist" CssClass="form-control" Width="280px" />
                                        <asp:CompareValidator runat="server" CssClass="text-danger"
                                            ControlToValidate="BARTODropdownlist" ValidationGroup="RTOSelectionGroup" ValueToCompare="-- Select PinCode --" Operator="NotEqual" Type="String" ErrorMessage="Please select pincode"></asp:CompareValidator>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel runat="server" Visible="False" ID="districtCodePanel">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="DHDHDropdownList" CssClass="col-md-2 control-label">Select District</asp:Label>
                                    <div class="col-md-10">
                                        <asp:DropDownList AutoPostBack="True" OnSelectedIndexChanged="DHDHDropdownList_OnSelectedIndexChanged" runat="server" ID="DHDHDropdownList" CssClass="form-control" Width="280px" />
                                        <asp:CompareValidator runat="server" CssClass="text-danger" ValidationGroup="DistrictSelectionGroup"
                                            ControlToValidate="DHDHDropdownList" ValueToCompare="-- Select District --" Operator="NotEqual" Type="String" ErrorMessage="Please select district"></asp:CompareValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="rtoDropDownList" CssClass="col-md-2 control-label">Select RTO</asp:Label>
                                    <div class="col-md-10">
                                        <asp:DropDownList runat="server" ID="rtoDropDownList" CssClass="form-control" Width="280px" />
                                        <asp:CompareValidator runat="server" CssClass="text-danger" ValidationGroup="DistrictSelectionGroup"
                                            ControlToValidate="rtoDropDownList" ValueToCompare="-- Select RTO --" Operator="NotEqual" Type="String" ErrorMessage="Please select respective RTO"></asp:CompareValidator>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel runat="server" Visible="False" ID="stateCodePanel">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="SHDropdownList" CssClass="col-md-2 control-label">Select State</asp:Label>
                                    <div class="col-md-5">
                                        <asp:DropDownList runat="server" ID="SHDropdownList" CssClass="form-control" Width="280px" />
                                        <asp:CompareValidator runat="server" CssClass="text-danger"
                                            ControlToValidate="SHDropdownList" ValidationGroup="StateSelectionGroup" ValueToCompare="-- Select State --" Operator="NotEqual" Type="String" ErrorMessage="Please select state"></asp:CompareValidator>
                                    </div>
                                </div>
                            </asp:Panel>--%>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-5">
                                    <asp:Button runat="server" OnClick="OnPersonalDetailsBack" CausesValidation="False" Text="Back" CssClass="btn btn-default" />
                                    <asp:Button ID="PersonalDetailsNextButton" runat="server" OnClick="OnPersonal" ValidationGroup="PersonalDetailsGroup" Text="Next" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="CompanyDetailsPanel" Visible="False" DefaultButton="companyDetailsButton">
                        <div class="form-horizontal">
                            <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                            <h4>Company Details.</h4>
                            <br />
                            <h6>If firm type is partnership, Please add partner</h6>
                            <br />
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NameoftheFirm" CssClass="col-md-2 control-label">Name of the Firm</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="NameoftheFirm" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NameoftheFirm" ValidationGroup="CompanyGroup"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Name of the Firm field is required." />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FirmType" CssClass="col-md-2 control-label">Firm Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="FirmType" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="FirmType" ValidationGroup="CompanyGroup"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Firm Type field is required." />
                                </div>
                                <asp:Button Text="Add Partner" OnClick="OnAddClick" runat="server" CssClass="btn btn-default" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TelephoneNumber" CssClass="col-md-2 control-label">Telephone Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="TelephoneNumber" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="TelephoneNumber" ValidationGroup="CompanyGroup"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Telephone Number field is required." ValidationExpression="\d+" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="CompanyRegistrationNumber" CssClass="col-md-2 control-label">Registration Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="CompanyRegistrationNumber" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CompanyRegistrationNumber" ValidationGroup="CompanyGroup"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Registration Number is required." />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-3">
                                    <asp:Button runat="server" OnClick="OnCompanyDetailsBack" CausesValidation="False" Text="Back" CssClass="btn btn-default" />
                                    <asp:Button runat="server" ID="companyDetailsButton" OnClick="OnCompanyDetailsNext" ValidationGroup="CompanyGroup" Text="Next" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="BankDetailsPanel" Visible="False" DefaultButton="bankNextButton">
                        <div class="form-horizontal">
                            <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                            <h4>Bank Details.</h4>
                            <hr />
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="AccountNumber" CssClass="col-md-2 control-label">Account number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="AccountNumber" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="AccountNumber" ValidationGroup="BankGroup"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Name of the Bank field is required." />
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NameoftheBank" CssClass="col-md-2 control-label">Bank Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="NameoftheBank" CssClass="form-control" Width="280px" />
                                </div>
                                <asp:CompareValidator ValidationGroup="BankGroup" runat="server" CssClass="text-danger"
                                    ControlToValidate="NameoftheBank" ValueToCompare="-- Select Bank --" Operator="NotEqual" Type="String" ErrorMessage="Please select bank"></asp:CompareValidator>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="BankAddress" CssClass="col-md-2 control-label">Bank Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="BankAddress" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="BankAddress" ValidationGroup="BankGroup"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The Bank Address field is required." />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="MICRCode" CssClass="col-md-2 control-label">MICR Code</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="MICRCode" CssClass="form-control" />
                                    <asp:RegularExpressionValidator ValidationGroup="BankGroup" ControlToValidate="MICRCode" CssClass="text-danger" runat="server" ErrorMessage="Enter valid MICR code" ValidationExpression="\d+" />
                                    <asp:RequiredFieldValidator ValidationGroup="BankGroup" runat="server" ControlToValidate="MICRCode"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The MICR Code field is required." />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="IFSCCode" CssClass="col-md-2 control-label">IFSC Code</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="IFSCCode" CssClass="form-control" />
                                    <asp:RegularExpressionValidator ValidationGroup="BankGroup" runat="server" ControlToValidate="IFSCCode"
                                        CssClass="text-danger" Display="Dynamic" ValidationExpression="^[^\s]{4}\d{7}$" ErrorMessage="Enter valid IFSC code" />
                                    <asp:RequiredFieldValidator ValidationGroup="BankGroup" runat="server" ControlToValidate="IFSCCode"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The IFSC Code Number is required." />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-5">
                                    <asp:Button runat="server" OnClick="OnBankBack" CausesValidation="False" Text="Back" CssClass="btn btn-default" />
                                    <asp:Button runat="server" ID="bankNextButton" OnClick="OnBankNext" ValidationGroup="BankGroup" Text="Next" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="DocumentDetailsPanel" Visible="False" DefaultButton="documentDetailsButton">
                        <div class="form-horizontal">
                            <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                            <h4>Create a new account.</h4>
                            <hr />
                            <asp:ValidationSummary runat="server" CssClass="text-danger" />
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PanCardProof" CssClass="col-md-2 control-label">Pan card proof</asp:Label>
                                <asp:CheckBox runat="server" ID="PanCardProof" CssClass="form-control" Width="40px" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="AadhrarCardProof" CssClass="col-md-2 control-label">Aadhar proof</asp:Label>
                                <asp:CheckBox runat="server" ID="AadhrarCardProof" TextMode="Password" CssClass="form-control" Width="40px" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="CompanyRegistrationProof" CssClass="col-md-2 control-label">Company registration proof</asp:Label>
                                <asp:CheckBox runat="server" ID="CompanyRegistrationProof" TextMode="Password" CssClass="form-control" Width="40px" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="CompanyAddressProof" CssClass="col-md-2 control-label">Company address proof</asp:Label>
                                <asp:CheckBox runat="server" ID="CompanyAddressProof" TextMode="Password" CssClass="form-control" Width="40px" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="MobileBill" CssClass="col-md-2 control-label">Mobile bill</asp:Label>
                                <asp:CheckBox runat="server" ID="MobileBill" TextMode="Password" CssClass="form-control" Width="40px" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="OfficeRentalDocument" CssClass="col-md-2 control-label">Office rental document</asp:Label>
                                <asp:CheckBox runat="server" ID="OfficeRentalDocument" TextMode="Password" CssClass="form-control" Width="40px" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PartnerAddressProof" CssClass="col-md-2 control-label">Office rental document</asp:Label>
                                <asp:CheckBox runat="server" ID="PartnerAddressProof" TextMode="Password" CssClass="form-control" Width="40px" />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-5">
                                    <asp:Button runat="server" OnClick="OnDocumentDetailsBack" CausesValidation="False" Text="Back" CssClass="btn btn-default" />
                                    <asp:Button runat="server" ID="documentDetailsButton" OnClick="OnDocumentDetailsNext" ValidationGroup="DocumentGroup" Text="Submit" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>

                <div class="tab-pane" id="pendingTicketList">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View pending tickets</h4>
                        <br />
                        <asp:EntityDataSource ID="pendingEntityDataSource" OnQueryCreated="pendingEntityDataSource_QueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails"></asp:EntityDataSource>
                        <asp:GridView ID="pendingGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" ValidationGroup="ViewPendingTicketList" OnSelectedIndexChanged="OnPendingGridChanged" runat="server" AutoGenerateColumns="False" DataSourceID="pendingEntityDataSource" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ValidationGroup="ViewPendingTicketList"></asp:CommandField>
                                <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="True" SortExpression="TicketId"></asp:BoundField>
                                <asp:BoundField DataField="Registration" HeaderText="Vehicle Number" SortExpression="Registration"></asp:BoundField>
                                <asp:BoundField DataField="BAPermission" HeaderText="BA Approval" SortExpression="BAPermission"></asp:BoundField>
                                <asp:BoundField DataField="DHPermission" HeaderText="DH Approval" SortExpression="DHPermission"></asp:BoundField>
                                <asp:BoundField DataField="SHPermission" HeaderText="SH Approval" SortExpression="SHPermission"></asp:BoundField>
                                <asp:BoundField DataField="SAPermission" HeaderText="SA Approval" SortExpression="SAPermission"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:LinkButton ID="PrintLinkPendingButton" Visible="true" runat="server" ValidationGroup="ViewPendingTicketList" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='pendingGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="ExportLinkPendingButton" Visible="true" runat="server" ValidationGroup="ViewPendingTicketList" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='pendingGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>

                    </div>
                </div>

                <div class="tab-pane" id="ViewEmployees">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <h4>View Employees</h4>
                    <br />
                    <asp:EntityDataSource ID="ViewEmployeesEntityDataSource" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="UserDetails"></asp:EntityDataSource>
                    <asp:GridView ID="ViewEmployeesGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" ValidationGroup="ApproveVehicleGroup" runat="server" AutoGenerateColumns="False" DataSourceID="ViewEmployeesEntityDataSource" DataKeyNames="UserId" AllowPaging="True" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status"></asp:BoundField>
                            <asp:BoundField DataField="Role" HeaderText="Role" SortExpression="Role"></asp:BoundField>
                        </Columns>
                    </asp:GridView>

                </div>


                <div class="tab-pane" id="Changecommission">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Update commission Details</h4>
                        <div class="form-group">

                            <asp:Panel runat="server" ID="UpdateCommisiondetailsPanel">
                                <br />
                                <div class="form-group">
                                    <asp:Label ID="Label34" runat="server" Text="For Business Associate" CssClass="col-md-2 control-label" />
                                    <div class="col-md-5">
                                        <asp:TextBox ID="UpdateBA" runat="server" CssClass="form-control" />
                                    </div>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateBA" ValidationGroup="UpdateCommissionDetails"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The field is required." />
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label35" runat="server" Text="For District Head" CssClass="col-md-2 control-label" />
                                    <div class="col-md-5">
                                        <asp:TextBox ID="UpdateDH" runat="server" CssClass="form-control" />
                                    </div>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateDH" ValidationGroup="UpdateCommissionDetails"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The field is required." />
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label29" runat="server" Text="For State Head<br />" CssClass="col-md-2 control-label" />
                                    <div class="col-md-5">
                                        <asp:TextBox ID="UpdateSH" runat="server" CssClass="form-control" />
                                    </div>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdateSH" ValidationGroup="UpdateCommissionDetails"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The field is required." />
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-5">
                                        <asp:Button runat="server" Text="Update" ValidationGroup="UpdateCommissionDetails" OnClick="OnUpdateCommissionDetails" CssClass="btn btn-default" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="ChangeRenewalAmount">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Update Renewal Cost</h4>
                        <asp:UpdatePanel ID="changeUpdatePanel1" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel runat="server" DefaultButton="renewalButton">
                                    <div class="form-group">
                                        <br />
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="tonnageDropdown" CssClass="col-md-2 control-label">Tonnage Range</asp:Label>
                                            <div class="col-md-5">
                                                <asp:DropDownList AutoPostBack="True" OnSelectedIndexChanged="tonnageDropdown_OnSelectedIndexChanged" runat="server" ID="tonnageDropdown" CssClass="form-control" Width="280px" />
                                            </div>
                                            <asp:CompareValidator ValidationGroup="updaterenewal" ID="CompareValidator1" runat="server" CssClass="text-danger"
                                                ControlToValidate="tonnageDropdown" ValueToCompare="-- Select Range --" Operator="NotEqual" Type="String" ErrorMessage="Please select tonnage range"></asp:CompareValidator>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" AssociatedControlID="tonnageTextbox" CssClass="col-md-2 control-label">Amount</asp:Label>
                                            <div class="col-md-5">
                                                <asp:TextBox runat="server" ID="tonnageTextbox" CssClass="form-control" />
                                            </div>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="tonnageTextbox" ValidationGroup="updaterenewal"
                                                CssClass="text-danger" Display="Dynamic" ErrorMessage="The amount field is required." />
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-2 col-md-5">
                                                <asp:Button ID="renewalButton" runat="server" Text="Update" ValidationGroup="updaterenewal" OnClick="OnUpdaterenewalCost" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <%--Notification--%>

                <div class="tab-pane" id="SendBulkSMS">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Send Bulk Notification</h4>

                        <table border="0">
                            <tr>
                                <td>Recipient Number:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRecepientNumberc" ValidationGroup="BulkNotificationGroup" runat="server" Width="300" CssClass="form-control"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ValidationGroup="BulkNotificationGroup" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required"
                                        ControlToValidate="txtRecepientNumberc" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Message:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMessage" ValidationGroup="BulkNotificationGroup" runat="server" CssClass="form-control"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required"
                                        ControlToValidate="txtMessage" ValidationGroup="BulkNotificationGroup" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:Button ID="btnSend" ValidationGroup="BulkNotificationGroup" runat="server" Text="Send" OnClick="btnSend_Click" CssClass="btn btn-default" />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="CommissionUploadFile">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Generate Commission Upload File</h4>
                        <br />
                        <%-- <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="corporateID" CssClass="col-md-3 control-label">Enter Corporate ID</asp:Label>
                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="corporateID" CssClass="form-control" />
                            </div>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="corporateID"
                                CssClass="text-danger" ValidationGroup="CommissionUploadFile" Display="Dynamic" ErrorMessage="Corporate ID is required." />
                        </div>

                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="fileserialnumber" CssClass="col-md-3 control-label">Enter File Serial Number</asp:Label>
                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="fileserialnumber" CssClass="form-control" />
                            </div>--%>
                        <%--                            <asp:RequiredFieldValidator runat="server" ControlToValidate="fileserialnumber"
                                CssClass="text-danger" ValidationGroup="CommissionUploadFile" Display="Dynamic" ErrorMessage="Corporate ID is required." />--%>
                        <%--</div>

                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="encryptionrequired" CssClass="col-md-3 control-label">Require Encryption ?(Y/N)</asp:Label>
                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="encryptionrequired" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="remarks" CssClass="col-md-3 control-label">Remarks</asp:Label>
                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="remarks" CssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="transfertype" CssClass="col-md-3 control-label">Enter transfer type (NEFT/RTGS)</asp:Label>
                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="transfertype" CssClass="form-control" />
                            </div>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="transfertype"
                                CssClass="text-danger" ValidationGroup="CommissionUploadFile" Display="Dynamic" ErrorMessage="Transfer type is required." />
                        </div>

                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="PayeeIFSCCode" CssClass="col-md-3 control-label">Enter Payee IFSC Code</asp:Label>
                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="payeeifsccode" CssClass="form-control" />
                            </div>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="PayeeIFSCCode"
                                CssClass="text-danger" ValidationGroup="CommissionUploadFile" Display="Dynamic" ErrorMessage="IFSC code is required." />
                        </div>

                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="payeeaccountnumber" CssClass="col-md-3 control-label">Enter Payee Account Number</asp:Label>
                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="payeeaccountnumber" CssClass="form-control" />
                            </div>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="payeeaccountnumber"
                                CssClass="text-danger" ValidationGroup="CommissionUploadFile" Display="Dynamic" ErrorMessage="Payee Account number is required." />
                        </div>--%>

                        <div class="form-group">
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="BAcomission" Text="BA Commission" OnClick="GenerateBAcomission" CssClass="btn btn-default" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="DHcomission" Text="DH Commission" OnClick="GenerateDHcomission" CssClass="btn btn-default" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="SHcomission" Text="SH Commission" OnClick="GenerateSHcomission" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="TDSAmount">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Generate TDS Amount File</h4>
                        <br />
                        
                        <div class="form-group">
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="BAtdsCommission" Text="BA TDS Amount" OnClick="GenerateBATDScomission" CssClass="btn btn-default" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="DHtdsCommission" Text="DH TDS Amount" OnClick="GenerateDHTDScomission" CssClass="btn btn-default" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="SHtdsCommission" Text="SH TDS Amount" OnClick="GenerateSHTDScomission" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="selected_tab" runat="server" />
    <asp:HiddenField ID="PasswordField" runat="server" />
    <asp:HiddenField ID="TicketIdHiddenField" runat="server" />
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="lblModalTitle" runat="server" Text="Comment"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:TextBox TextMode="MultiLine" Width="100%" ID="Comments" runat="server"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                            ControlToValidate="Comments" ValidationGroup="CommentsValidationGroup" ErrorMessage="Please enter comments"></asp:RequiredFieldValidator>
                        <div class="modal-footer">
                            <asp:Button ID="IdButton" class="btn btn-info" ValidationGroup="CommentsValidationGroup" runat="server" UseSubmitBehavior="False" OnClick="ApprovalComments" Text="Ok" data-dismiss="modal" aria-hidden="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="modal fade" id="myModalComments" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModalComments" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="Label1" runat="server" Text="Comment"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:TextBox TextMode="MultiLine" Width="100%" ID="userComments" runat="server"></asp:TextBox>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" onserverclick="OnApprovalComments" data-dismiss="modal" aria-hidden="true">Ok</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="modal fade" id="myModalPartner" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModalPartner" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="Label2" runat="server" Text="Add Partner Details"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PartnerName" CssClass="col-md-2 control-label">Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="PartnerName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PartnerAge" CssClass="col-md-2 control-label">Age</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="PartnerAge" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="PartnerAddress" CssClass="col-md-2 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="PartnerAddress" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" onserverclick="OnPartnerDetails" data-dismiss="modal" aria-hidden="true">Add</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="ticketModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updateTicketModel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="TicketDetails" runat="server" Text="Ticket Details"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketId" CssClass="col-md-3 control-label">Ticket Id</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketId" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketGoodsType" CssClass="col-md-3 control-label">Goods Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketGoodsType" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketTonnage" CssClass="col-md-3 control-label">Tonnage Range</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketTonnage" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketMakeOftheVehicle" CssClass="col-md-3 control-label">Make of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketMakeOftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketModelOftheVehicle" CssClass="col-md-3 control-label">Model of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketModelOftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketCubicCapacity" CssClass="col-md-3 control-label">Cubic Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketCubicCapacity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketLengthoftheVehicle" CssClass="col-md-3 control-label">Length of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketLengthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketWidthoftheVehicle" CssClass="col-md-3 control-label">Width of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketWidthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketBreadthoftheVehicle" CssClass="col-md-3 control-label">Breadth of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketBreadthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketHeightoftheVehicle" CssClass="col-md-3 control-label">Height of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketHeightoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketRegistrationNumber" CssClass="col-md-3 control-label">Registration Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketRegistrationNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketEngineNumber" CssClass="col-md-3 control-label">Engine Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketEngineNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketChasisNumber" CssClass="col-md-3 control-label">Chassis Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketChasisNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketNOCRTO" CssClass="col-md-3 control-label">NOC from RTO</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="TicketNOCRTO" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketNOCPolice" CssClass="col-md-3 control-label">NOC from Police</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="TicketNOCPolice" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketVehiclePhotoFront" CssClass="col-md-3 control-label">Vehicle Photo Front</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="TicketVehiclePhotoFront" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketVehiclePhotoBack" CssClass="col-md-3 control-label">Vehicle Photo Back</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="TicketVehiclePhotoBack" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketVehiclePhotoLeft" CssClass="col-md-3 control-label">Vehicle Photo Left</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="TicketVehiclePhotoLeft" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketVehiclePhotoRight" CssClass="col-md-3 control-label">Vehicle Photo Right</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="TicketVehiclePhotoRight" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketFCValidityFrom" CssClass="col-md-3 control-label">FC Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketFCValidityFrom" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketFCValidity" CssClass="col-md-3 control-label">FC Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketFCValidity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketVehicleOwnerName" CssClass="col-md-3 control-label">Vehicle Owner Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketVehicleOwnerName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketAddress" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketCity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDistrict" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketState" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketState" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketCountry" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketInsuranceType" CssClass="col-md-3 control-label">Insurance Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketInsuranceType" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketInsuranceCompany" CssClass="col-md-3 control-label">Insurer Company</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketInsuranceCompany" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketValidityFrom" CssClass="col-md-3 control-label">Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketValidityFrom" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketValidityTo" CssClass="col-md-3 control-label">Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketValidityTo" CssClass="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverName" CssClass="col-md-3 control-label">Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverAge" CssClass="col-md-3 control-label">Age</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverAge" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverAddress" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverCity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverDistrict" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverState" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverState" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverCountry" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverDLNumber" CssClass="col-md-3 control-label">DL Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverDLNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverPhoto" CssClass="col-md-3 control-label">Vehicle Photo Right</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="TicketDriverPhoto" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverMobileNumber" CssClass="col-md-3 control-label">Mobile Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverMobileNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverEmailId" CssClass="col-md-3 control-label">Email</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverEmailId" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverPanNumber" CssClass="col-md-3 control-label">Pan card number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverPanNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketDriverAadharCardNumber" CssClass="col-md-3 control-label">Adhar Card Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketDriverAadharCardNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="TicketApprove" class="btn btn-info" runat="server" UseSubmitBehavior="False" OnClick="OnApproveClick" Text="Approve" data-dismiss="modal" aria-hidden="true" />
                            <asp:Button ID="TicketReject" class="btn btn-info" runat="server" UseSubmitBehavior="False" OnClick="OnRejectClick" Text="Reject" data-dismiss="modal" aria-hidden="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>


    <%--  <div class="modal fade" id="myModalPinCode" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="RTOUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label runat="server" Text="PinCode"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="BADHDropdownList" CssClass="col-md-2 control-label">Select District</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList AutoPostBack="True" OnSelectedIndexChanged="BADHDropdownList_OnSelectedIndexChanged" runat="server" ID="BADHDropdownList" CssClass="form-control" Width="280px" />
                                    <asp:CompareValidator runat="server" CssClass="text-danger" ValidationGroup="DistrictSelectionGroup"
                                        ControlToValidate="DHDHDropdownList" ValueToCompare="-- Select District --" Operator="NotEqual" Type="String" ErrorMessage="Please select district"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="BARTODropdownList" CssClass="col-md-2 control-label">Select RTO</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList AutoPostBack="True" OnSelectedIndexChanged="BARTODropdownList_OnSelectedIndexChanged" runat="server" ID="BARTODropdownList" CssClass="form-control" Width="280px" />
                                    <asp:CompareValidator runat="server" CssClass="text-danger" ValidationGroup="DistrictSelectionGroup"
                                        ControlToValidate="BARTODropdownList" ValueToCompare="-- Select RTO --" Operator="NotEqual" Type="String" ErrorMessage="Please select respective RTO"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="BAPinCodeDropdownlist" CssClass="col-md-2 control-label">Select Pincode</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="BAPinCodeDropdownlist" CssClass="form-control" Width="280px" />
                                    <asp:CompareValidator runat="server" CssClass="text-danger"
                                        ControlToValidate="BAPinCodeDropdownlist" ValidationGroup="RTOSelectionGroup" ValueToCompare="-- Select PinCode --" Operator="NotEqual" Type="String" ErrorMessage="Please select pincode"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" validationgroup="RTOSelectionGroup" runat="server" usesubmitbehavior="False" onserverclick="OnRTODetails" data-dismiss="modal" aria-hidden="true">Ok</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>--%>

    <%-- <div class="modal fade" id="myModalDistrict" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="DistrictUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label runat="server" Text="District"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DHDHDropdownList" CssClass="col-md-2 control-label">Select District</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList AutoPostBack="True" OnSelectedIndexChanged="DHDHDropdownList_OnSelectedIndexChanged" runat="server" ID="DHDHDropdownList" CssClass="form-control" Width="280px" />
                                    <asp:CompareValidator runat="server" CssClass="text-danger" ValidationGroup="DistrictSelectionGroup"
                                        ControlToValidate="DHDHDropdownList" ValueToCompare="-- Select District --" Operator="NotEqual" Type="String" ErrorMessage="Please select district"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DHRTODropdownList" CssClass="col-md-2 control-label">Select RTO</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList runat="server" ID="DHRTODropdownList" CssClass="form-control" Width="280px" />
                                    <asp:CompareValidator runat="server" CssClass="text-danger" ValidationGroup="DistrictSelectionGroup"
                                        ControlToValidate="DHRTODropdownList" ValueToCompare="-- Select RTO --" Operator="NotEqual" Type="String" ErrorMessage="Please select respective RTO"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" validationgroup="DistrictSelectionGroup" runat="server" usesubmitbehavior="False" onserverclick="OnDistrictDetails" data-dismiss="modal" aria-hidden="true">Ok</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>--%>

    <%--<div class="modal fade" id="myModalState" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="StateUpdatePanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label runat="server" Text="State"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="SHDropdownList" CssClass="col-md-2 control-label">Select State</asp:Label>
                                <div class="col-md-5">
                                    <asp:DropDownList runat="server" ID="SHDropdownList" CssClass="form-control" Width="280px" />
                                    <asp:CompareValidator runat="server" CssClass="text-danger"
                                        ControlToValidate="SHDropdownList" ValidationGroup="StateSelectionGroup" ValueToCompare="-- Select State --" Operator="NotEqual" Type="String" ErrorMessage="Please select state"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" validationgroup="StateSelectionGroup" runat="server" usesubmitbehavior="False" onserverclick="OnStateDetails" data-dismiss="modal" aria-hidden="true">Ok</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>--%>

    <div class="modal fade" id="myModalYesNo" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="YesNoUpdatePanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label runat="server" Text="Confirmation" Font-Bold="True" />
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" ForeColor="#3a87ad" Font-Size="20" Font-Bold="True" Width="100%">Do you want to add this ticket?</asp:Label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" usesubmitbehavior="False" onserverclick="OnConfirmationYesClick" data-dismiss="modal" aria-hidden="true">Yes</button>
                            <button class="btn btn-info" runat="server" usesubmitbehavior="False" onserverclick="OnConfirmationNoClick" data-dismiss="modal" aria-hidden="true">No</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="myModalPendingYesNo" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="pendingYesNoUpdatePanel1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label runat="server" Text="Confirmation" Font-Bold="True" />
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" ForeColor="#3a87ad" Font-Size="20" Font-Bold="True" Width="100%">Do you want to add this ticket?</asp:Label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" usesubmitbehavior="False" onserverclick="OnPendingTicketConfirmation" data-dismiss="modal" aria-hidden="true">Yes</button>
                            <button class="btn btn-info" runat="server" usesubmitbehavior="False" data-dismiss="modal" aria-hidden="true">No</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- Enable the tabs -->
    <script type="text/javascript">
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
        $(function () {
            var tabName = $("[id*=selected_tab]").val() != "" ? $("[id*=selected_tab]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=selected_tab]").val($(this).attr("href").replace("#", ""));
            });
        });

    </script>
</asp:Content>
