﻿<%@ Page Title="Document Details" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DocumentDetails.aspx.cs" Inherits="Dashboard.Account.DocumentDetails" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <asp:UpdatePanel runat="server" ID="MessagePanel">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-success"><%: SuccessMessage %></p>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-danger"><%: FailureMessage %></p>
            </asp:PlaceHolder>
            <asp:Timer runat="server" Interval="1000" OnTick="MessagePanelClick"></asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel runat="server" ID="DocumentDetailsPanel" Visible="False">
        <div class="form-horizontal">
            <h4>Create a new account.</h4>
            <hr />
            <asp:ValidationSummary runat="server" CssClass="text-danger" />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="PanCardProof" CssClass="col-md-2 control-label">Pan card proof</asp:Label>
                <asp:CheckBox runat="server" ID="PanCardProof" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="AadhrarCardProof" CssClass="col-md-2 control-label">Aadhar card proof</asp:Label>
                <asp:CheckBox runat="server" ID="AadhrarCardProof" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="CompanyRegistrationProof" CssClass="col-md-2 control-label">Company registration proof</asp:Label>
                <asp:CheckBox runat="server" ID="CompanyRegistrationProof" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="CompanyAddressProof" CssClass="col-md-2 control-label">Company address proof</asp:Label>
                <asp:CheckBox runat="server" ID="CompanyAddressProof" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="MobileBill" CssClass="col-md-2 control-label">Mobile bill</asp:Label>
                <asp:CheckBox runat="server" ID="MobileBill" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="OfficeRentalDocument" CssClass="col-md-2 control-label">Office rental document</asp:Label>
                <asp:CheckBox runat="server" ID="OfficeRentalDocument" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="PartnerAddressProof" CssClass="col-md-2 control-label">Office rental document</asp:Label>
                <asp:CheckBox runat="server" ID="PartnerAddressProof" TextMode="Password" CssClass="form-control" Width="40px" />
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="CreateUser_Click" ValidationGroup="DocumentGroup" Text="Submit" CssClass="btn btn-default" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="lblModalTitle" runat="server" Text="Comment"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:TextBox TextMode="MultiLine" Width="100%" ID="Comments" runat="server"></asp:TextBox>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" onserverclick="ApprovalComments" data-dismiss="modal" aria-hidden="true">Ok</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
