﻿<%@ Page Language="C#" Title="Register" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CompanyDetails.aspx.cs" Inherits="Dashboard.Account.CompanyDetails" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h3><%: Title %>.</h3>
    <asp:UpdatePanel runat="server" ID="MessagePanel">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-success"><%: SuccessMessage %></p>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-danger"><%: FailureMessage %></p>
            </asp:PlaceHolder>
            <asp:Timer runat="server" Interval="1000" OnTick="MessagePanelClick"></asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:Panel runat="server" ID="CompanyDetailsPanel" Visible="False">
        <div class="form-horizontal">
            <h4>Company Details.</h4>
            <hr />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="NameoftheFirm" CssClass="col-md-2 control-label">Name of the Firm</asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="NameoftheFirm" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="NameoftheFirm" ValidationGroup="CompanyGroup"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Name of the Firm field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="FirmType" CssClass="col-md-2 control-label">Firm Type</asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="FirmType" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirmType" ValidationGroup="CompanyGroup"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Firm Type field is required." />
                <asp:Button Text="Add Partner" OnClick="OnAddClick" runat="server" CssClass="btn btn-default" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="TelephoneNumber" CssClass="col-md-2 control-label">Telephone Number</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="TelephoneNumber" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="TelephoneNumber" ValidationGroup="CompanyGroup"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Telephone Number field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-2 control-label">Registration Number</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="RegistrationNumber" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="RegistrationNumber" ValidationGroup="CompanyGroup"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Registration Number is required." />
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-3">
                    <asp:Button runat="server" OnClick="OnCompanyDetailsBack" ValidationGroup="CompanyGroup" Text="Back" CssClass="btn btn-default" />
                    <asp:Button runat="server" OnClick="OnCompanyDetailsNext" ValidationGroup="CompanyGroup" Text="Next" CssClass="btn btn-default" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="lblModalTitle" runat="server" Text="Add Partner Details"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-2 control-label">Name</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="PartnerName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-2 control-label">Age</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="PartnerAge" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-2 control-label">Address</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="PartnerAddress" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" runat="server" onserverclick="ApprovalComments" data-dismiss="modal" aria-hidden="true">Add</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
