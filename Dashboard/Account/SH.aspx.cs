﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dashboard.Controllers;
using System.IO;
using System.Drawing;
using System.Text;
using Dashboard.Models;

namespace Dashboard.Account
{
    public partial class SH : Page
    {
        protected void OnApproveClick(object sender, EventArgs e)
        {
            OnApproveVehicleTicket();
            TicketReject.Visible = false;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            //
        }

        private void CalculateTotalListViewCommission()
        {
            var dataTable = new DataTable("commission");
            dataTable.Columns.Add(new DataColumn("DHName", typeof(string)));
            dataTable.Columns.Add(new DataColumn("DHCity", typeof(string)));
            dataTable.Columns.Add(new DataColumn("DHMobileNumber", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Amount", typeof(string)));
            var tonnageController = new TonnageController();
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;
            var districtController = new DistrictController();
            var districtDetails = districtController.Get().Where(x => x.StateId == loggedinUserDetails.StateId);
            if (null == districtDetails) return;
            var districtList = districtDetails.Select(districtDetail => districtDetail.DistrictId).ToList();
            var queryDistrictHeadList = (from userDetail in users where userDetail.DistrictId != null && districtList.Contains((int)userDetail.DistrictId) select userDetail.UserId).ToList();
            var personalController = new PersonalController();
            var totalcommissionValue = 0d;
            foreach (var i in queryDistrictHeadList)
            {
                var personalDetail = personalController.GetByUserId(i);
                var userDistrictDetail = userController.GetByUserId(i);
                var pinCodeController = new PinCodeController();
                var pinCodeDetails = pinCodeController.Get();
                if (null == pinCodeDetails) return;
                var pinCodeList =
                    (from pinCodeDetail in pinCodeDetails
                     where userDistrictDetail.RTOId == pinCodeDetail.RTOId
                     select pinCodeDetail.PinCodeId).ToList();

                var queryBaList = (from userDetail in users
                                   where userDetail.PinCodeId != null && pinCodeList.Contains((int)userDetail.PinCodeId)
                                   select userDetail.UserId).ToList();

                var ticketController = new TicketController();
                var ticketDetailsTemp = ticketController.Get();
                if (null == ticketDetailsTemp) return;
                var ticketDetails =
                    ticketDetailsTemp.Where(
                        ticketDetail =>
                            queryBaList.Contains(ticketDetail.UserId) &&
                            ticketDetail.TicketStatus == Global.UserStatus.Approved.ToString()).ToList();

                var commissiondetailscontroller = new CommissionController();
                var commissionDetail = commissiondetailscontroller.Get().FirstOrDefault();
                if (commissionDetail == null) return;
                var fincommssionrate = float.Parse(commissionDetail.CommissionSH);

                var tonnageDetails = tonnageController.Get();
                if (null == tonnageDetails) return;
                var enumerable = ticketDetails.ToArray();
                foreach (var ticket in enumerable)
                {
                    
                    if (ticket.CreatedDate == null || ticket.TicketEndDate == null) continue;
                    var startDate = (DateTime)ticket.CreatedDate;
                    if (DateTime.Compare(startDate, DatePickerstart.CalendarDate) < 0) continue;
                    if (DateTime.Compare(startDate, DatePickerEnd.CalendarDate) >= 0) continue;
                    var totalamount = 0d;
                    foreach (
                        var amount in
                            enumerable.Select(
                                ticketDetail =>
                                    float.Parse(
                                        ((fincommssionrate/100)*ticketDetail.RenewalCounter*
                                         tonnageController.Get(ticketDetail.TonnageId).TonnageCost).ToString())))
                    {
                        totalamount += amount;
                        totalcommissionValue += double.Parse(amount.ToString());
                    }
                    var row = dataTable.NewRow();
                    row[0] = personalDetail.Name;
                    row[1] = personalDetail.City;
                    row[2] = personalDetail.MobileNumber;
                    row[3] = totalamount;
                    dataTable.Rows.Add(row);
                }
            }
            totalCommission.Text = totalcommissionValue.ToString();
            dataTable.AcceptChanges();
            if (dataTable.Rows.Count > 0)
            {
                commissionPrintLinkButton.Visible = commissionExportLinkButton.Visible = false;
            }
            ComissionGrid.DataSource = dataTable;
            ComissionGrid.DataBind();
        }

        //private void CalculateTotalCommission()
        //{
        //    var userController = new UserController();
        //    var userDetails = userController.Get();
        //    if (null == userDetails) return;
        //    var users = userDetails as UserDetail[] ?? userDetails.ToArray();
        //    var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name);
        //    if (null == loggedinUserDetails) return;

        //    var districtController = new DistrictController();
        //    var districtDetails = districtController.Get().Where(x => x.StateId == loggedinUserDetails.StateId);
        //    if (null == districtDetails) return;
        //    var districtList = districtDetails.Select(districtDetail => districtDetail.DistrictId).ToList();
        //    var queryList = (from userDetail in users where userDetail.DistrictId != null && districtList.Contains((int)userDetail.DistrictId) select userDetail.DistrictId).ToList();

        //    var rtoController = new RTOController();
        //    var rtoDetails = rtoController.Get();
        //    if (null == rtoDetails) return;
        //    var rtoList = (from rtoDetail in rtoDetails where queryList.Contains(rtoDetail.DistrictId) select rtoDetail.RTOId).ToList();

        //    var pinCodeController = new PinCodeController();
        //    var pinCodeDetails = pinCodeController.Get();
        //    if (null == pinCodeDetails) return;
        //    var pinCodeList = (from pinCodeDetail in pinCodeDetails where rtoList.Contains(pinCodeDetail.RTOId) select pinCodeDetail.PinCodeId).ToList();

        //    var queryBaList = (from userDetail in users where userDetail.PinCodeId != null && pinCodeList.Contains((int)userDetail.PinCodeId) select userDetail.UserId).ToList();

        //    var ticketController = new TicketController();
        //    var ticketDetailsTemp = ticketController.Get();
        //    if (null == ticketDetailsTemp) return;
        //    var ticketDetails = ticketDetailsTemp.Where(ticketDetail => queryBaList.Contains(ticketDetail.UserId) && ticketDetail.TicketStatus == Global.UserStatus.Approved.ToString()).ToList();

        //    var commissiondetailscontroller = new CommissionController();
        //    var commissionDetail = commissiondetailscontroller.Get().FirstOrDefault();
        //    if (commissionDetail == null) return;
        //    var fincommssionrate = float.Parse(commissionDetail.CommissionSH);
        //    var renewalCounter = 0;
        //    var totalcommissionValue = 0d;
        //    if (null != ticketDetails)
        //    {
        //        var tonnageController = new TonnageController();
        //        var tonnageDetails = tonnageController.Get();
        //        if (null == tonnageDetails) return;
        //        var enumerable = ticketDetails.ToArray();

        //        foreach (var ticketDetail in enumerable)
        //        {
        //            TicketDetail detail = ticketDetail;
        //            ticketDetail.CommissionAmount = float.Parse(((fincommssionrate / 100) * ticketDetail.RenewalCounter *
        //                                             tonnageDetails.FirstOrDefault(x => x.TonnageFrom == detail.TonnageCapacity.Split('-')[0].Trim()).TonnageCost).ToString());
        //            if (null != ticketDetail.CommissionAmount)
        //                totalcommissionValue += double.Parse(ticketDetail.CommissionAmount.ToString());

        //            ticketController.Put(ticketDetail);
        //        }
        //        renewalCounter += enumerable.Where(ticketDetail => ticketDetail.RenewalCounter != null).Sum(ticketDetail => ticketDetail.RenewalCounter != null ? (int)ticketDetail.RenewalCounter : 0);
        //    }

        //    if (renewalCounter > 0)
        //    {
        //        totalCommission.Text = totalcommissionValue.ToString();
        //    }
        //}


        protected void OnTicketRejected(object sender, EventArgs e)
        {
            var tonnageController = new TonnageController();
            var ticketController = new TicketController();
            var ticketDetail = ticketController.Get(RejectedGridView.SelectedRow.Cells[1].Text);
            if (null == ticketDetail) return;
            TicketId.Text = ticketDetail.TicketId;
            GoodsType.Text = ticketDetail.GoodsType;
            var tonnageDetail = tonnageController.Get(ticketDetail.TonnageId);
            Tonnage.Text = tonnageDetail.TonnageTo;
            MakeOftheVehicle.Text = ticketDetail.Make;
            ModelOftheVehicle.Text = ticketDetail.Model;
            CubicCapacity.Text = ticketDetail.CubicCapacity;
            LengthoftheVehicle.Text = ticketDetail.Length;
            WidthoftheVehicle.Text = ticketDetail.Width;
            BreadthoftheVehicle.Text = ticketDetail.Breadth;
            HeightoftheVehicle.Text = ticketDetail.Height;
            RegistrationNumber.Text = ticketDetail.Registration;
            EngineNumber.Text = ticketDetail.EngineNumber;
            ChasisNumber.Text = ticketDetail.ChasisNumber;
            NOCRTO.Checked = bool.Parse(ticketDetail.NOCRTO);
            NOCPolice.Checked = bool.Parse(ticketDetail.NOCPolicy);
            VehiclePhotoFront.ImageUrl =
          string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
               ticketDetail.VehiclePhotoFront);
            VehiclePhotoBack.ImageUrl =
            string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                 ticketDetail.VehiclePhotoBack);
            VehiclePhotoLeft.ImageUrl =
            string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                 ticketDetail.VehiclePhotoLeft);
            VehiclePhotoRight.ImageUrl =
            string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                 ticketDetail.VehiclePhotoRight);
            FCValidityFrom.Text = ticketDetail.FCValidityFrom;
            FCValidity.Text = ticketDetail.FCValidity;
            VehicleOwnerName.Text = ticketDetail.Name;
            Address.Text = ticketDetail.Address;
            City.Text = ticketDetail.City;
            District.Text = ticketDetail.District;
            State.Text = ticketDetail.State;
            Country.Text = ticketDetail.Country;
            InsuranceType.Text = ticketDetail.InsuranceType;
            InsuranceCompany.Text = ticketDetail.InsuranceCompany;
            ValidityFrom.Text = ticketDetail.ValidityFrom;
            ValidityTo.Text = ticketDetail.Validity;
            DriverName.Text = ticketDetail.DriverName;
            DriverAge.Text = ticketDetail.DriverAge.ToString();
            DriverAddress.Text = ticketDetail.DriverAddress;
            DriverCity.Text = ticketDetail.DriverCity;
            DriverDistrict.Text = ticketDetail.DriverDistrict;
            DriverState.Text = ticketDetail.DriverState;
            DriverCountry.Text = ticketDetail.DriverCountry;
            DriverDLNumber.Text = ticketDetail.DriverDLNumber;
            DriverPhoto.ImageUrl =
               string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
               ticketDetail.DriverPhoto);
            DriverMobileNumber.Text = ticketDetail.DriverMobileNumber;
            DriverEmailId.Text = ticketDetail.DriverEmail;
            DriverPanNumber.Text = ticketDetail.DriverPanCard;
            DriverAadharCardNumber.Text = ticketDetail.DriverAadharCardNumber;
            TicketApprove.Visible = true;
            TicketReject.Visible = true;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ticketModel", "$('#ticketModel').modal();", true);
        }
        protected void OnRejectClick(object sender, EventArgs e)
        {
            OnApproveVehicleTicket();
            TicketApprove.Visible = false;
        }

        private void OnApproveVehicleTicket()
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected void OnTicketNotificationGridChanged(object sender, EventArgs e)
        {
            var tonnageController = new TonnageController();
            var ticketController = new TicketController();
            var ticketDetail = ticketController.Get(TicketGridView.SelectedRow.Cells[1].Text);
            if (null == ticketDetail) return;
            TicketId.Text = ticketDetail.TicketId;
            GoodsType.Text = ticketDetail.GoodsType;
            var tonnageDetail = tonnageController.Get(ticketDetail.TonnageId);
            Tonnage.Text = tonnageDetail.TonnageTo;
            MakeOftheVehicle.Text = ticketDetail.Make;
            ModelOftheVehicle.Text = ticketDetail.Model;
            CubicCapacity.Text = ticketDetail.CubicCapacity;
            LengthoftheVehicle.Text = ticketDetail.Length;
            WidthoftheVehicle.Text = ticketDetail.Width;
            BreadthoftheVehicle.Text = ticketDetail.Breadth;
            HeightoftheVehicle.Text = ticketDetail.Height;
            RegistrationNumber.Text = ticketDetail.Registration;
            EngineNumber.Text = ticketDetail.EngineNumber;
            ChasisNumber.Text = ticketDetail.ChasisNumber;
            NOCRTO.Checked = bool.Parse(ticketDetail.NOCRTO);
            NOCPolice.Checked = bool.Parse(ticketDetail.NOCPolicy);
            VehiclePhotoFront.ImageUrl =
          string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
               ticketDetail.VehiclePhotoFront);
            VehiclePhotoBack.ImageUrl =
            string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                 ticketDetail.VehiclePhotoBack);
            VehiclePhotoLeft.ImageUrl =
            string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                 ticketDetail.VehiclePhotoLeft);
            VehiclePhotoRight.ImageUrl =
            string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                 ticketDetail.VehiclePhotoRight);
            FCValidityFrom.Text = ticketDetail.FCValidityFrom;
            FCValidity.Text = ticketDetail.FCValidity;
            VehicleOwnerName.Text = ticketDetail.Name;
            Address.Text = ticketDetail.Address;
            City.Text = ticketDetail.City;
            District.Text = ticketDetail.District;
            State.Text = ticketDetail.State;
            Country.Text = ticketDetail.Country;
            InsuranceType.Text = ticketDetail.InsuranceType;
            InsuranceCompany.Text = ticketDetail.InsuranceCompany;
            ValidityFrom.Text = ticketDetail.ValidityFrom;
            ValidityTo.Text = ticketDetail.Validity;
            DriverName.Text = ticketDetail.DriverName;
            DriverAge.Text = ticketDetail.DriverAge.ToString();
            DriverAddress.Text = ticketDetail.DriverAddress;
            DriverCity.Text = ticketDetail.DriverCity;
            DriverDistrict.Text = ticketDetail.DriverDistrict;
            DriverState.Text = ticketDetail.DriverState;
            DriverCountry.Text = ticketDetail.DriverCountry;
            DriverDLNumber.Text = ticketDetail.DriverDLNumber;
            DriverPhoto.ImageUrl =
               string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
               ticketDetail.DriverPhoto);
            DriverMobileNumber.Text = ticketDetail.DriverMobileNumber;
            DriverEmailId.Text = ticketDetail.DriverEmail;
            DriverPanNumber.Text = ticketDetail.DriverPanCard;
            DriverAadharCardNumber.Text = ticketDetail.DriverAadharCardNumber;
            TicketApprove.Visible = true;
            TicketReject.Visible = true;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ticketModel", "$('#ticketModel').modal();", true);
        }


        private Dictionary<string, object> _commands = new Dictionary<string, object>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture)))
                {
                    var userController = new UserController();
                    var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
                    if (userDetail.Role != Global.EmployeeType.StatetHead)
                        Response.Redirect("~/");

                    //CalculateTotalCommission();
                    CalculateTotalListViewCommission();
                }
                else
                {
                    Response.Redirect("~/");
                }
                Title = "State head";
            }
            else
            {
                selected_tab.Value = Request.Form[selected_tab.UniqueID];
            }
            AddCommands();
        }

        private void AddCommands()
        {
            _commands.Add("ViewDHGridView", ViewDHGridView);
            _commands.Add("TicketGridView", TicketGridView);
            _commands.Add("ViewBAGridView", ViewBAGridView);
            _commands.Add("ComissionGrid", ComissionGrid);
        }
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected string FailureMessage
        {
            get;
            private set;
        }
        protected void MessagePanelClick(object sender, EventArgs e)
        {
            SuccessMessage = FailureMessage = string.Empty;
            successMessage.Visible = failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
            if (null != Session["Update"] && bool.Parse(Session["Update"].ToString()))
            {
                Session["Update"] = false;
                Response.Redirect("~/Account/SH");
            }
        }

        protected void ApprovalComments(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Comments.Text))
            {
                FailureMessage = "Please enter comments";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            if (TicketApprove.Visible)
            {
                UpdateTicketPermissions(false);
            }
            else if (TicketReject.Visible)
            {
                UpdateTicketPermissions(true);
            }
            Comments.Text = string.Empty;
            Session.Add("Update", true);
        }

        protected void ExportCurrentpage(object sender, CommandEventArgs e)
        {
            if (_commands.ContainsKey(e.CommandArgument.ToString()))
            {
                Exportpage(_commands[e.CommandArgument.ToString()] as GridView);
            }
        }


        private void Exportpage(GridView gridview)
        {
            try
            {
                if (gridview.Rows.Count <= 0) return;
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=GridViewExport.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                using (var sw = new StringWriter())
                {
                    var hw = new HtmlTextWriter(sw);

                    //To Export all pages
                    gridview.AllowPaging = false;
                    gridview.DataBind();

                    gridview.HeaderRow.BackColor = Color.White;
                    foreach (TableCell cell in gridview.HeaderRow.Cells)
                    {
                        cell.BackColor = gridview.HeaderStyle.BackColor;
                    }
                    foreach (GridViewRow row in gridview.Rows)
                    {
                        row.BackColor = Color.White;
                        foreach (TableCell cell in row.Cells)
                        {
                            if (row.RowIndex % 2 == 0)
                            {
                                cell.BackColor = gridview.AlternatingRowStyle.BackColor;
                            }
                            else
                            {
                                cell.BackColor = gridview.RowStyle.BackColor;
                            }
                            cell.CssClass = "textmode";
                        }
                    }

                    gridview.RenderControl(hw);

                    //style to format numbers to string
                    string style = @"<style> .textmode { } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                //throw new Exception(ex.Message);
            }
        }

        protected void PrintCurrentPage(object sender, CommandEventArgs e)
        {
            if (_commands.ContainsKey(e.CommandArgument.ToString()))
            {
                PrintPage(_commands[e.CommandArgument.ToString()] as GridView);
            }
        }

        private void PrintPage(GridView gridview)
        {
            if (gridview.Rows.Count <= 0) return;
            gridview.PagerSettings.Visible = false;
            gridview.DataBind();
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gridview.RenderControl(hw);
            string gridHTML = sw.ToString().Replace("\"", "'")
                .Replace(System.Environment.NewLine, "");
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            sb.Append(gridHTML);
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            gridview.PagerSettings.Visible = true;
            gridview.DataBind();
        }

        private void UpdateTicketPermissions(bool isRejected)
        {
            var ticketController = new TicketController();
            var ticketDetail = ticketController.Get(TicketId.Text);
            ticketDetail.SHComments = Comments.Text;
            if (isRejected)
            {
                ticketDetail.SHPermission = Global.UserStatus.Rejected.ToString();
            }
            else
            {
                ticketDetail.SHPermission = Global.UserStatus.Approved.ToString();
                ticketDetail.SAPermission = Global.UserStatus.Pending.ToString();
            }
            ticketController.Put(ticketDetail);
            SuccessMessage = "Updated ticket details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
        }

        protected void OnViewDistrictHeads(object sender, QueryCreatedEventArgs e)
        {
            ViewBAGridView.Visible = false;
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;
            var districtController = new DistrictController();
            var districtDetails = districtController.Get().Where(x => x.StateId == loggedinUserDetails.StateId);
            if (null == districtDetails) return;
            var districtList = districtDetails.Select(districtDetail => districtDetail.DistrictId).ToList();
            var queryList = (from userDetail in users where userDetail.DistrictId != null && districtList.Contains((int)userDetail.DistrictId) select userDetail.UserId).ToList();

            var orders = e.Query.Cast<PersonalDetail>();
            var query = from order in orders
                        where queryList.Contains(order.UserId) && order.EmployeeType == "District Head"
                        select order;
            e.Query = query;
            if (!query.Any())
            {
                viewDHPrintLinkButton.Visible = false;
                viewDHExportLinkButton.Visible = false;
            }
        }

        protected void OnViewBusinessAssociate(object sender, QueryCreatedEventArgs e)
        {
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;

            var districtController = new DistrictController();
            var districtDetails = districtController.Get().Where(x => x.StateId == loggedinUserDetails.StateId);
            if (null == districtDetails) return;
            var districtList = districtDetails.Select(districtDetail => districtDetail.DistrictId).ToList();
            var queryList = (from userDetail in users where userDetail.DistrictId != null && districtList.Contains((int)userDetail.DistrictId) select userDetail.DistrictId).ToList();

            var rtoController = new RTOController();
            var rtoDetails = rtoController.Get();
            if (null == rtoDetails) return;
            var rtoList = (from rtoDetail in rtoDetails where queryList.Contains(rtoDetail.DistrictId) select rtoDetail.RTOId).ToList();

            var pinCodeController = new PinCodeController();
            var pinCodeDetails = pinCodeController.Get();
            if (null == pinCodeDetails) return;
            var pinCodeList = (from pinCodeDetail in pinCodeDetails where rtoList.Contains(pinCodeDetail.RTOId) select pinCodeDetail.PinCodeId).ToList();

            var queryBaList = (from userDetail in users where userDetail.PinCodeId != null && pinCodeList.Contains((int)userDetail.PinCodeId) select userDetail.UserId).ToList();
            var orders = e.Query.Cast<PersonalDetail>();
            e.Query = from order in orders
                      where queryBaList.Contains(order.UserId)
                      select order;
        }

        protected void TicketEntityDataSource_OnQueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;

            var districtController = new DistrictController();
            var districtDetails = districtController.Get().Where(x => x.StateId == loggedinUserDetails.StateId);
            if (null == districtDetails) return;
            var districtList = districtDetails.Select(districtDetail => districtDetail.DistrictId).ToList();
            var queryList = (from userDetail in users where userDetail.DistrictId != null && districtList.Contains((int)userDetail.DistrictId) select userDetail.DistrictId).ToList();

            var rtoController = new RTOController();
            var rtoDetails = rtoController.Get();
            if (null == rtoDetails) return;
            var rtoList = (from rtoDetail in rtoDetails where queryList.Contains(rtoDetail.DistrictId) select rtoDetail.RTOId).ToList();

            var pinCodeController = new PinCodeController();
            var pinCodeDetails = pinCodeController.Get();
            if (null == pinCodeDetails) return;
            var pinCodeList = (from pinCodeDetail in pinCodeDetails where rtoList.Contains(pinCodeDetail.RTOId) select pinCodeDetail.PinCodeId).ToList();

            var queryBaList = (from userDetail in users where userDetail.PinCodeId != null && pinCodeList.Contains((int)userDetail.PinCodeId) select userDetail.UserId).ToList();
            var orders = e.Query.Cast<TicketDetail>();
            var query = from order in orders
                        where queryBaList.Contains(order.UserId) && order.SHPermission == "Pending"
                        select order;
            e.Query = query;
            if (!query.Any())
            {
                pendingNotificationPanel.Visible = true;
                PrintLinkButton.Visible = false;
                ExportLinkButton.Visible = false;
            }
            else
            {
                pendingNotificationPanel.Visible = false;
            }
        }

        protected void TicketEntityDataSource_OnRejectQueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;

            var districtController = new DistrictController();
            var districtDetails = districtController.Get().Where(x => x.StateId == loggedinUserDetails.StateId);
            if (null == districtDetails) return;
            var districtList = districtDetails.Select(districtDetail => districtDetail.DistrictId).ToList();
            var queryList = (from userDetail in users where userDetail.DistrictId != null && districtList.Contains((int)userDetail.DistrictId) select userDetail.DistrictId).ToList();

            var rtoController = new RTOController();
            var rtoDetails = rtoController.Get();
            if (null == rtoDetails) return;
            var rtoList = (from rtoDetail in rtoDetails where queryList.Contains(rtoDetail.DistrictId) select rtoDetail.RTOId).ToList();

            var pinCodeController = new PinCodeController();
            var pinCodeDetails = pinCodeController.Get();
            if (null == pinCodeDetails) return;
            var pinCodeList = (from pinCodeDetail in pinCodeDetails where rtoList.Contains(pinCodeDetail.RTOId) select pinCodeDetail.PinCodeId).ToList();

            var queryBaList = (from userDetail in users where userDetail.PinCodeId != null && pinCodeList.Contains((int)userDetail.PinCodeId) select userDetail.UserId).ToList();
            var orders = e.Query.Cast<TicketDetail>();
            var query = from order in orders
                        where queryBaList.Contains(order.UserId) && order.SAPermission == "Rejected" && order.SHPermission != "Rejected"
                        select order;
            e.Query = query;
            if (!query.Any())
            {
                rejectedNotificationPanel.Visible = true;
                PrintRejectLinkButton.Visible = false;
                ExportRejectLinkButton.Visible = false;
            }
            else
            {
                rejectedNotificationPanel.Visible = false;
            }
        }

        protected void ViewDHGridView_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            selected_tab.Value = "ViewDH";
            ViewBAGridView.DataBind();
            ViewBAGridView.Visible = true;
        }

        protected void ViewBAEntityDataSource_OnQueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var userId = int.Parse(ViewDHGridView.SelectedRow.Cells[5].Text);
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.UserId == userId);
            if (null == loggedinUserDetails) return;

            var rtoController = new RTOController();
            var rtoDetails = rtoController.Get();
            if (null == rtoDetails) return;
            var rtoList = (from rtoDetail in rtoDetails where loggedinUserDetails.RTOId == rtoDetail.RTOId select rtoDetail.RTOId).ToList();

            var pinCodeController = new PinCodeController();
            var pinCodeDetails = pinCodeController.Get();
            if (null == pinCodeDetails) return;
            var pinCodeList = (from pinCodeDetail in pinCodeDetails where rtoList.Contains(pinCodeDetail.RTOId) select pinCodeDetail.PinCodeId).ToList();

            var queryBaList = (from userDetail in users where userDetail.PinCodeId != null && pinCodeList.Contains((int)userDetail.PinCodeId) select userDetail.UserId).ToList();
            var orders = e.Query.Cast<PersonalDetail>();
            var query = from order in orders
                        where queryBaList.Contains(order.UserId) && (order.EmployeeType == "Business Associate")
                        select order;
            e.Query = query;
            if (query.Any())
            {
                viewBAPrintLinkButton.Visible = true;
                viewBAExportLinkButton.Visible = true;
            }
            else
            {
                viewBAPrintLinkButton.Visible = false;
                viewBAExportLinkButton.Visible = false;
            }
        }

        protected void ViewBAGridView_OnSelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            selected_tab.Value = "ViewDH";
        }


        protected void commissionButton_OnClick(object sender, EventArgs e)
        {
            if (DatePickerstart.IsValidDate && DatePickerEnd.IsValidDate)
            {
                if (DatePickerstart.CalendarDate > DatePickerEnd.CalendarDate)
                {
                    FailureMessage = "Start date cannot be greater than end date";
                    failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                    return;
                }
                CalculateTotalListViewCommission();
            }
            else
            {
                FailureMessage = "Please enter proper date";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
            }
        }

        //protected void ComissionGrid_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    selected_tab.Value = "Commission1";
        //}

        //protected void CommissionDataSource_OnQueryCreated(object sender, QueryCreatedEventArgs e)
        //{
        //    var userController = new UserController();
        //    var userDetails = userController.Get();
        //    if (null == userDetails) return;
        //    var users = userDetails as UserDetail[] ?? userDetails.ToArray();
        //    var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name);
        //    if (null == loggedinUserDetails) return;

        //    var districtController = new DistrictController();
        //    var districtDetails = districtController.Get().Where(x => x.StateId == loggedinUserDetails.StateId);
        //    if (null == districtDetails) return;
        //    var districtList = districtDetails.Select(districtDetail => districtDetail.DistrictId).ToList();
        //    var queryList = (from userDetail in users where userDetail.DistrictId != null && districtList.Contains((int)userDetail.DistrictId) select userDetail.DistrictId).ToList();

        //    var rtoController = new RTOController();
        //    var rtoDetails = rtoController.Get();
        //    if (null == rtoDetails) return;
        //    var rtoList = (from rtoDetail in rtoDetails where queryList.Contains(rtoDetail.DistrictId) select rtoDetail.RTOId).ToList();

        //    var pinCodeController = new PinCodeController();
        //    var pinCodeDetails = pinCodeController.Get();
        //    if (null == pinCodeDetails) return;
        //    var pinCodeList = (from pinCodeDetail in pinCodeDetails where rtoList.Contains(pinCodeDetail.RTOId) select pinCodeDetail.PinCodeId).ToList();

        //    var queryBaList = (from userDetail in users where userDetail.PinCodeId != null && pinCodeList.Contains((int)userDetail.PinCodeId) select userDetail.UserId).ToList();

        //    var status = Global.UserStatus.Approved.ToString();
        //    var orders = e.Query.Cast<TicketDetail>();
        //    e.Query = from order in orders
        //              where queryBaList.Contains(order.UserId) && order.TicketStatus == status
        //              select order;
        //}
    }
}