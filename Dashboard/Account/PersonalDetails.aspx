﻿<%@ Page Language="C#" Title="Register" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="PersonalDetails.aspx.cs" Inherits="Dashboard.Account.PersonalDetails" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:Panel runat="server" ID="PersonalDetailsPanel" Visible="False">
        <div class="form-horizontal">
            <h4>Personal Details.</h4>
            <hr />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Name</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Name"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The name field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-2 control-label">Address</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Address"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The address field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-2 control-label">City</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="City" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="City"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The city field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="District" CssClass="col-md-2 control-label">District</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="District" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="District"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The district field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-2 control-label">State</asp:Label>
                <div class="col-md-5">
                    <asp:DropDownList runat="server" ID="State" CssClass="form-control" Width="280px" />
                </div>
                <asp:CompareValidator ValidationGroup="PersonalDetailsGroup" ID="StateComapareValidator" runat="server" CssClass="text-danger"
                    ControlToValidate="State" ValueToCompare="--Select State--" Operator="NotEqual" Type="String" ErrorMessage="Please select State"></asp:CompareValidator>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Country" CssClass="col-md-2 control-label">Country</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="Country" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Country"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The country field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Age" CssClass="col-md-2 control-label">Age</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="Age" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="Age"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The age field is required." />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorAge" ControlToValidate="Age" CssClass="text-danger" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmailId" CssClass="col-md-2 control-label">Email Id</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="EmailId" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="EmailId"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The email id field is required." />
                <asp:RegularExpressionValidator ID="regEmail" CssClass="text-danger" runat="server" ControlToValidate="EmailId" ValidationExpression="^(.+)@([^\.].*)\.([a-z]{2,})$" Text="Enter a valid email" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="MobileNumber" CssClass="col-md-2 control-label">Mobile Number</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="MobileNumber" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator ValidationGroup="PersonalDetailsGroup" runat="server" ControlToValidate="MobileNumber"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Mobile Number field is required." />
                <asp:RegularExpressionValidator runat="server" ControlToValidate="MobileNumber"
                    CssClass="text-danger" ValidationGroup="PersonalDetailsGroup" Display="Dynamic" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$" ErrorMessage="Enter valid mobile number" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="PanCard" CssClass="col-md-2 control-label">Pan card number</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="PanCard" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="PanCard"
                    CssClass="text-danger" DValidationGroup="PersonalDetailsGroup" isplay="Dynamic" ErrorMessage="The pan card field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="AadharCardNumber" CssClass="col-md-2 control-label">Aadhar Card Number</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="AadharCardNumber" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="AadharCardNumber"
                    CssClass="text-danger" ValidationGroup="PersonalDetailsGroup" Display="Dynamic" ErrorMessage="The Aadhar Card number field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmployeeType" CssClass="col-md-2 control-label">Employee Type</asp:Label>
                <div class="col-md-5">
                    <asp:DropDownList runat="server" ID="EmployeeType" CssClass="form-control" Width="280px" />
                </div>
                <asp:CompareValidator ID="CompareValidatorEmployeeType" runat="server" CssClass="text-danger"
                    ControlToValidate="EmployeeType" ValidationGroup="PersonalDetailsGroup" ValueToCompare="--Select Employee Type--" Operator="NotEqual" Type="String" ErrorMessage="Please select employee Type"></asp:CompareValidator>
            </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-5">
                <asp:Button runat="server" ValidationGroup="PersonalDetailsGroup" OnClick="CreateUser_Click" Text="Next" CssClass="btn btn-default" />
            </div>
        </div>
        </div>
    </asp:Panel>
</asp:Content>
