﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dashboard.Controllers;
using Dashboard.Models;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using EditableControls;

namespace Dashboard.Account
{
    public partial class BA : System.Web.UI.Page
    {
        private void LoadTonnageDetails(DropDownList dropDownList)
        {
            dropDownList.Items.Clear();
            dropDownList.Items.Add("-- Select Range --");
            var tonnageController = new TonnageController();
            var tonnageDetails = tonnageController.Get();
            if (null == tonnageDetails) return;
            foreach (var tonnageDetail in tonnageDetails)
            {
                dropDownList.Items.Add(tonnageDetail.TonnageTo);
            }
        }
        private const string VehicleFileNameFront = "VehicleFileNameFront";
        private const string VehicleFileNameBack = "VehicleFileNameBack";
        private const string VehicleFileNameLeft = "VehicleFileNameLeft";
        private const string VehicleFileNameRight = "VehicleFileNameRigtht";
        private const string VehicleFileBytes = "VehicleFileBytes";
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected string FailureMessage
        {
            get;
            private set;
        }
        private Dictionary<string, object> _commands = new Dictionary<string, object>();

        protected void HttpContextButton_Click(object sender, EventArgs e)
        {
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;
            var bankController = new BankController();
            var bankDetail = bankController.GetByUserId(userDetail.UserId);
            if (null == bankDetail) return;
            var accountController = new AccountController();
            var accountDetail = accountController.GetByUserId(userDetail.UserId);
            if (null == accountDetail)
            {
                var newAccountDetail = new AccountDetail
                {
                    Funds = depositTextbox.Text,
                    Bank = bankDetail.Name,
                    UserId = userDetail.UserId
                };
                accountController.Post(newAccountDetail);
            }
            else
            {
                var newAccountDetail = new AccountDetail();
                newAccountDetail.Funds += depositTextbox.Text;
                newAccountDetail.Bank = bankDetail.Name;
                newAccountDetail.UserId = userDetail.UserId;
                accountController.Put(newAccountDetail);
            }

            Response.Redirect("enterpayinfo.aspx?Amount=" + depositTextbox.Text);
        }


        private void CalculateTotalListViewCommission()
        {
            totalCommissionPanel.Visible = false;
            var dataTable = new DataTable("commission");

            dataTable.Columns.Add(new DataColumn("Registration", typeof(string)));
            dataTable.Columns.Add(new DataColumn("StarteDate", typeof(string)));
            dataTable.Columns.Add(new DataColumn("EndDate", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Amount", typeof(string)));
            var approved = Global.UserStatus.Approved.ToString();
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            var commissiondetailscontroller = new CommissionController();
            var commissionDetail = commissiondetailscontroller.Get().FirstOrDefault();
            if (commissionDetail == null) return;
            var fincommssionrate = float.Parse(commissionDetail.CommissionBA);
            var ticketController = new TicketController();
            var ticketDetails = ticketController.Get().Where(x => x.UserId == userDetail.UserId && x.TicketStatus == approved);
            var totalcommissionValue = 0d;
            if (null != ticketDetails)
            {
                var tonnageController = new TonnageController();
                var tonnageDetails = tonnageController.Get();
                if (null == tonnageDetails) return;
                var enumerable = ticketDetails as TicketDetail[] ?? ticketDetails.ToArray();

                foreach (var ticketDetail in enumerable)
                {
                    if (ticketDetail.CreatedDate == null || ticketDetail.TicketEndDate == null) continue;
                    var startDate = (DateTime)ticketDetail.CreatedDate;
                    if (null == startDate) continue;
                    if (DateTime.Compare(startDate, DatePickerstart.CalendarDate) < 0) continue;
                    if (DateTime.Compare(startDate, DatePickerEnd.CalendarDate) >= 0) continue;

                    var row = dataTable.NewRow();
                    double totalamount =
                        float.Parse(
                            ((fincommssionrate / 100) * ticketDetail.RenewalCounter *
                             tonnageController.Get(ticketDetail.TonnageId).TonnageCost).ToString());
                    totalcommissionValue += totalamount;
                    row[0] = ticketDetail.Registration;
                    row[1] = ticketDetail.CreatedDate;
                    row[2] = ticketDetail.TicketEndDate;
                    row[3] = totalamount;
                    dataTable.Rows.Add(row);
                }
            }
            totalCommission.Text = totalcommissionValue.ToString();
            dataTable.AcceptChanges();
            if (dataTable.Rows.Count > 0)
            {
                totalCommissionPanel.Visible = true;
                commissionPrintLinkButton.Visible = commissionExportLinkButton.Visible = false;
            }
            ComissionGrid.DataSource = dataTable;
            ComissionGrid.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture)))
                {
                    var userController = new UserController();
                    var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
                    if (userDetail.Role != Global.EmployeeType.BusinessAssociate)
                        Response.Redirect("~/");
                    var accountcontroller = new AccountController();
                    var balancedetail = accountcontroller.GetByUserId(userDetail.UserId);
                    if (null == balancedetail)
                    {
                        balancelabel.Text = "Account Balance in rupees : " + 0.ToString();
                    }
                    else
                    {
                        balancelabel.Text = "Account Balance in rupees : " + balancedetail.Funds;
                    }
                }
                else
                {
                    Response.Redirect("~/");
                }

                GetIndianStates(DriverState);
                GetIndianStates(State);
                GetIndianStates(DriverDetailDropDown);
                GetIndianStates(UpdateVehicleDetailsDropDown);
                GetIndianStates(notificationState);
                GetIndianStates(notificationDriverState);
                GetGoodsType(GoodsType);
                GetGoodsType(notificationGoodsType);
                GetGoodsType(UpdateVehicleGoodsType);
                //CalculateTotalCommission();
                LoadTonnageDetails(notificationTonnageCapacity);
                LoadTonnageDetails(UpdateVehicleTonnageCapacity);
                LoadTonnageDetails(tonnageCapacity);
                //CalculateTotalListViewCommission();
                LoadVehicles(DisableVehicleId);
                LoadVehicles(UpdateDriverVehicleId);
                LoadVehicles(UpdateLocationVehicleId);
                LoadVehicles(UpdateVehicleId);
            }
            else
            {
                selected_tab.Value = Request.Form[selected_tab.UniqueID];
                
            }
            AddCommands();
            Title = Global.EmployeeType.BusinessAssociate;
        }

        private void AddCommands()
        {
            _commands.Add("renewVehicleGridview", renewVehicleGridview);
            _commands.Add("ViewVehicleGridView", ViewVehicleGridView);
            _commands.Add("ViewAccountDetailsGridView", ViewAccountDetailsGridView);
            _commands.Add("gridview4", gridview4);
            _commands.Add("TicketGridView", TicketGridView);
            _commands.Add("ComissionGrid", ComissionGrid);
        }

        private void GetGoodsType(DropDownList dropDownList)
        {
            dropDownList.Items.Add("--Select Goods Type--");
            dropDownList.Items.Add("General Goods");
            dropDownList.Items.Add("Perishable Goods");
            dropDownList.Items.Add("Gas");
            dropDownList.Items.Add("Liquid Goods");
            dropDownList.Items.Add("Harmful Goods");
        }

        private void GetIndianStates(DropDownList dropDownList)
        {
            dropDownList.Items.Add("--Select State--");
            dropDownList.Items.Add("Andhra Pradesh");
            dropDownList.Items.Add("Arunachal Pradesh");
            dropDownList.Items.Add("Assam");
            dropDownList.Items.Add("Bihar");
            dropDownList.Items.Add("Chhattisgarh");
            dropDownList.Items.Add("Goa");
            dropDownList.Items.Add("Gujarat");
            dropDownList.Items.Add("Haryana");
            dropDownList.Items.Add("Himachal Pradesh");
            dropDownList.Items.Add("Jammu and Kashmir");
            dropDownList.Items.Add("Jharkhand");
            dropDownList.Items.Add("Karnataka");
            dropDownList.Items.Add("Kerala");
            dropDownList.Items.Add("Madhya Pradesh");
            dropDownList.Items.Add("Maharashtra");
            dropDownList.Items.Add("Manipur");
            dropDownList.Items.Add("Meghalaya");
            dropDownList.Items.Add("Mizoram");
            dropDownList.Items.Add("Nagaland");
            dropDownList.Items.Add("Odisha");
            dropDownList.Items.Add("Punjab");
            dropDownList.Items.Add("Rajasthan");
            dropDownList.Items.Add("Sikkim");
            dropDownList.Items.Add("Tamil Nadu");
            dropDownList.Items.Add("Telangana");
            dropDownList.Items.Add("Tripura");
            dropDownList.Items.Add("Uttar Pradesh");
            dropDownList.Items.Add("Uttarakhand");
            dropDownList.Items.Add("West Bengal");
        }

        protected void MessagePanelClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FailureMessage) || string.IsNullOrEmpty(SuccessMessage))
            {
                timer.Interval = int.MaxValue;
            }
            ErrorMessage.Text = SuccessMessage = FailureMessage = string.Empty;
            successMessage.Visible = failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
            if (null == Session["Update"] || !bool.Parse(Session["Update"].ToString())) return;
            Session["Update"] = false;
            Response.Redirect("~/Account/BA");
        }

        protected void OnUpdateLocationSubmit(object sender, EventArgs e)
        {
            UpdateLocationPanel.Visible = false;
            var ticketController = new TicketController();
            var ticketDetailslist = ticketController.Get();
            TicketDetail ticketDetails = null;
            if (null != ticketDetailslist)
            {
                ticketDetails = ticketDetailslist.Where(x => x.Registration == UpdateLocationVehicleId.Text).ToList().FirstOrDefault();
            }
            if (null == ticketDetails)
            {
                FailureMessage = "Please enter proper vehicle number";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }


            if (string.IsNullOrEmpty(ticketDetails.SAPermission) ||
               ticketDetails.SAPermission != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetails.TicketStatus) ||
               ticketDetails.TicketStatus != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            var vehicleLocationController = new VehicleLocationController();
            var vehicleDetails = vehicleLocationController.Get();
            if (null != vehicleDetails)
            {
                var vehcicleDetail = vehicleDetails.Where(x => x.VehicleId == ticketDetails.Registration).ToList().FirstOrDefault();
                if (null != vehcicleDetail)
                {
                    UpdateLocationAddress.Text = vehcicleDetail.Address;
                    UpdateLocationCity.Text = vehcicleDetail.City;
                    UpdateLocationDistrict.Text = vehcicleDetail.District;
                    UpdateVehicleCurrentLocation.Text = vehcicleDetail.State;
                    UpdateLocationCountry.Text = vehcicleDetail.Country;
                    UpdateVehicleLocation.Text = vehcicleDetail.Location;
                }

                else
                {
                    UpdateLocationAddress.Text = ticketDetails.Address;
                    UpdateLocationCity.Text = ticketDetails.City;
                    UpdateLocationDistrict.Text = ticketDetails.District;
                    UpdateVehicleCurrentLocation.Text = ticketDetails.State;
                    UpdateLocationCountry.Text = ticketDetails.Country;
                    UpdateVehicleLocation.Text = ticketDetails.City;
                }

            }
            else
            {
                UpdateLocationAddress.Text = ticketDetails.Address;
                UpdateLocationCity.Text = ticketDetails.City;
                UpdateLocationDistrict.Text = ticketDetails.District;
                UpdateVehicleCurrentLocation.Text = ticketDetails.State;
                UpdateLocationCountry.Text = ticketDetails.Country;
                UpdateVehicleLocation.Text = ticketDetails.City;
            }
            UpdateLocationPanel.Visible = true;
            //UpdateLocationVehicleId.ReadOnly = true;
            //UpdateLocationSubmitButton.Visible = false;
        }

        protected void OnUpdateLocationCancel(object sender, EventArgs e)
        {
            UpdateLocationVehicleId.SelectedIndex = 0;
            UpdateLocationPanel.Visible = false;
        }

        protected void OnUpdateLocation(object sender, EventArgs e)
        {


            var ticketController = new TicketController();
            var ticketDetailslist = ticketController.Get();
            TicketDetail ticketDetails = null;
            if (null != ticketDetailslist)
            {
                ticketDetails = ticketDetailslist.Where(x => x.Registration == UpdateLocationVehicleId.Text).ToList().FirstOrDefault();
            }
            if (null == ticketDetails)
            {
                FailureMessage = "Please enter proper vehicle number";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetails.SAPermission) ||
                ticketDetails.SAPermission != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetails.TicketStatus) ||
              ticketDetails.TicketStatus != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (ticketDetails.VehicleStatus != Global.VehicleStatus.Active.ToString())
            {
                FailureMessage = "Already Vehicle is disabled";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }

            var vehicleLocationController = new VehicleLocationController();
            var vehicleLocationDetails = vehicleLocationController.Get();
            if (null != vehicleLocationDetails)
            {
                var vehcicleLocationDetail = vehicleLocationDetails.Where(x => x.VehicleId == ticketDetails.Registration).ToList().FirstOrDefault();
                if (null != vehcicleLocationDetail)
                {
                    vehcicleLocationDetail.Location = UpdateVehicleLocation.Text;
                    vehicleLocationController.Put(vehcicleLocationDetail);
                }
                else
                {
                    var vehicleLocationDetail = new VehicleLocationDetail
                    {
                        VehicleId = ticketDetails.Registration,
                        UserId = ticketDetails.UserId,
                        Address = UpdateLocationAddress.Text,
                        City = UpdateLocationCity.Text,
                        District = UpdateLocationDistrict.Text,
                        State = UpdateVehicleCurrentLocation.Text,
                        Country = UpdateLocationCountry.Text,
                        Location = UpdateVehicleLocation.Text
                    };
                    vehicleLocationController.Post(vehicleLocationDetail);
                }
            }
            else
            {
                var vehicleLocationDetail = new VehicleLocationDetail
                {
                    VehicleId = ticketDetails.Registration,
                    UserId = ticketDetails.UserId,
                    Address = UpdateLocationAddress.Text,
                    City = UpdateLocationCity.Text,
                    District = UpdateLocationDistrict.Text,
                    State = UpdateVehicleCurrentLocation.Text,
                    Country = UpdateLocationCountry.Text,
                    Location = UpdateVehicleLocation.Text
                };
                vehicleLocationController.Post(vehicleLocationDetail);
            }
            SuccessMessage = "Location Updated";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            timer.Interval = 1000;
            UpdateLocationPanel.Visible = false;
            UpdateLocationVehicleId.SelectedIndex = 0;
            //UpdateLocationSubmitButton.Visible = true;
            //UpdateLocationVehicleId.ReadOnly = false;
        }

        protected void OnViewVehicleSelectionChanged(object sender, EventArgs e)
        {
            if (ViewVehicleGridView.Rows.Count > 0) return;
            ExportViewVehicleLinkButton.Visible = false;
            PrintViewVehicleLinkButton.Visible = false;
        }

        protected void OnViewVehicleListGridChanged(object sender, EventArgs e)
        {
            selected_tab.Value = "viewvehicle";
            if (!PopulateTicketDetails(ViewVehicleGridView)) return;
            TicketDetails.Text = "View vehicle";
            TicketApprove.Text = "Ok";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "viewVehicleListModel", "$('#viewVehicleListModel').modal();", true);
        }

        protected void OnRenewVehicleListGridChanged(object sender, EventArgs e)
        {
            selected_tab.Value = "renewvehicle";
            if (!PopulateTicketDetails(renewVehicleGridview)) return;
            TicketDetails.Text = "Renew vehicle";
            TicketApprove.Text = "Renew";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "viewVehicleListModel", "$('#viewVehicleListModel').modal();", true);
        }

        private bool PopulateTicketDetails(GridView gridView)
        {
            var ticketController = new TicketController();
            var tonnageController = new TonnageController();
            var ticketDetail = ticketController.Get(gridView.SelectedRow.Cells[1].Text);
            if (null == ticketDetail) return false;
            TicketId.Text = ticketDetail.TicketId;
            TicketGoodsType.Text = ticketDetail.GoodsType;
            var tonnageDetail = tonnageController.Get(ticketDetail.TonnageId);
            TicketTonnage.Text = tonnageDetail.TonnageTo;
            TicketMakeOftheVehicle.Text = ticketDetail.Make;
            TicketModelOftheVehicle.Text = ticketDetail.Model;
            TicketCubicCapacity.Text = ticketDetail.CubicCapacity;
            TicketLengthoftheVehicle.Text = ticketDetail.Length;
            TicketWidthoftheVehicle.Text = ticketDetail.Width;
            TicketHeightoftheVehicle.Text = ticketDetail.Height;
            TicketBreadthoftheVehicle.Text = ticketDetail.Breadth;
            TicketRegistrationNumber.Text = ticketDetail.Registration;
            TicketEngineNumber.Text = ticketDetail.EngineNumber;
            TicketChasisNumber.Text = ticketDetail.ChasisNumber;
            TicketNOCRTO.Checked = bool.Parse(ticketDetail.NOCRTO);
            TicketNOCPolice.Checked = bool.Parse(ticketDetail.NOCPolicy);
            TicketFCValidityFrom.Text = ticketDetail.FCValidityFrom;
            TicketFCValidity.Text = ticketDetail.FCValidity;
            TicketVehicleOwnerName.Text = ticketDetail.Name;
            TicketAddress.Text = ticketDetail.Address;
            TicketCity.Text = ticketDetail.City;
            TicketDistrict.Text = ticketDetail.District;
            TicketState.Text = ticketDetail.State;
            TicketCountry.Text = ticketDetail.Country;
            TicketInsuranceType.Text = ticketDetail.InsuranceType;
            TicketInsuranceCompany.Text = ticketDetail.InsuranceCompany;
            TicketValidityFrom.Text = ticketDetail.ValidityFrom;
            TicketValidityTo.Text = ticketDetail.Validity;
            TicketDriverName.Text = ticketDetail.DriverName;
            TicketDriverAge.Text = ticketDetail.DriverAge.ToString();
            TicketDriverAddress.Text = ticketDetail.DriverAddress;
            TicketDriverCity.Text = ticketDetail.DriverCity;
            TicketDriverDistrict.Text = ticketDetail.DriverDistrict;
            TicketDriverState.Text = ticketDetail.DriverState;
            TicketDriverCountry.Text = ticketDetail.DriverCountry;
            TicketDriverDLNumber.Text = ticketDetail.DriverDLNumber;
            TicketDriverMobileNumber.Text = ticketDetail.DriverMobileNumber;
            TicketDriverEmailId.Text = ticketDetail.DriverEmail;
            TicketDriverPanNumber.Text = ticketDetail.DriverPanCard;
            TicketDriverAadharCardNumber.Text = ticketDetail.DriverAadharCardNumber;
            return true;
        }

        private void LoadVehicles(DropDownList dropDownList)
        {
            dropDownList.Items.Clear();
            dropDownList.Items.Add("-- Select Vehicle No --");
            var ticketController = new TicketController();
            var ticketDetails = ticketController.Get().Where(x => x.TicketStatus == Global.UserStatus.Approved.ToString()).ToList();
            if (null == ticketDetails) return;
            foreach (var ticketDetail in ticketDetails)
            {
                dropDownList.Items.Add(ticketDetail.Registration);
            }
        }

        protected void OnEnableConfirmationYesClick(object sender, EventArgs e)
        {
            var ticketController = new TicketController();
            if (string.IsNullOrEmpty(DisableVehicleId.Text)) return;
            var ticketDetailslist = ticketController.Get();
            TicketDetail ticketDetails = null;
            if (null != ticketDetailslist)
            {
                ticketDetails = ticketDetailslist.Where(x => x.Registration == DisableVehicleId.Text).ToList().FirstOrDefault();
            }
            if (null == ticketDetails)
            {
                FailureMessage = "Please enter proper vehicle number";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }

            if (string.IsNullOrEmpty(ticketDetails.SAPermission) ||
              ticketDetails.SAPermission != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetails.TicketStatus) ||
              ticketDetails.TicketStatus != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (ticketDetails.VehicleStatus == Global.VehicleStatus.Active.ToString())
            {
                FailureMessage = "Already Vehicle is enabled";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            ticketDetails.VehicleStatus = Global.VehicleStatus.Active.ToString();
            ticketController.Put(ticketDetails);
            SuccessMessage = "Vehicle is enabled";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            timer.Interval = 1000;
            const string email = "itsupport@ltlindia.com";
            const string password = "ltlindia@2015";
            var txtTo = ticketDetails.DriverEmail;
            try
            {
                using (var mailMessage = new MailMessage(email, txtTo))
                {
                    const string subject = "LTL Message from Admin";
                    const string body = "Hello customer,\r\n Your vehicle has been disabled\r\n Regards LTL India";
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = false;
                    var smtp = new SmtpClient { Host = "mail.ltlindia.com", EnableSsl = true };
                    var networkCred = new NetworkCredential(email, password);
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = networkCred;
                    smtp.Port = 465;
                    smtp.Send(mailMessage);
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
                }
            }
            catch (Exception ex)
            {
                Console.Write("Exception inside the Email Notification is ", ex);
            }
            Session.Add("Update", true);
        }

        protected void OnDisableConfirmationYesClick(object sender, EventArgs e)
        {
            var ticketController = new TicketController();
            if (string.IsNullOrEmpty(DisableVehicleId.Text)) return;
            var ticketDetailslist = ticketController.Get();
            TicketDetail ticketDetails = null;
            if (null != ticketDetailslist)
            {
                ticketDetails = ticketDetailslist.Where(x => x.Registration == DisableVehicleId.Text).ToList().FirstOrDefault();
            }
            if (null == ticketDetails)
            {
                FailureMessage = "Please enter proper vehicle number";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }

            if (string.IsNullOrEmpty(ticketDetails.SAPermission) ||
              ticketDetails.SAPermission != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetails.TicketStatus) ||
              ticketDetails.TicketStatus != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (ticketDetails.VehicleStatus != Global.VehicleStatus.Active.ToString())
            {
                FailureMessage = "Already Vehicle is disabled";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            ticketDetails.VehicleStatus = Global.VehicleStatus.Disabled.ToString();
            ticketController.Put(ticketDetails);
            SuccessMessage = "Vehicle is disabled";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            timer.Interval = 1000;
            const string email = "itsupport@ltlindia.com";
            const string password = "ltlindia@2015";
            var txtTo = ticketDetails.DriverEmail;
            try
            {
                using (var mailMessage = new MailMessage(email, txtTo))
                {
                    const string subject = "LTL Message from Admin";
                    const string body = "Hello customer,\r\n Your vehicle has been disabled\r\n Regards LTL India";
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = false;
                    var smtp = new SmtpClient { Host = "mail.ltlindia.com", EnableSsl = true };
                    var networkCred = new NetworkCredential(email, password);
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = networkCred;
                    smtp.Port = 465;
                    smtp.Send(mailMessage);
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
                }
            }
            catch (Exception ex)
            {
                Console.Write("Exception inside the Email Notification is ", ex);
            }
            Session.Add("Update", true);
        }

        protected void OnDisableVehicle(object sender, EventArgs e)
        {
            selected_tab.Value = "disablevehicle";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalDisableYesNo", "$('#myModalDisableYesNo').modal();", true);
        }

        protected void OnEnableVehicle(object sender, EventArgs e)
        {
            selected_tab.Value = "disablevehicle";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalEnableYesNo", "$('#myModalEnableYesNo').modal();", true);
        }

        protected void PrintCurrentPage(object sender, CommandEventArgs e)
        {
            if (_commands.ContainsKey(e.CommandArgument.ToString()))
            {
                PrintPage(_commands[e.CommandArgument.ToString()] as GridView);
            }
        }

        protected void OnTicketNotificationGridChanged(object sender, EventArgs e)
        {
            var tonnageController = new TonnageController();
            var ticketController = new TicketController();
            var ticketDetail = ticketController.Get(TicketGridView.SelectedRow.Cells[1].Text);
            if (null == ticketDetail) return;
            notificationTicketId.Text = ticketDetail.TicketId;
            notificationGoodsType.Text = ticketDetail.GoodsType;
            var tonnageDetail = tonnageController.Get(ticketDetail.TonnageId);
            notificationTonnageCapacity.Text = tonnageDetail.TonnageTo;
            notificationMakeOftheVehicle.Text = ticketDetail.Make;
            notificationModelOftheVehicle.Text = ticketDetail.Model;
            notificationCubicCapacity.Text = ticketDetail.CubicCapacity;
            notificationLengthsoftheVehicle.Text = ticketDetail.Length;
            notificationWidthsoftheVehicle.Text = ticketDetail.Width;
            notificationBreadthsoftheVehicle.Text = ticketDetail.Breadth;
            notificationHeigthsoftheVehicle.Text = ticketDetail.Height;
            notificationRegistrationNumber.Text = ticketDetail.Registration;
            notificationEngineNumber.Text = ticketDetail.EngineNumber;
            notificationChasisNumber.Text = ticketDetail.ChasisNumber;
            notificationNOCRTO.Checked = bool.Parse(ticketDetail.NOCRTO);
            notificationNOCPolice.Checked = bool.Parse(ticketDetail.NOCPolicy);
            notificationVehiclePhotoFront.ImageUrl =
                string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                ticketDetail.VehiclePhotoFront);
            notificationVehiclePhotoBack.ImageUrl =
                string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                ticketDetail.VehiclePhotoBack);
            notificationVehiclePhotoLeft.ImageUrl =
                string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                ticketDetail.VehiclePhotoLeft);
            notificationVehiclePhotoRight.ImageUrl =
                string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                ticketDetail.VehiclePhotoRight);
            notificationFCValidityFrom.Text = ticketDetail.FCValidityFrom;
            notificationFCValidity.Text = ticketDetail.FCValidity;
            notificationVehicleOwnerName.Text = ticketDetail.Name;
            notificationAddress.Text = ticketDetail.Address;
            notificationCity.Text = ticketDetail.City;
            notificationDistrict.Text = ticketDetail.District;
            notificationState.Text = ticketDetail.State;
            notificationCountry.Text = ticketDetail.Country;
            notificationInsuranceType.Text = ticketDetail.InsuranceType;
            notificationInsuranceCompany.Text = ticketDetail.InsuranceCompany;
            notificationValidityFrom.Text = ticketDetail.ValidityFrom;
            notificationValidityTo.Text = ticketDetail.Validity;
            notificationDriverName.Text = ticketDetail.DriverName;
            notificationDriverAge.Text = ticketDetail.DriverAge.ToString();
            notificationDriverAddress.Text = ticketDetail.DriverAddress;
            notificationDriverCity.Text = ticketDetail.DriverCity;
            notificationDriverDistrict.Text = ticketDetail.DriverDistrict;
            notificationDriverState.Text = ticketDetail.DriverState;
            notificationDriverCountry.Text = ticketDetail.DriverCountry;
            notificationDriverDLNumber.Text = ticketDetail.DriverDLNumber;
            notificationDriverMobileNumber.Text = ticketDetail.DriverMobileNumber;
            notificationDriverEmailId.Text = ticketDetail.DriverEmail;
            notificationDriverPanNumber.Text = ticketDetail.DriverPanCard;
            notificationDriverAadharCardNumber.Text = ticketDetail.DriverAadharCardNumber;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "notificationTicketModel", "$('#notificationTicketModel').modal();", true);
        }

        protected void OnNotificationReSubmit(object sender, EventArgs e)
        {
            if (notificationGoodsType.SelectedIndex <= 0 ||
                notificationTonnageCapacity.SelectedIndex <= 0 ||
                string.IsNullOrEmpty(notificationMakeOftheVehicle.Text) ||
                string.IsNullOrEmpty(notificationModelOftheVehicle.Text) ||
                string.IsNullOrEmpty(notificationCubicCapacity.Text) ||
                string.IsNullOrEmpty(notificationLengthsoftheVehicle.Text) ||
                string.IsNullOrEmpty(notificationWidthsoftheVehicle.Text) ||
                string.IsNullOrEmpty(notificationBreadthsoftheVehicle.Text) ||
                string.IsNullOrEmpty(notificationHeigthsoftheVehicle.Text) ||
                string.IsNullOrEmpty(notificationRegistrationNumber.Text) ||
                string.IsNullOrEmpty(notificationEngineNumber.Text) ||
                string.IsNullOrEmpty(notificationChasisNumber.Text) ||
                string.IsNullOrEmpty(notificationFCValidityFrom.Text) ||
                string.IsNullOrEmpty(notificationFCValidity.Text) ||
                string.IsNullOrEmpty(notificationVehicleOwnerName.Text) ||
                string.IsNullOrEmpty(notificationAddress.Text) ||
                string.IsNullOrEmpty(notificationCity.Text) ||
                string.IsNullOrEmpty(notificationDistrict.Text) ||
                notificationState.SelectedIndex <= 0 ||
                string.IsNullOrEmpty(notificationCountry.Text) ||
                string.IsNullOrEmpty(notificationInsuranceType.Text) ||
                string.IsNullOrEmpty(notificationInsuranceCompany.Text) ||
                string.IsNullOrEmpty(notificationValidityFrom.Text) ||
                string.IsNullOrEmpty(notificationValidityTo.Text) ||
                string.IsNullOrEmpty(notificationDriverName.Text) ||
                string.IsNullOrEmpty(notificationDriverAge.Text) ||
                string.IsNullOrEmpty(notificationDriverAddress.Text) ||
                string.IsNullOrEmpty(notificationDriverCity.Text) ||
                string.IsNullOrEmpty(notificationDriverDistrict.Text) ||
                notificationDriverState.SelectedIndex <= 0 ||
                string.IsNullOrEmpty(notificationDriverCountry.Text) ||
                string.IsNullOrEmpty(notificationDriverDLNumber.Text) ||
                string.IsNullOrEmpty(notificationDriverMobileNumber.Text) ||
                string.IsNullOrEmpty(notificationDriverEmailId.Text) ||
                string.IsNullOrEmpty(notificationDriverPanNumber.Text) ||
                string.IsNullOrEmpty(notificationDriverAadharCardNumber.Text))
            {
                FailureMessage = "Please enter Mandatory fields";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            var ticketController = new TicketController();
            var ticketDetail = ticketController.Get(notificationTicketId.Text);
            if (null == ticketDetail) return;
            ticketDetail.TicketId = notificationTicketId.Text;
            ticketDetail.GoodsType = notificationGoodsType.Text;
            ticketDetail.TonnageId = notificationTonnageCapacity.SelectedIndex;
            ticketDetail.Make = notificationMakeOftheVehicle.Text;
            ticketDetail.Model = notificationModelOftheVehicle.Text;
            ticketDetail.CubicCapacity = notificationCubicCapacity.Text;
            ticketDetail.Length = notificationLengthsoftheVehicle.Text;
            ticketDetail.Width = notificationWidthsoftheVehicle.Text;
            ticketDetail.Breadth = notificationBreadthsoftheVehicle.Text;
            ticketDetail.Height = notificationHeigthsoftheVehicle.Text;
            ticketDetail.Registration = notificationRegistrationNumber.Text;
            ticketDetail.EngineNumber = notificationEngineNumber.Text;
            ticketDetail.ChasisNumber = notificationChasisNumber.Text;
            ticketDetail.NOCRTO = notificationNOCRTO.Checked.ToString();
            ticketDetail.NOCPolicy = notificationNOCPolice.Checked.ToString();
            ticketDetail.FCValidityFrom = notificationFCValidityFrom.Text;
            ticketDetail.FCValidity = notificationFCValidity.Text;
            ticketDetail.Name = notificationVehicleOwnerName.Text;
            ticketDetail.Address = notificationAddress.Text;
            ticketDetail.City = notificationCity.Text;
            ticketDetail.District = notificationDistrict.Text;
            ticketDetail.State = notificationState.Text;
            ticketDetail.Country = notificationCountry.Text;
            ticketDetail.InsuranceType = notificationInsuranceType.Text;
            ticketDetail.InsuranceCompany = notificationInsuranceCompany.Text;
            ticketDetail.ValidityFrom = notificationValidityFrom.Text;
            ticketDetail.Validity = notificationValidityTo.Text;
            ticketDetail.DriverName = notificationDriverName.Text;
            ticketDetail.DriverAge = int.Parse(notificationDriverAge.Text);
            ticketDetail.DriverAddress = notificationDriverAddress.Text;
            ticketDetail.DriverCity = notificationDriverCity.Text;
            ticketDetail.DriverDistrict = notificationDriverDistrict.Text;
            ticketDetail.DriverState = notificationDriverState.Text;
            ticketDetail.DriverCountry = notificationDriverCountry.Text;
            ticketDetail.DriverDLNumber = notificationDriverDLNumber.Text;
            ticketDetail.DriverMobileNumber = notificationDriverMobileNumber.Text;
            ticketDetail.DriverEmail = notificationDriverEmailId.Text;
            ticketDetail.DriverPanCard = notificationDriverPanNumber.Text;
            ticketDetail.DriverAadharCardNumber = notificationDriverAadharCardNumber.Text;
            ticketDetail.DHPermission = ticketDetail.SHPermission = ticketDetail.SAPermission = string.Empty;
            ticketDetail.DHPermission = Global.UserStatus.Pending.ToString();
            ticketDetail.BAPermission = Global.UserStatus.Approved.ToString();
            ticketController.Put(ticketDetail);
            SuccessMessage = "Re submitted successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            timer.Interval = 1000;
            Session.Add("Update", true);
        }

        private void PrintPage(GridView gridview)
        {
            gridview.PagerSettings.Visible = false;
            gridview.DataBind();
            var sw = new StringWriter();
            var hw = new HtmlTextWriter(sw);
            gridview.RenderControl(hw);
            var gridHTML = sw.ToString().Replace("\"", "'")
                .Replace(System.Environment.NewLine, "");
            var sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            sb.Append(gridHTML);
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            gridview.PagerSettings.Visible = true;
            gridview.DataBind();
        }

        private void ExportPage(GridView gridView)
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=GridViewExport.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                using (StringWriter sw = new StringWriter())
                {
                    HtmlTextWriter hw = new HtmlTextWriter(sw);

                    //To Export all pages
                    gridView.AllowPaging = false;
                    gridView.DataBind();

                    gridView.HeaderRow.BackColor = Color.White;
                    foreach (TableCell cell in gridView.HeaderRow.Cells)
                    {
                        cell.BackColor = gridView.HeaderStyle.BackColor;
                    }
                    foreach (GridViewRow row in gridView.Rows)
                    {
                        row.BackColor = Color.White;
                        foreach (TableCell cell in row.Cells)
                        {
                            if (row.RowIndex % 2 == 0)
                            {
                                cell.BackColor = gridView.AlternatingRowStyle.BackColor;
                            }
                            else
                            {
                                cell.BackColor = gridView.RowStyle.BackColor;
                            }
                            cell.CssClass = "textmode";
                        }
                    }

                    gridView.RenderControl(hw);

                    //style to format numbers to string
                    string style = @"<style> .textmode { } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                //throw new Exception(ex.Message);
            }
        }

        protected void OnViewVehiclePopUpOk(object sender, EventArgs e)
        {
            if (TicketApprove.Text != "Renew") return;
            var ticketController = new TicketController();
            var ticketDetail = ticketController.Get(TicketId.Text);
            if (null == ticketDetail) return;
            ticketDetail.RenewalCounter = ticketDetail.RenewalCounter + 1;
            ticketDetail.TicketStartDate = DateTime.Today;
            ticketDetail.TicketEndDate = DateTime.Today.AddMonths(1).AddDays(-1);
            ticketController.Put(ticketDetail);
            SuccessMessage = "Vehicle renewed successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            timer.Interval = 1000;
            Session.Add("Update", true);
        }

        protected void ExportCurrentpage(object sender, EventArgs e)
        {
            var linkButton = sender as LinkButton;
            if (null != linkButton)
            {
                ExportPage(_commands[linkButton.CommandArgument] as GridView);
            }
        }

        protected void OnVehicleDriverIdChanged(object sender, EventArgs e)
        {
            UpdateDriverDetailsPanel.Visible = false;
            var ticketController = new TicketController();
            var ticketDetailslist = ticketController.Get();
            TicketDetail ticketDetails = null;
            if (null != ticketDetailslist)
            {
                ticketDetails = ticketDetailslist.Where(x => x.Registration == UpdateDriverVehicleId.Text).ToList().FirstOrDefault();
            }
            if (null == ticketDetails)
            {
                FailureMessage = "Please enter proper vehicle number";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetails.SAPermission) ||
            ticketDetails.SAPermission != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Driver approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetails.TicketStatus) ||
              ticketDetails.TicketStatus != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            UpdateDriverName.Text = ticketDetails.DriverName;
            UpdateDriverAge.Text = ticketDetails.DriverAge.ToString();
            UpdateDriverAddress.Text = ticketDetails.DriverAddress;
            UpdateDriverCity.Text = ticketDetails.DriverCity;
            UpdateDriverDistrict.Text = ticketDetails.DriverDistrict;
            DriverDetailDropDown.Text = ticketDetails.DriverState;
            UpdateDriverCoutry.Text = ticketDetails.DriverCountry;
            UpdateDriverDLNumber.Text = ticketDetails.DriverDLNumber;
            UpdateDriverPhoto.Checked = bool.Parse(ticketDetails.DriverPhoto);
            UpdateDriverMobileNumber.Text = ticketDetails.DriverMobileNumber;
            UpdateDriverEmailId.Text = ticketDetails.DriverEmail;
            UpdateDriverAadharCardNumber.Text = ticketDetails.DriverAadharCardNumber;
            UpdateDriverPanNumber.Text = ticketDetails.DriverPanCard;
            UpdateDriverDetailsPanel.Visible = true;

        }
        protected void OnVehicleIdSearch(object sender, EventArgs e)
        {
            UpdateVehiclePanel.Visible = false;
            var ticketController = new TicketController();
            var ticketDetailslist = ticketController.Get();
            TicketDetail ticketDetails = null;
            if (null != ticketDetailslist)
            {
                ticketDetails = ticketDetailslist.Where(x => x.Registration == UpdateVehicleId.Text).ToList().FirstOrDefault();
            }
            if (null == ticketDetails)
            {
                FailureMessage = "Please enter proper vehicle number";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetails.SAPermission) ||
                ticketDetails.SAPermission != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetails.TicketStatus) ||
              ticketDetails.TicketStatus != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (ticketDetails.VehicleStatus != Global.VehicleStatus.Active.ToString())
            {
                FailureMessage = "Vehicle is disabled";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            UpdateVehicleGoodsType.Text = ticketDetails.GoodsType;
            UpdateVehicleTonnageCapacity.SelectedIndex = ticketDetails.TonnageId;
            UpdateVehicleMakeoftheVehicle.Text = ticketDetails.Make;
            UpdateVehicleModelofthevehicle.Text = ticketDetails.Model;
            UpdateVehicleCC.Text = ticketDetails.CubicCapacity;
            UpdateVehicleLength.Text = ticketDetails.Length;
            UpdateVehicleWidth.Text = ticketDetails.Width;
            UpdateVehicleBreadth.Text = ticketDetails.Breadth;
            UpdateVehicleHeight.Text = ticketDetails.Height;
            UpdateVehicleEngineNumber.Text = ticketDetails.EngineNumber;
            UpdateVehicleChassisNumber.Text = ticketDetails.ChasisNumber;
            UpdateVehicleNOCFromRTO.Checked = bool.Parse(ticketDetails.NOCRTO);
            UpdateVehicleNOCFromPolice.Checked = bool.Parse(ticketDetails.NOCPolicy);
            UpdateVehiclePhoto.Checked = bool.Parse(ticketDetails.Photo);
            UpdateVehicleFCValidityTo.Text = ticketDetails.FCValidity;
            UpdateVehicleFCValidityFrom.Text = ticketDetails.FCValidityFrom;
            UpdateVehicleOwnerName.Text = ticketDetails.Name;
            UpdateVehicleOwnerAddress.Text = ticketDetails.Address;
            UpdateVehicleCity.Text = ticketDetails.City;
            UpdateVehicleDistrict.Text = ticketDetails.District;
            UpdateVehicleDetailsDropDown.Text = ticketDetails.State;
            UpdateVehicleCountry.Text = ticketDetails.Country;
            UpdateVehiclePanel.Visible = true;
        }

        protected void OnUpdateDriverDetailsCancel(object sender, EventArgs e)
        {
            UpdateDriverVehicleId.SelectedIndex = 0;
            UpdateDriverDetailsPanel.Visible = false;
        }

        protected void OnUpdateDriverDetails(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(UpdateDriverVehicleId.Text)) return;
            var ticketController = new TicketController();
            var ticketDetailslist = ticketController.Get();
            TicketDetail ticketDetail = null;
            if (null != ticketDetailslist)
            {
                ticketDetail = ticketDetailslist.Where(x => x.Registration == UpdateDriverVehicleId.Text).ToList().FirstOrDefault();
            }
            if (null == ticketDetail)
            {
                FailureMessage = "Please enter proper driver Id";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetail.SAPermission) ||
                ticketDetail.SAPermission != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetail.TicketStatus) ||
              ticketDetail.TicketStatus != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            ticketDetail.DriverName = UpdateDriverName.Text;
            ticketDetail.DriverAge = int.Parse(UpdateDriverAge.Text);
            ticketDetail.DriverAddress = UpdateDriverAddress.Text;
            ticketDetail.DriverCity = UpdateDriverCity.Text;
            ticketDetail.DriverDistrict = UpdateDriverDistrict.Text;
            ticketDetail.DriverState = DriverDetailDropDown.Text;
            ticketDetail.DriverCountry = UpdateDriverCoutry.Text;
            ticketDetail.DriverDLNumber = UpdateDriverDLNumber.Text;
            ticketDetail.DriverPhoto = UpdateDriverPhoto.Checked.ToString(CultureInfo.CurrentUICulture);
            ticketDetail.DriverMobileNumber = UpdateDriverMobileNumber.Text;
            ticketDetail.DriverEmail = UpdateDriverEmailId.Text;
            ticketDetail.DriverPanCard = UpdateDriverPanNumber.Text;
            ticketDetail.DriverAadharCardNumber = UpdateDriverAadharCardNumber.Text;
            ticketController.Put(ticketDetail);
            SuccessMessage = "Updated driver details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            timer.Interval = 1000;
            UpdateDriverVehicleId.SelectedIndex = 0;
            UpdateDriverDetailsPanel.Visible = false;
        }

        protected void OnUpdateVehicleDetailsCancel(object sender, EventArgs e)
        {
            UpdateVehicleId.SelectedIndex = 0;
            UpdateVehiclePanel.Visible = false;
        }

        protected void OnUpdateVehicleDetails(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(UpdateVehicleId.Text)) return;
            var ticketController = new TicketController();
            var ticketDetailslist = ticketController.Get();
            TicketDetail ticketDetail = null;
            if (null != ticketDetailslist)
            {
                ticketDetail = ticketDetailslist.Where(x => x.Registration == UpdateVehicleId.Text).ToList().FirstOrDefault();
            }
            if (null == ticketDetail)
            {
                FailureMessage = "Please enter proper vehicle Id";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                UpdateVehiclePanel.Visible = false;
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetail.SAPermission) ||
                ticketDetail.SAPermission != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                UpdateVehiclePanel.Visible = false;
                timer.Interval = 1000;
                return;
            }
            if (string.IsNullOrEmpty(ticketDetail.TicketStatus) ||
                ticketDetail.TicketStatus != Global.UserStatus.Approved.ToString())
            {
                FailureMessage = "Vehicle approval is pending";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                UpdateVehiclePanel.Visible = false;
                timer.Interval = 1000;
                return;
            }
            ticketDetail.GoodsType = UpdateVehicleGoodsType.Text;
            ticketDetail.TonnageId = UpdateVehicleTonnageCapacity.SelectedIndex;
            ticketDetail.Make = UpdateVehicleMakeoftheVehicle.Text;
            ticketDetail.Model = UpdateVehicleModelofthevehicle.Text;
            ticketDetail.CubicCapacity = UpdateVehicleCC.Text;
            ticketDetail.Length = UpdateVehicleLength.Text;
            ticketDetail.Width = UpdateVehicleWidth.Text;
            ticketDetail.Breadth = UpdateVehicleBreadth.Text;
            ticketDetail.Height = UpdateVehicleHeight.Text;
            ticketDetail.EngineNumber = UpdateVehicleEngineNumber.Text;
            ticketDetail.ChasisNumber = UpdateVehicleChassisNumber.Text;
            ticketDetail.NOCRTO = UpdateVehicleNOCFromRTO.Checked.ToString(CultureInfo.InvariantCulture);
            ticketDetail.NOCPolicy = UpdateVehicleNOCFromPolice.Checked.ToString(CultureInfo.InvariantCulture);
            ticketDetail.Photo = UpdateVehiclePhoto.Checked.ToString(CultureInfo.InvariantCulture);
            ticketDetail.FCValidity = UpdateVehicleFCValidityTo.Text;
            ticketDetail.FCValidityFrom = UpdateVehicleFCValidityFrom.Text;
            ticketDetail.Name = UpdateVehicleOwnerName.Text;
            ticketDetail.Address = UpdateVehicleOwnerAddress.Text;
            ticketDetail.City = UpdateVehicleCity.Text;
            ticketDetail.District = UpdateVehicleDistrict.Text;
            ticketDetail.State = UpdateVehicleDetailsDropDown.Text;
            ticketDetail.Country = UpdateVehicleCountry.Text;
            ticketController.Put(ticketDetail);
            SuccessMessage = "Updated vehicle details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            timer.Interval = 1000;
            UpdateVehiclePanel.Visible = false;
            UpdateVehicleId.SelectedIndex = 0;
        }

        private bool CanUploadSelectedPhoto(string filePath)
        {
            // Read the file and convert it to Byte Array
            var filename = Path.GetFileName(filePath);
            var ext = Path.GetExtension(filename);
            //Set the content type based on File Extension
            switch (ext)
            {
                case ".jpg":
                case ".jpeg":
                case ".png":
                    return true;
            }
            return false;
        }

        private string SelectedPhotoContentType(string filePath)
        {
            // Read the file and convert it to Byte Array
            var filename = Path.GetFileName(filePath);
            var ext = Path.GetExtension(filename);
            //Set the content type based on File Extension
            switch (ext)
            {
                case ".jpg":
                    return ".jpg";
                case ".jpeg":
                    return ".jpeg";
                case ".png":
                    return ".png";
            }
            return string.Empty;
        }

        protected void OnCreateVehicleTicket(object sender, EventArgs e)
        {
            if (!VehiclePhotoUploadFront.HasFile || !VehiclePhotoUploadBack.HasFile ||
                !VehiclePhotoUploadRightSide.HasFile || !VehiclePhotoUploadLeftSide.HasFile)
            {
                FailureMessage = "Please upload photo(s)";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            if (!CanUploadSelectedPhoto(VehiclePhotoUploadFront.PostedFile.FileName) || !CanUploadSelectedPhoto(VehiclePhotoUploadBack.PostedFile.FileName)
                || !CanUploadSelectedPhoto(VehiclePhotoUploadRightSide.PostedFile.FileName) || !CanUploadSelectedPhoto(VehiclePhotoUploadLeftSide.PostedFile.FileName))
            {

                FailureMessage = "Selected photo format is not supported";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            var directoryPath = Server.MapPath(string.Format("~/{0}/{1}", "VehicleImages", RegistrationNumber.Text.Trim()));
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('vehicle number already exists.');", true);
            }

            VehiclePhotoUploadFront.SaveAs(Server.MapPath(string.Format("~/{0}/{1}/", "VehicleImages", RegistrationNumber.Text.Trim()) + Path.GetFileName(VehiclePhotoUploadFront.PostedFile.FileName)));
            VehiclePhotoUploadBack.SaveAs(Server.MapPath(string.Format("~/{0}/{1}/", "VehicleImages", RegistrationNumber.Text.Trim()) + Path.GetFileName(VehiclePhotoUploadBack.PostedFile.FileName)));
            VehiclePhotoUploadLeftSide.SaveAs(Server.MapPath(string.Format("~/{0}/{1}/", "VehicleImages", RegistrationNumber.Text.Trim()) + Path.GetFileName(VehiclePhotoUploadLeftSide.PostedFile.FileName)));
            VehiclePhotoUploadRightSide.SaveAs(Server.MapPath(string.Format("~/{0}/{1}/", "VehicleImages", RegistrationNumber.Text.Trim()) + Path.GetFileName(VehiclePhotoUploadRightSide.PostedFile.FileName)));
            Session.Add(VehicleFileNameFront, Path.GetFileName(VehiclePhotoUploadFront.PostedFile.FileName));
            Session.Add(VehicleFileNameBack, Path.GetFileName(VehiclePhotoUploadBack.PostedFile.FileName));
            Session.Add(VehicleFileNameLeft, Path.GetFileName(VehiclePhotoUploadLeftSide.PostedFile.FileName));
            Session.Add(VehicleFileNameRight, Path.GetFileName(VehiclePhotoUploadRightSide.PostedFile.FileName));
            //Session.Add(VehicleFileBytes, VehiclePhotoUpload.FileBytes);
            VehicleInsurancePanel.Visible = true;
            VehicleDetailsPanel.Visible = false;
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //
        }
        protected void OnCreateInsuranceTicket(object sender, EventArgs e)
        {
            VehicleInsurancePanel.Visible = false;
            VehicleDriverPanel.Visible = true;
        }

        protected void OnCreateDriverTicket(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(DriverPhotoUpload.PostedFile.FileName))
            {
                ApprovalComments();
                return;
            }
            FailureMessage = "Please upload photo";
            failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
            timer.Interval = 1000;
        }

        private void ApprovalComments()
        {
            if (!CanUploadSelectedPhoto(DriverPhotoUpload.PostedFile.FileName))
            {
                FailureMessage = "Selected photo format is not supported";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
                return;
            }
            DriverPhotoUpload.SaveAs(Server.MapPath(string.Format("~/{0}/{1}/", "VehicleImages", RegistrationNumber.Text.Trim()) + Path.GetFileName(DriverPhotoUpload.PostedFile.FileName)));
            var userController = new UserController();
            var newUserDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            var userId = newUserDetail.UserId;
            var ticketDetail = new TicketDetail
            {
                UserId = userId,
                GoodsType = GoodsType.Text,
                TonnageId = tonnageCapacity.SelectedIndex,
                Make = MakeOftheVehicle.Text,
                Model = ModelOftheVehicle.Text,
                CubicCapacity = CubicCapacity.Text,
                Length = LengthoftheVehicle.Text,
                Width = WidthoftheVehicle.Text,
                Breadth = BreadthoftheVehicle.Text,
                Height = HeightoftheVehicle.Text,
                Registration = RegistrationNumber.Text,
                EngineNumber = EngineNumber.Text,
                ChasisNumber = ChasisNumber.Text,
                NOCRTO = NOCRTO.Checked.ToString(CultureInfo.CurrentUICulture),
                NOCPolicy = NOCPolice.Checked.ToString(CultureInfo.CurrentUICulture),
                VehiclePhotoFront = Session[VehicleFileNameFront] as string,
                VehiclePhotoBack = Session[VehicleFileNameBack] as string,
                VehiclePhotoLeft = Session[VehicleFileNameLeft] as string,
                VehiclePhotoRight = Session[VehicleFileNameRight] as string,
                //Photo = true.ToString(CultureInfo.InvariantCulture),
                //PhotoFileName = Session[VehicleFileNameFront] as string,
                //PhotoType = SelectedPhotoContentType(Session[VehicleFileName] as string),
                //PhotoContent = Session[VehicleFileBytes] as byte[],
                FCValidity = FCValidity.Text,
                FCValidityFrom = FCValidityFrom.Text,
                Name = VehicleOwnerName.Text,
                Address = Address.Text,
                City = City.Text,
                District = District.Text,
                State = State.Text,
                Country = Country.Text,
                InsuranceType = InsuranceType.Text,
                InsuranceCompany = InsuranceCompany.Text,
                Validity = ValidityTo.Text,
                ValidityFrom = ValidityFrom.Text,
                DriverName = DriverName.Text,
                DriverAge = int.Parse(DriverAge.Text),
                DriverAddress = DriverAddress.Text,
                DriverCity = DriverCity.Text,
                DriverDistrict = DriverDistrict.Text,
                DriverState = DriverState.Text,
                DriverCountry = DriverCountry.Text,
                DriverDLNumber = DriverDLNumber.Text,
                DriverPhoto = true.ToString(CultureInfo.InvariantCulture),
                DriverPhotoFileName = Path.GetFileName(DriverPhotoUpload.PostedFile.FileName),
                //DriverPhotoType = SelectedPhotoContentType(DriverPhotoUpload.PostedFile.FileName),
                //DriverPhotoContent = DriverPhotoUpload.FileBytes,
                DriverMobileNumber = DriverMobileNumber.Text,
                DriverEmail = DriverEmailId.Text,
                DriverPanCard = DriverPanNumber.Text,
                DriverAadharCardNumber = DriverAadharCardNumber.Text,
                VehicleStatus = Global.VehicleStatus.Active.ToString()
            };

            switch (newUserDetail.Role)
            {
                case Global.EmployeeType.BusinessAssociate:
                    {
                        ticketDetail.BAPermission = Global.UserStatus.Approved.ToString();
                        ticketDetail.DHPermission = Global.UserStatus.Pending.ToString();
                        break;
                    }
            }

            var ticketController = new TicketController();
            ticketController.Post(ticketDetail);
            SuccessMessage = "Ticket submitted successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            timer.Interval = 1000;
            GeneratedTicketIdLabel.Text = "Your Ticket Id is :" + ticketController.TicketId;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalTicketId", "$('#myModalTicketId').modal();", true);
            VehicleDriverPanel.Visible = false;
            VehicleDetailsPanel.Visible = true;
            //Email-Notiification/

            //const string email = "itsupport@ltlindia.com";
            //const string password = "ltlindia@2015";
            //var txtTo = DriverEmailId.Text;
            //try
            //{
            //    using (var mailMessage = new MailMessage(email, txtTo))
            //    {
            //        const string subject = "LTL Message from Admin";
            //        const string body = "Hello customer,\r\n Your vehicle has been disabled\r\n Regards LTL India";
            //        mailMessage.Subject = subject;
            //        mailMessage.Body = body;
            //        mailMessage.IsBodyHtml = false;
            //        var smtp = new SmtpClient { Host = "mail.ltlindia.com", EnableSsl = true };
            //        var networkCred = new NetworkCredential(email, password);
            //        smtp.UseDefaultCredentials = false;
            //        smtp.Credentials = networkCred;
            //        smtp.Port = 465;
            //        smtp.Send(mailMessage);
            //        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
            //    }
            //}

            //catch (Exception ex)
            //{
            //    Console.Write("Exception inside the Email Notification is ", ex);

            //}

            //GoodsType.SelectedIndex = State.SelectedIndex = DriverState.SelectedIndex = 0;
            //NOCRTO.Checked = NOCPolice.Checked = false;
            //MakeOftheVehicle.Text = ModelOftheVehicle.Text = CubicCapacity.Text = DimensionsoftheVehicle.Text = RegistrationNumber.Text = EngineNumber.Text = ChasisNumber.Text = FCValidity.Text =
            //    FCValidityFrom.Text = VehicleOwnerName.Text = Address.Text = City.Text = District.Text = Country.Text = InsuranceType.Text =
            //    InsuranceCompany.Text = ValidityTo.Text = ValidityFrom.Text = DriverName.Text = DriverAge.Text = DriverAddress.Text = DriverCity.Text = DriverDistrict.Text =
            //    DriverCountry.Text = DriverDLNumber.Text = DriverMobileNumber.Text = DriverEmailId.Text = DriverPanNumber.Text = DriverAadharCardNumber.Text = string.Empty;

            Session.Add("Update", true);
        }

        protected void OnBackInsuranceTicket(object sender, EventArgs e)
        {
            VehicleDetailsPanel.Visible = true;
            VehicleInsurancePanel.Visible = false;
        }
        protected void OnBackDriverTicket(object sender, EventArgs e)
        {
            VehicleInsurancePanel.Visible = true;
            VehicleDriverPanel.Visible = false;
        }

        protected void ViewVehicleGridView_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            selected_tab.Value = "viewvehicle";
        }

        protected void RenewVehicleGridView_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            selected_tab.Value = "renewvehicle";
        }

        protected void OnViewBusinessAssociateNotification(object sender, QueryCreatedEventArgs e)
        {
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;

            var orders = e.Query.Cast<TicketDetail>();

            var query = from order in orders
                        where order.UserId == loggedinUserDetails.UserId && order.DHPermission == "Rejected"
                        select order;
            e.Query = query;
            if (!query.Any())
            {
                pendingNotificationPanel.Visible = true;
                PrintLinkButton.Visible = false;
                ExportLinkButton.Visible = false;
            }
            else
            {
                pendingNotificationPanel.Visible = false;
            }
        }

        protected void renewVehicleEntityDataSource_OnQueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;
            var endDate = DateTime.Today.AddDays(-5);
            var orders = e.Query.Cast<TicketDetail>();
            var query = from order in orders
                        where order.UserId == loggedinUserDetails.UserId && order.TicketEndDate <= endDate
                        select order;
            e.Query = query;
            if (!query.Any())
            {
                lnkPrint.Visible = false;
                LinkButton1.Visible = false;
            }
        }

        protected void ViewVehicleEntityDataSource_OnQueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;

            var orders = e.Query.Cast<TicketDetail>();
            e.Query = from order in orders
                      where order.UserId == loggedinUserDetails.UserId
                      select order;
        }

        protected void ComissionGrid_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            selected_tab.Value = "Commission";
        }

        protected void CommissionDataSource_OnQueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;
            var status = Global.UserStatus.Approved.ToString();
            var orders = e.Query.Cast<TicketDetail>();
            e.Query = from order in orders
                      where order.UserId == loggedinUserDetails.UserId && order.TicketStatus == status
                      select order;
        }

        protected void EntityDataSource4_OnQueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var userController = new UserController();
            var userDetails = userController.Get();
            if (null == userDetails) return;
            var users = userDetails as UserDetail[] ?? userDetails.ToArray();
            var loggedinUserDetails = users.Single(x => x.Name == User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == loggedinUserDetails) return;

            var orders = e.Query.Cast<TicketDetail>();
            var query = from order in orders
                        where order.UserId == loggedinUserDetails.UserId
                        select order;
            e.Query = query;
            if (!query.Any())
            {
                LinkButton6.Visible = false;
                LinkButton7.Visible = false;
            }
        }

        protected void ViewAccountDetailsEntityDataSource_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var query = e.Query.Cast<AccountDetail>();
            e.Query = query;
            if (!query.Any())
            {
                viewAccountDetailsPrintLinkButton.Visible = false;
                viewAccountDetailsExportLinkButton.Visible = false;
            }
        }

        protected void commissionPrintLinkButton_OnClick(object sender, EventArgs e)
        {
            selected_tab.Value = "Commission";
            PrintPage(ComissionGrid);
        }

        protected void commissionButton_OnClick(object sender, EventArgs e)
        {
            if (DatePickerstart.IsValidDate && DatePickerEnd.IsValidDate)
            {
                if (DatePickerstart.CalendarDate > DatePickerEnd.CalendarDate)
                {
                    FailureMessage = "Start date cannot be greater than end date";
                    failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                    timer.Interval = 1000;
                    return;
                }
                CalculateTotalListViewCommission();
            }
            else
            {
                FailureMessage = "Please enter proper date";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                timer.Interval = 1000;
            }
        }
    }
}