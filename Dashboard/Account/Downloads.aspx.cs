﻿using Dashboard.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dashboard.Account
{
    public partial class Downloads : System.Web.UI.Page
    {
        private Dictionary<string, object> _commands = new Dictionary<string, object>();
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected string FailureMessage
        {
            get;
            private set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


            }
            AddCommands();
        }
        private void AddCommands()
        {
            _commands.Add("HypBA", HypBA);
            _commands.Add("HypBP", HypBP);
            _commands.Add("HypSH", HypSH);
            _commands.Add("HypDH", HypDH);

        }
        protected void DownloadFile_Common(object sender, CommandEventArgs e)
        {
            if (_commands.ContainsKey(e.CommandArgument.ToString()))
            {
                DetailsofFile(_commands[e.CommandArgument.ToString()] as LinkButton);
            }

        }

        protected void DetailsofFile(LinkButton hyper)
        {
            var filenameindex = hyper.CommandArgument;
            
            string allowedExtensions = ".mp4,.pdf,.m4v,.gif,.jpg,.png,.swf,.css,.htm,.html,.txt";
            string fileName = "";
            // edit this list to allow file types - do not allow sensitive file types like .cs or .config

            fileName = "Images/" + filenameindex + ".pdf";
            string filePath = "";

            //if (Request.QueryString["file"] != null) fileName = Request.QueryString["file"].ToString();
            //if (Request.QueryString["path"] != null) filePath = Request.QueryString["path"].ToString();

            if (fileName != "" && fileName.IndexOf(".") > 0)
            {
                bool extensionAllowed = false;
                // get file extension
                string fileExtension = fileName.Substring(fileName.LastIndexOf('.'), fileName.Length - fileName.LastIndexOf('.'));

                // check that we are allowed to download this file extension
                string[] extensions = allowedExtensions.Split(',');
                for (int a = 0; a < extensions.Length; a++)
                {
                    if (extensions[a] == fileExtension)
                    {
                        extensionAllowed = true;
                        break;
                    }
                }

                if (extensionAllowed)
                {
                    // check to see that the file exists 
                    if (File.Exists(Server.MapPath(filePath + '/' + fileName)))
                    {

                        // for iphones and ipads, this script can cause problems - especially when trying to view videos, so we will redirect to file if on iphone/ipad
                        // if (Request.UserAgent.ToLower().Contains("iphone") || Request.UserAgent.ToLower().Contains("ipad")) { Response.Redirect(filePath + '/' + fileName); }
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                        Response.WriteFile(Server.MapPath(filePath + '/' + fileName));
                        Response.End();
                    }
                    else
                    {
                        FailureMessage = "File could not be found";
                        failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                        return;
                    }
                }
                else
                {
                    FailureMessage = "File extension is not allowed";
                    failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                }
            }
            else
            {
                FailureMessage = "Error - no file to download";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }


    }
}