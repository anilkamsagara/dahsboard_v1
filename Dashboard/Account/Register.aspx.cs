﻿using System;
using System.Globalization;
using System.Web.UI;
using Dashboard.Controllers;
using Dashboard.Models;

namespace Dashboard.Account
{
    public partial class Register : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture)))
                {
                    Response.Redirect("~/");
                }
                GetEmployeeTypes();
                GetIndianStates();
            }
        }
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected string FailureMessage
        {
            get;
            private set;
        }

        protected void MessagePanelClick(object sender, EventArgs e)
        {
            ErrorMessage.Text = SuccessMessage = FailureMessage = string.Empty;
            successMessage.Visible = failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        }
        private void GetIndianStates()
        {
            State.Items.Add("--Select State--");
            State.Items.Add("Andhra Pradesh");
            State.Items.Add("Arunachal Pradesh");
            State.Items.Add("Assam");
            State.Items.Add("Bihar");
            State.Items.Add("Chhattisgarh");
            State.Items.Add("Goa");
            State.Items.Add("Gujarat");
            State.Items.Add("Haryana");
            State.Items.Add("Himachal Pradesh");
            State.Items.Add("Jammu and Kashmir");
            State.Items.Add("Jharkhand");
            State.Items.Add("Karnataka");
            State.Items.Add("Kerala");
            State.Items.Add("Madhya Pradesh");
            State.Items.Add("Maharashtra");
            State.Items.Add("Manipur");
            State.Items.Add("Meghalaya");
            State.Items.Add("Mizoram");
            State.Items.Add("Nagaland");
            State.Items.Add("Odisha");
            State.Items.Add("Punjab");
            State.Items.Add("Rajasthan");
            State.Items.Add("Sikkim");
            State.Items.Add("Tamil Nadu");
            State.Items.Add("Telangana");
            State.Items.Add("Tripura");
            State.Items.Add("Uttar Pradesh");
            State.Items.Add("Uttarakhand");
            State.Items.Add("West Bengal");
        }

        private void GetEmployeeTypes()
        {
            EmployeeType.Items.Add("--Select Employee Type--");
            EmployeeType.Items.Add(Global.EmployeeType.BusinessAssociate.ToString());
            EmployeeType.Items.Add(Global.EmployeeType.DistrictHead.ToString());
            EmployeeType.Items.Add(Global.EmployeeType.StatetHead.ToString());
        }


        protected void OnRegister(object sender, EventArgs e)
        {
            var userRepository = new UserRepository();
            var result = userRepository.Get(UserName.Text);
            if (null != result)
            {
                ErrorMessage.Text = "This user name is already taken";
                return;
            }
            RegisterPanel.Visible = false;
            PersonalDetailsPanel.Visible = true;
        }

        protected void OnAddClick(object sender, EventArgs e)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalPartner", "$('#myModalPartner').modal();", true);
        }
        protected void OnDocumentDetailsBack(object sender, EventArgs e)
        {
            DocumentDetailsPanel.Visible = false;
            BankDetailsPanel.Visible = true;
        }
        protected void OnCompanyDetailsBack(object sender, EventArgs e)
        {
            CompanyDetailsPanel.Visible = false;
            PersonalDetailsPanel.Visible = true;
        }
        protected void OnBankBack(object sender, EventArgs e)
        {
            BankDetailsPanel.Visible = false;
            CompanyDetailsPanel.Visible = true;
        }
        protected void OnBankNext(object sender, EventArgs e)
        {
            BankDetailsPanel.Visible = false;
            DocumentDetailsPanel.Visible = true;
        }

        protected void OnCompanyDetailsNext(object sender, EventArgs e)
        {
            CompanyDetailsPanel.Visible = false;
            BankDetailsPanel.Visible = true;
        }
        protected void OnDocumentDetailsNext(object sender, EventArgs e)
        {
            Comments.Text = string.Empty;
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalComments", "$('#myModalComments').modal();", true);
        }

        protected void OnPartnerDetails(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(PartnerName.Text))
            {
                FailureMessage = "Please enter partner name";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            if (string.IsNullOrEmpty(PartnerAge.Text))
            {
                FailureMessage = "Please enter partner age";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            if (string.IsNullOrEmpty(PartnerAddress.Text))
            {
                FailureMessage = "Please enter partner address";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;

            var partnerController = new PartnerController();
            var partnerDetail = new PartnerDetail
            {
                Age = PartnerAge.Text,
                Name = PartnerName.Text,
                Address = PartnerAddress.Text,
                UserId = userDetail.UserId
            };
            partnerController.Post(partnerDetail);
            SuccessMessage = "Updated partner details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            PartnerAddress.Text = PartnerAge.Text = PartnerName.Text = string.Empty;
        }

        protected void OnApprovalComments(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Comments.Text))
            {
                FailureMessage = "Please enter comments";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }

            var userDetail = new UserDetail
            {
                Name = UserName.Text,
                Password = Password.Text,
                Status = Global.UserStatus.Pending.ToString()
            };

            //var personalDetail = Session["PersonalDetail"] as PersonalDetail;
            var personalDetail = new PersonalDetail
            {
                Name = Name.Text,
                Address = Address.Text,
                City = City.Text,
                District = District.Text,
                State = State.Text,
                Country = Country.Text,
                Age = Convert.ToInt32(Age.Text),
                EmailId = EmailId.Text,
                MobileNumber = MobileNumber.Text,
                PanCard = PanCard.Text,
                AadharCardNumber = AadharCardNumber.Text,
                EmployeeType = EmployeeType.Text
            };
            var usrController = new UserController();
            userDetail.Role = personalDetail.EmployeeType;
            usrController.Post(userDetail);
            var userController = new UserController();
            var newUserDetail = userController.Get(userDetail.Name);
            var userId = newUserDetail.UserId;

            var personalController = new PersonalController();
            personalDetail.UserId = userId;
            personalController.Post(personalDetail);
          
            var companyDetail = new CompanyDetail
            {
                Name = NameoftheFirm.Text,
                FirmType = FirmType.Text,
                TelephoneNumber = TelephoneNumber.Text,
                RegistrationNumber = RegistrationNumber.Text,
                UserId = userId
            };
            var companyController = new CompanyController();
            companyController.Post(companyDetail);

            var bankDetail = new BankDetail
            {
                Name = NameoftheBank.Text,
                Address = BankAddress.Text,
                MICRCode = MICRCode.Text,
                IFSCCode = IFSCCode.Text,
                UserId = userId
            };
            var bankController = new BankController();
            bankController.Post(bankDetail);

            var documentDetail = new DocumentDetail
            {
                AadharCardProof = AadhrarCardProof.Checked.ToString(),
                CompanyAddressProof = CompanyAddressProof.Checked.ToString(),
                CompanyRegistration = CompanyRegistrationProof.Checked.ToString(),
                MobileBill = MobileBill.Checked.ToString(),
                OfficeRentalDocument = OfficeRentalDocument.Checked.ToString(),
                PanCardProof = PanCardProof.Checked.ToString(),
                PartnerAddressProof = PartnerAddressProof.Checked.ToString(),
                UserId = userId
            };
            var documentController = new DocumentController();
            documentController.Post(documentDetail);           

            AadhrarCardProof.Checked = CompanyAddressProof.Checked = CompanyRegistrationProof.Checked =
                MobileBill.Checked = OfficeRentalDocument.Checked = PanCardProof.Checked = PartnerAddressProof.Checked = false;
            EmployeeType.SelectedIndex =State.SelectedIndex= 0;
            UserName.Text = Password.Text = Name.Text = Address.Text = City.Text = District.Text = Country.Text =
                Age.Text = EmailId.Text = MobileNumber.Text = PanCard.Text = AadharCardNumber.Text = NameoftheFirm.Text =
                FirmType.Text = TelephoneNumber.Text = RegistrationNumber.Text = NameoftheBank.Text = BankAddress.Text = MICRCode.Text = IFSCCode.Text = string.Empty;
            DocumentDetailsPanel.Visible = false;
            RegisterPanel.Visible = true;
            SuccessMessage = "Updated user details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
        }

        protected void OnPersonal(object sender, EventArgs e)
        {
            PersonalDetailsPanel.Visible = false;
            CompanyDetailsPanel.Visible = true;
        }
    }
}