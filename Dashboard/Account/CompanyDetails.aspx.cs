﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dashboard.Models;
using Dashboard.Controllers;

namespace Dashboard.Account
{
    public partial class CompanyDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture)))
                {
                    Response.Redirect("~/");
                }
            }
        }

        protected void OnCompanyDetailsBack(object sender, EventArgs e)
        {
            var companyDetail = new CompanyDetail
            {
                Name = NameoftheFirm.Text,
                FirmType = FirmType.Text,
                TelephoneNumber = TelephoneNumber.Text,
                RegistrationNumber = RegistrationNumber.Text
            };
            Session.Add("CompanyDetail", companyDetail);
            Response.Redirect("~/Account/BankDetails");
        }

        protected void OnCompanyDetailsNext(object sender, EventArgs e)
        {
            var companyDetail = new CompanyDetail
            {
                Name = NameoftheFirm.Text,
                FirmType = FirmType.Text,
                TelephoneNumber = TelephoneNumber.Text,
                RegistrationNumber = RegistrationNumber.Text
            };
            Session.Add("CompanyDetail", companyDetail);
            Response.Redirect("~/Account/BankDetails");
        }

        protected void OnAddClick(object sender, EventArgs e)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected string FailureMessage
        {
            get;
            private set;
        }

        protected void MessagePanelClick(object sender, EventArgs e)
        {
            SuccessMessage = FailureMessage = string.Empty;
            successMessage.Visible = failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        }

        protected void ApprovalComments(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(PartnerName.Text))
            {
                FailureMessage = "Please enter partner name";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            if (string.IsNullOrEmpty(PartnerAge.Text))
            {
                FailureMessage = "Please enter partner age";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            if (string.IsNullOrEmpty(PartnerAddress.Text))
            {
                FailureMessage = "Please enter partner address";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;

            var partnerController = new PartnerController();
            var partnerDetail = new PartnerDetail();
            partnerDetail.Age = PartnerAge.Text;
            partnerDetail.Name = PartnerName.Text;
            partnerDetail.Address = PartnerAddress.Text;
            partnerDetail.UserId = userDetail.UserId;
            partnerController.Post(partnerDetail);
            SuccessMessage = "Updated partner details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            PartnerAddress.Text = PartnerAge.Text = PartnerName.Text = string.Empty;
        }
    }
}