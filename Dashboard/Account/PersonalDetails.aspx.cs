﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dashboard.Models;

namespace Dashboard.Account
{
    public partial class PersonalDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetEmployeeTypes();

                GetIndianStates();
            }
        }

        private void GetIndianStates()
        {
            State.Items.Add("--Select State--");
            State.Items.Add("Andhra Pradesh");
            State.Items.Add("Arunachal Pradesh");
            State.Items.Add("Assam");
            State.Items.Add("Bihar");
            State.Items.Add("Chhattisgarh");
            State.Items.Add("Goa");
            State.Items.Add("Gujarat");
            State.Items.Add("Haryana");
            State.Items.Add("Himachal Pradesh");
            State.Items.Add("Jammu and Kashmir");
            State.Items.Add("Jharkhand");
            State.Items.Add("Karnataka");
            State.Items.Add("Kerala");
            State.Items.Add("Madhya Pradesh");
            State.Items.Add("Maharashtra");
            State.Items.Add("Manipur");
            State.Items.Add("Meghalaya");
            State.Items.Add("Mizoram");
            State.Items.Add("Nagaland");
            State.Items.Add("Odisha");
            State.Items.Add("Punjab");
            State.Items.Add("Rajasthan");
            State.Items.Add("Sikkim");
            State.Items.Add("Tamil Nadu");
            State.Items.Add("Telangana");
            State.Items.Add("Tripura");
            State.Items.Add("Uttar Pradesh");
            State.Items.Add("Uttarakhand");
            State.Items.Add("West Bengal");
        }

        private void GetEmployeeTypes()
        {
            EmployeeType.Items.Add("--Select Employee Type--");
            EmployeeType.Items.Add(Global.EmployeeType.BusinessAssociate.ToString());
            EmployeeType.Items.Add(Global.EmployeeType.DistrictHead.ToString());
            EmployeeType.Items.Add(Global.EmployeeType.StatetHead.ToString());
        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var personalDetail = new PersonalDetail
            {
                Name = Name.Text,
                Address = Address.Text,
                City = City.Text,
                District = District.Text,
                State = State.Text,
                Country = Country.Text,
                Age = Convert.ToInt32(Age.Text),
                EmailId = EmailId.Text,
                MobileNumber = MobileNumber.Text,
                PanCard = PanCard.Text,
                AadharCardNumber = AadharCardNumber.Text,
                EmployeeType = EmployeeType.Text
            };
            Session.Add("PersonalDetail", personalDetail);
            Response.Redirect("~/Account/CompanyDetails");
        }
    }
}