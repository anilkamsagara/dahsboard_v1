﻿using System.Globalization;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Dashboard.Models;

namespace Dashboard.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterHyperLink.NavigateUrl = "Register";
            ForgotpasswordHyperlink.NavigateUrl = "ForgotPassword";
            //OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            //if (!String.IsNullOrEmpty(returnUrl))
            //{
            //    RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            //}
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (!IsValid) return;
            var userRepository=new UserRepository();
            var userDetail = userRepository.Get(UserName.Text.ToUpper(CultureInfo.InvariantCulture));
            if (userDetail != null && userDetail.Password == Password.Text)
            {
                if (userDetail.Status == Global.UserStatus.Approved.ToString())
                {
                    IdentityHelper.SignIn(UserName.Text, RememberMe.Checked);
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }
                else
                {
                    FailureText.Text = "Not yet approved.";
                    ErrorMessage.Visible = true;
                }
            }
            else
            {
                FailureText.Text = "Invalid username or password.";
                ErrorMessage.Visible = true;
            }
        }
    }
}