﻿<%@ Page Language="C#" Title="Register" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="BankDetails.aspx.cs" Inherits="Dashboard.Account.BankDetails" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:Panel runat="server" ID="BankDetailsPanel" Visible="False">
        <div class="form-horizontal">
            <h4>Bank Details.</h4>
            <hr />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="NameoftheBank" CssClass="col-md-2 control-label">Name of the Bank</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="NameoftheBank" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="NameoftheBank" ValidationGroup="BankGroup"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Name of the Bank field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="BankAddress" CssClass="col-md-2 control-label">Bank Address</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="BankAddress" CssClass="form-control" />
                </div>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="BankAddress" ValidationGroup="BankGroup"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The Bank Address field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="MICRCode" CssClass="col-md-2 control-label">MICR Code</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="MICRCode" CssClass="form-control" />
                </div>
                <asp:RegularExpressionValidator ValidationGroup="BankGroup" ControlToValidate="MICRCode" CssClass="text-danger" runat="server" ErrorMessage="Enter valid MICR code" ValidationExpression="\d+" />
                <asp:RequiredFieldValidator ValidationGroup="BankGroup" runat="server" ControlToValidate="MICRCode"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The MICR Code field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="IFSCCode" CssClass="col-md-2 control-label">IFSC Code</asp:Label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="IFSCCode" CssClass="form-control" />
                </div>
                <asp:RegularExpressionValidator ValidationGroup="BankGroup" runat="server" ControlToValidate="IFSCCode"
                    CssClass="text-danger" Display="Dynamic" ValidationExpression="^[^\s]{4}\d{7}$" ErrorMessage="Enter valid IFSC code" />
                <asp:RequiredFieldValidator ValidationGroup="BankGroup" runat="server" ControlToValidate="IFSCCode"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The IFSC Code Number is required." />
            </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="CreateUser_Click" ValidationGroup="BankGroup" Text="Next" CssClass="btn btn-default" />
            </div>
        </div>
        </div>
    </asp:Panel>
</asp:Content>
