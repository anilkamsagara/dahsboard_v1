﻿using System.Globalization;
using Dashboard.Controllers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using Dashboard.Models;

namespace Dashboard.Account
{
    public partial class Manage : System.Web.UI.Page
    {
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected bool CanRemoveExternalLogins
        {
            get;
            private set;
        }

        private bool HasPassword()
        {
            return User.Identity.IsAuthenticated && User.Identity.AuthenticationType == DefaultAuthenticationTypes.ApplicationCookie && !string.IsNullOrEmpty(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            //var user = manager.FindById(User.Identity.GetUserId());
            //return (user != null && user.PasswordHash != null);
        }

        protected void Page_Load()
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(User.Identity.Name))
                {
                    Response.Redirect("~/");
                }
                // Determine the sections to render
                //UserManager manager = new UserManager();
                if (HasPassword())
                {
                    changePasswordHolder.Visible = true;
                }
                else
                {
                    setPassword.Visible = true;
                    changePasswordHolder.Visible = false;
                }
                //CanRemoveExternalLogins = manager.GetLogins(User.Identity.GetUserId()).Count() > 1;

                // Render success message
                var message = Request.QueryString["m"];
                if (message != null)
                {
                    // Strip the query string from action
                    Form.Action = ResolveUrl("~/Account/Manage");

                    SuccessMessage =
                        message == "ChangePwdSuccess" ? "Your password has been changed."
                        : message == "SetPwdSuccess" ? "Your password has been set."
                        : message == "RemoveLoginSuccess" ? "The account was removed."
                        : String.Empty;
                    successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
                }
            }
        }

        protected void ChangePassword_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                var userController = new UserController();
                var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
                if (CurrentPassword.Text == userDetail.Password)
                {
                    userDetail.Password = NewPassword.Text;
                    userController.Put(userDetail);

                    // Render success message
                    var message = Request.QueryString["m"];
                    if (message != null)
                    {
                        // Strip the query string from action
                        Form.Action = ResolveUrl("~/Account/BA");

                        SuccessMessage =
                            message == "ChangePwdSuccess" ? "Your password has been changed."
                            : message == "SetPwdSuccess" ? "Your password has been set."
                            : message == "RemoveLoginSuccess" ? "The account was removed."
                            : String.Empty;
                        successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
                    }
                }
                else
                {
                    AddErrors("Current password is wrong");
                }
            }

            //if (IsValid)
            //{
            //    var userDetail = new UserDetail { Name = User.Identity.Name, Password = NewPassword.Text };
            //    var userController = new UserController();
            //    userController.Post(userDetail);
            //    //UserManager manager = new UserManager();
            //    //IdentityResult result = manager.ChangePassword(User.Identity.GetUserId(), CurrentPassword.Text, NewPassword.Text);
            //    //if (result.Succeeded)
            //    {
            //        Response.Redirect("~/Account/Manage?m=ChangePwdSuccess");
            //    }
            //    //else
            //    //{
            //    //    AddErrors(result);
            //    //}
            //}
        }

        protected void SetPassword_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Create the local login info and link the local account to the user
                UserManager manager = new UserManager();
                IdentityResult result = manager.AddPassword(User.Identity.GetUserId(), password.Text);
                if (result.Succeeded)
                {
                    Response.Redirect("~/Account/Manage?m=SetPwdSuccess");
                }
                else
                {
                    //AddErrors(result);
                }
            }
        }

        public IEnumerable<UserLoginInfo> GetLogins()
        {
            //UserManager manager = new UserManager();
            //var accounts = manager.GetLogins(User.Identity.GetUserId());
            //CanRemoveExternalLogins = accounts.Count() > 1 || HasPassword();
            return null;
        }

        public void RemoveLogin(string loginProvider, string providerKey)
        {
            UserManager manager = new UserManager();
            var result = manager.RemoveLogin(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            var msg = result.Succeeded
                ? "?m=RemoveLoginSuccess"
                : String.Empty;
            Response.Redirect("~/Account/Manage" + msg);
        }

        private void AddErrors(string error)
        {
            ModelState.AddModelError("", error);
        }
    }
}