﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DH.aspx.cs" Inherits="Dashboard.Account.DH" EnableEventValidation="false" %>

<%@ Register TagPrefix="cc1" Namespace="DatePickerControl" Assembly="DatePickerControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h6></h6>
    <h3><%: Title %></h3>
    <asp:UpdatePanel runat="server" ID="MessagePanel">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-success"><%: SuccessMessage %></p>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-danger"><%: FailureMessage %></p>
            </asp:PlaceHolder>
            <asp:Timer runat="server" Interval="1000" OnTick="MessagePanelClick"></asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="row" id="Tabs">
        <!-- Navigation Buttons -->
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" id="myTabs">
                <li class="active"><a href="#Notification">Notification</a></li>
                <li><a href="#ViewBA">View Team</a></li>
                <%--<li><a href="#ViewSP">View Service Partners</a></li>--%>
                <%--<li><a href="#Commission">View Commission Report</a></li>--%>
                <li><a href="#ViewRemainderList">View Remainder List</a></li>
                <li><a href="#Viewcompliantreport">View Complaint Report</a></li>
                <li><a href="#Commission1">View Team Commission Report</a></li>
            </ul>
        </div>

        <!-- Content -->
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="Notification">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <br />
                        <h4>Pending notifications</h4>
                        <br />
                        <asp:Panel ID="pendingNotificationPanel" runat="server" Visible="True">
                            <asp:Label runat="server" Text="No pending notification" CssClass="label-info"></asp:Label>
                        </asp:Panel>
                        <asp:EntityDataSource ID="TicketEntityDataSource" OnQueryCreated="TicketEntityDataSource_OnQueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails"></asp:EntityDataSource>
                        <asp:GridView runat="server" ID="TicketGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" ValidationGroup="Viewticketnotification" OnSelectedIndexChanged="OnTicketNotificationGridChanged" AutoGenerateColumns="False" DataSourceID="TicketEntityDataSource" DataKeyNames="TicketId">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ValidationGroup="ApproveVehicleGroup"></asp:CommandField>
                                <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="True" SortExpression="TicketId"></asp:BoundField>
                                <asp:BoundField DataField="Registration" HeaderText="Vehicle Number" SortExpression="Registration"></asp:BoundField>
                                <asp:BoundField DataField="BAPermission" HeaderText="BA Approval" SortExpression="BAPermission"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:LinkButton ID="PrintLinkButton" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument="TicketGridView" OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="ExportLinkButton" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument="TicketGridView" OnCommand="ExportCurrentpage"></asp:LinkButton>
                        <br />
                        <h4>Rejected notifications</h4>
                        <br />
                        <asp:Panel ID="rejectedNotificationPanel" runat="server" Visible="True">
                            <asp:Label runat="server" Text="No rejected notification" CssClass="label-info"></asp:Label>
                        </asp:Panel>
                        <asp:EntityDataSource ID="RejectedEntityDataSource" OnQueryCreated="TicketEntityDataSource_OnRejectedQueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="TicketDetails"></asp:EntityDataSource>
                        <asp:GridView runat="server" ID="RejectedGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" ValidationGroup="Viewticketnotification" OnSelectedIndexChanged="OnTicketRejected" AutoGenerateColumns="False" DataSourceID="RejectedEntityDataSource" DataKeyNames="TicketId">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ValidationGroup="ApproveVehicleGroup"></asp:CommandField>
                                <asp:BoundField DataField="TicketId" HeaderText="TicketId" ReadOnly="True" SortExpression="TicketId"></asp:BoundField>
                                <asp:BoundField DataField="Registration" HeaderText="Vehicle Number" SortExpression="Registration"></asp:BoundField>
                                <asp:BoundField DataField="BAPermission" HeaderText="BA Approval" SortExpression="BAPermission"></asp:BoundField>
                                <asp:BoundField DataField="DHPermission" HeaderText="DH Approval" SortExpression="DHPermission"></asp:BoundField>
                                <asp:BoundField DataField="SHPermission" HeaderText="SH Approval" SortExpression="SHPermission"></asp:BoundField>
                            </Columns>

                        </asp:GridView>
                        <asp:LinkButton ID="LinkButton1" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='RejectedGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" Visible="true" runat="server" ValidationGroup="Viewticketnotification" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='RejectedGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>
                    </div>
                </div>
                <%--<div class="tab-pane" id="ViewSP">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Service Partners</h4>
                        <br />
                        <asp:EntityDataSource ID="EntityDataSource3" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="CompanyDetails"></asp:EntityDataSource>
                        <asp:GridView ID="GridView3" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" runat="server" AutoGenerateColumns="False" DataKeyNames="CompanyId" DataSourceID="EntityDataSource3" AllowSorting="True" AllowPaging="True">
                            <Columns>
                                <asp:BoundField DataField="CompanyId" HeaderText="CompanyId" ReadOnly="True" SortExpression="CompanyId"></asp:BoundField>
                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                                <asp:BoundField DataField="FirmType" HeaderText="FirmType" SortExpression="FirmType"></asp:BoundField>
                                <asp:BoundField DataField="TelephoneNumber" HeaderText="TelephoneNumber" SortExpression="TelephoneNumber"></asp:BoundField>
                                <asp:BoundField DataField="RegistrationNumber" HeaderText="RegistrationNumber" SortExpression="RegistrationNumber"></asp:BoundField>
                                <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:LinkButton ID="LinkButton5" Visible="true" runat="server" ValidationGroup="ViewSP" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='GridView3' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton8" Visible="true" runat="server" ValidationGroup="ViewSP" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='GridView3' OnCommand="ExportCurrentpage"></asp:LinkButton>


                    </div>
                </div>--%>
                <%--                <div class="tab-pane" id="Commission">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Commission Report</h4>
                    </div>
                </div>--%>
                <div class="tab-pane" id="ViewBA">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>Business associates</h4>
                        <br />
                        <asp:EntityDataSource ID="ViewBAEntityDataSource" OnQueryCreated="OnViewBusinessAssociate" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="PersonalDetails">
                        </asp:EntityDataSource>
                        <asp:GridView ID="ViewBAGridView" OnPageIndexChanging="ViewBAGridView_OnPageIndexChanging" CssClass="mGrid" OnSelectedIndexChanged="ViewBAGridView_OnSelectedIndexChanged" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" runat="server" AutoGenerateColumns="False" DataSourceID="ViewBAEntityDataSource" DataKeyNames="PersonalId" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" SelectText="View Service Partner"></asp:CommandField>
                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                                <asp:BoundField DataField="City" HeaderText="City" SortExpression="City"></asp:BoundField>
                                <asp:BoundField DataField="District" HeaderText="District" SortExpression="District"></asp:BoundField>
                                <asp:BoundField DataField="State" HeaderText="State" SortExpression="State"></asp:BoundField>
                                <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:LinkButton ID="LinkButton13" Visible="true" runat="server" ValidationGroup="ViewBA" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='ViewBAGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton14" Visible="true" runat="server" ValidationGroup="ViewBA" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='ViewBAGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>
                    </div>

                    <div class="form-horizontal">
                        <h4>View service partners</h4>
                        <br />
                        <asp:EntityDataSource ID="ViewSPEntityDataSource" OnQueryCreated="ViewSPEntityDataSource_OnQueryCreated" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="CompanyDetails">
                        </asp:EntityDataSource>
                        <asp:GridView ID="ViewSpGridView" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" runat="server" AutoGenerateColumns="False" DataSourceID="ViewSPEntityDataSource" DataKeyNames="CompanyId" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                                <asp:BoundField DataField="FirmType" HeaderText="Firm Type" SortExpression="FirmType"></asp:BoundField>
                                <asp:BoundField DataField="TelephoneNumber" HeaderText="Telephone Number" SortExpression="TelephoneNumber"></asp:BoundField>
                                <asp:BoundField DataField="RegistrationNumber" HeaderText="Registration Number" SortExpression="RegistrationNumber"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:LinkButton ID="LinkButton3" Visible="true" runat="server" ValidationGroup="ViewBA" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='ViewSpGridView' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton4" Visible="true" runat="server" ValidationGroup="ViewBA" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='ViewSpGridView' OnCommand="ExportCurrentpage"></asp:LinkButton>
                    </div>
                </div>
                <div class="tab-pane" id="ViewRemainderList">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Remainder List</h4>
                        <br />
                        <asp:EntityDataSource ID="EntityDataSource5" runat="server" ConnectionString="name=DashboardManagementEntities" DefaultContainerName="DashboardManagementEntities1" EnableFlattening="False" EntitySetName="AccountDetails" EnableDelete="True" EnableInsert="True" EnableUpdate="True"></asp:EntityDataSource>
                        <asp:GridView ID="GridView5"
                            runat="server" DataSourceID="EntityDataSource5"
                            AllowPaging="True"
                            CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                            ValidationGroup="ViewRemainderList"
                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="AccountId">
                            <Columns>
                                <asp:BoundField DataField="AccountId" HeaderText="AccountId" ReadOnly="True" SortExpression="AccountId"></asp:BoundField>
                                <asp:BoundField DataField="Funds" HeaderText="Balance" SortExpression="Funds"></asp:BoundField>
                                <asp:BoundField DataField="Bank" HeaderText="Bank" SortExpression="Bank"></asp:BoundField>
                                <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId"></asp:BoundField>
                            </Columns>

                        </asp:GridView>

                        <asp:LinkButton ID="LinkButton6" Visible="true" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='GridView5' OnCommand="PrintCurrentPage"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton7" Visible="true" runat="server" ValidationGroup="ViewRemainderList" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='GridView5' OnCommand="ExportCurrentpage"></asp:LinkButton>

                    </div>
                </div>
                <div class="tab-pane" id="Viewcompliantreport">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Compliant Report</h4>
                    </div>
                </div>

                <div class="tab-pane" id="Commission1">
                    <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                    <div class="form-horizontal">
                        <h4>View Team Commission Report</h4>
                        <br />
                        <div class="form-group">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="totalCommission">Start Date  </asp:Label>
                                <cc1:DatePicker DateFormat="dd/MM/yyyy" ID="DatePickerstart" Width="150px" runat="server" />
                                <asp:Label runat="server" AssociatedControlID="totalCommission">End Date  </asp:Label>
                                <cc1:DatePicker DateFormat="dd/MM/yyyy" ID="DatePickerEnd" Width="150px" runat="server" />
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-10">
                                    <asp:Button runat="server" ID="commissionButton" Text="Submit" OnClick="commissionButton_OnClick" CssClass="btn btn-default" />
                                </div>
                            </div>
                            <br />
                            <asp:GridView ID="ComissionGrid" runat="server" PagerStyle-CssClass="pgr" CssClass="mGrid" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="BAName" HeaderText="BA Name" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="BACity" HeaderText="BA City" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="BAMobileNumber" HeaderText="BA Mobile Number" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" ReadOnly="True"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:LinkButton ID="commissionPrintLinkButton" Visible="False" runat="server" ToolTip="Click to Print current page" Text="Print Current Page" CommandArgument='ComissionGrid' OnCommand="PrintCurrentPage"></asp:LinkButton>
                            <asp:LinkButton ID="commissionExportLinkButton" Visible="False" runat="server" ToolTip="Click to Export to Excel" Text="Export Page to Excel" CommandArgument='ComissionGrid' OnCommand="ExportCurrentpage"></asp:LinkButton>
                        </div>
                        <br />
                        <asp:Panel runat="server" Visible="False" ID="totalCommissionPanel">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="totalCommission" CssClass="col-md-3 control-label">Total Commission: </asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox ReadOnly="True" runat="server" ID="totalCommission" CssClass="form-control" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="selected_tab" runat="server" />
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="lblModalTitle" runat="server" Text="Comment"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:TextBox TextMode="MultiLine" Width="100%" ID="Comments" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                                ControlToValidate="Comments" ValidationGroup="CommentsValidationGroup" ErrorMessage="Please enter comments"></asp:RequiredFieldValidator>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="IdButton" class="btn btn-info" ValidationGroup="CommentsValidationGroup" runat="server" UseSubmitBehavior="false" OnClick="ApprovalComments" Text="Ok" data-dismiss="modal" aria-hidden="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="ticketModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updateTicketModel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label ID="TicketDetails" runat="server" Text="Ticket Details"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="TicketId" CssClass="col-md-3 control-label">Ticket Id</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="TicketId" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="GoodsType" CssClass="col-md-3 control-label">Goods Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="GoodsType" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Tonnage" CssClass="col-md-3 control-label">Tonnage Range</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="Tonnage" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="MakeOftheVehicle" CssClass="col-md-3 control-label">Make of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="MakeOftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ModelOftheVehicle" CssClass="col-md-3 control-label">Model of the vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="ModelOftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="CubicCapacity" CssClass="col-md-3 control-label">Cubic Capacity</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="CubicCapacity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="LengthoftheVehicle" CssClass="col-md-3 control-label">Length of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="LengthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="WidthoftheVehicle" CssClass="col-md-3 control-label">Width of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="WidthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="BreadthoftheVehicle" CssClass="col-md-3 control-label">Breadth of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="BreadthoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="HeightoftheVehicle" CssClass="col-md-3 control-label">Height of the Vehicle</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="HeightoftheVehicle" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="RegistrationNumber" CssClass="col-md-3 control-label">Registration Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="RegistrationNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="EngineNumber" CssClass="col-md-3 control-label">Engine Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="EngineNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ChasisNumber" CssClass="col-md-3 control-label">Chassis Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="ChasisNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NOCRTO" CssClass="col-md-3 control-label">NOC from RTO</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="NOCRTO" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="NOCPolice" CssClass="col-md-3 control-label">NOC from Police</asp:Label>
                                <div class="col-md-5">
                                    <asp:CheckBox runat="server" ID="NOCPolice" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoFront" CssClass="col-md-3 control-label">Vehicle Photo Front</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="VehiclePhotoFront" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoBack" CssClass="col-md-3 control-label">Vehicle Photo Back</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="VehiclePhotoBack" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoLeft" CssClass="col-md-3 control-label">Vehicle Photo Left</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="VehiclePhotoLeft" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehiclePhotoRight" CssClass="col-md-3 control-label">Vehicle Photo Right</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="VehiclePhotoRight" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FCValidityFrom" CssClass="col-md-3 control-label">FC Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="FCValidityFrom" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="FCValidity" CssClass="col-md-3 control-label">FC Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="FCValidity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="VehicleOwnerName" CssClass="col-md-3 control-label">Vehicle Owner Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="VehicleOwnerName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="Address" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="City" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="District" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="District" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="State" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="State" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="Country" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="Country" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="InsuranceType" CssClass="col-md-3 control-label">Insurance Type</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="InsuranceType" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="InsuranceCompany" CssClass="col-md-3 control-label">Insurer Company</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="InsuranceCompany" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ValidityFrom" CssClass="col-md-3 control-label">Validity From</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="ValidityFrom" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="ValidityTo" CssClass="col-md-3 control-label">Validity To</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="ValidityTo" CssClass="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverName" CssClass="col-md-3 control-label">Name</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverName" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverAge" CssClass="col-md-3 control-label">Age</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverAge" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverAddress" CssClass="col-md-3 control-label">Address</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverAddress" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverCity" CssClass="col-md-3 control-label">City</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverCity" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverDistrict" CssClass="col-md-3 control-label">District</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverDistrict" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverState" CssClass="col-md-3 control-label">State</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverState" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverCountry" CssClass="col-md-3 control-label">Country</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverCountry" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverDLNumber" CssClass="col-md-3 control-label">DL Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverDLNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverPhoto" CssClass="col-md-3 control-label">Vehicle Photo Right</asp:Label>
                                <asp:Image Width="400px" Height="400px" runat="server" ID="DriverPhoto" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverMobileNumber" CssClass="col-md-3 control-label">Mobile Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverMobileNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverEmailId" CssClass="col-md-3 control-label">Email</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverEmailId" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverPanNumber" CssClass="col-md-3 control-label">Pan card number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverPanNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="DriverAadharCardNumber" CssClass="col-md-3 control-label">Adhar Card Number</asp:Label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ReadOnly="True" ID="DriverAadharCardNumber" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="TicketApprove" class="btn btn-info" runat="server" UseSubmitBehavior="false" OnClick="OnApproveClick" Text="Approve" data-dismiss="modal" aria-hidden="true" />
                            <asp:Button ID="TicketReject" class="btn btn-info" runat="server" UseSubmitBehavior="false" OnClick="OnRejectClick" Text="Reject" data-dismiss="modal" aria-hidden="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- Enable the tabs -->
    <script type="text/javascript">
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
        $(function () {
            var tabName = $("[id*=selected_tab]").val() != "" ? $("[id*=selected_tab]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=selected_tab]").val($(this).attr("href").replace("#", ""));
            });
        });

    </script>

    <%--<script type="text/javascript">
        function UpdateInfo() {
            var daysTotal = deEnd.GetRangeDayCount();
            tbInfo.SetText(daysTotal !== -1 ? daysTotal + ' days' : '');
        }
    </script>--%>
</asp:Content>
