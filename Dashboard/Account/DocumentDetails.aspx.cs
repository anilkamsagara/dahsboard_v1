﻿using System;
using Dashboard.Controllers;
using Dashboard.Models;

namespace Dashboard.Account
{
    public partial class DocumentDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            Comments.Text = string.Empty;
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);            
        }
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected string FailureMessage
        {
            get;
            private set;
        }
        protected void MessagePanelClick(object sender, EventArgs e)
        {
            SuccessMessage = FailureMessage = string.Empty;
            successMessage.Visible = failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
            if (null != Session["Update"] && bool.Parse(Session["Update"].ToString()))
            {
                Session["Update"] = false;
                Response.Redirect("~/");
            }
        }

        protected void ApprovalComments(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Comments.Text))
            {
                FailureMessage = "Please enter comments";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            var documentDetail = new DocumentDetail
            {
                AadharCardProof = AadhrarCardProof.Checked.ToString(),
                CompanyAddressProof = CompanyAddressProof.Checked.ToString(),
                CompanyRegistration = CompanyRegistrationProof.Checked.ToString(),
                MobileBill = MobileBill.Checked.ToString(),
                OfficeRentalDocument = OfficeRentalDocument.Checked.ToString(),
                PanCardProof = PanCardProof.Checked.ToString(),
                PartnerAddressProof = PartnerAddressProof.Checked.ToString()
            };
            var userDetail = Session["UserDetail"] as UserDetail;
            var personalDetail = Session["PersonalDetail"] as PersonalDetail;

            if (userDetail == null) return;
            var usrController = new UserController();
            if (personalDetail != null)
                userDetail.Role = personalDetail.EmployeeType;
            usrController.Post(userDetail);
            var userController = new UserController();
            var newUserDetail = userController.Get(userDetail.Name);
            var userId = newUserDetail.UserId;
            if (personalDetail != null)
            {
                var personalController = new PersonalController();
                personalDetail.UserId = userId;
                personalController.Post(personalDetail);
            }
            var companyDetail = Session["CompanyDetail"] as CompanyDetail;
            if (companyDetail != null)
            {
                var companyController = new CompanyController();
                companyDetail.UserId = userId;
                companyController.Post(companyDetail);
            }
            var bankDetail = Session["BankDetail"] as BankDetail;
            if (bankDetail != null)
            {
                var bankController = new BankController();
                bankDetail.UserId = userId;
                bankController.Post(bankDetail);
            }
            documentDetail.UserId = userId;
            var documentController = new DocumentController();
            documentController.Post(documentDetail);
            SuccessMessage = "Updated user details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            Session.Clear();
            Session.Add("Update", true);
        }
    }

}