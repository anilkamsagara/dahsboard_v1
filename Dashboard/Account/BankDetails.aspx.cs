﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dashboard.Models;

namespace Dashboard.Account
{
    public partial class BankDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var bankDetail=new BankDetail
            {
                Name = NameoftheBank.Text,
                Address = BankAddress.Text,
                MICRCode = MICRCode.Text,
                IFSCCode = IFSCCode.Text
            };
            Session.Add("BankDetail",bankDetail);
            Response.Redirect("~/Account/DocumentDetails");
        }
    }
}