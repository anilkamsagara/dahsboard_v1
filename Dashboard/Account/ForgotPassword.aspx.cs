﻿using Dashboard.Controllers;
using Dashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dashboard.Account
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected string FailureMessage
        {
            get;
            private set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPass_Click(object sender, EventArgs e)
        {
            try
            {
                var userdetail = new UserController();
                var userid = txtEmail.Text;
                var userstatus = userdetail.Get(userid);

                var personaldetails = new PersonalController();

                var persondetails1 = personaldetails.GetByUserId(userstatus.UserId);
                var emailid = persondetails1.EmailId;
                var telno = persondetails1.MobileNumber;

                if (emailid != null && userstatus.Status == "Approved")
                {
                    SuccessMessage = "Password sent to Registered Email and Mobile number ";
                    successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
                }
                else
                {
                    FailureMessage = "Invalid username or User is not yet approved. ";
                    failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);

                }
            }

            catch (System.NullReferenceException)
            {
                FailureMessage = "Invalid username or User is not yet approved. ";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
            }



        }
        protected void MessagePanelClick(object sender, EventArgs e)
        {
            SuccessMessage = FailureMessage = string.Empty;
            successMessage.Visible = failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);

        }

    }
}