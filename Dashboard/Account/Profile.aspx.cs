﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Dashboard.Controllers;

namespace Dashboard.Account
{
    public partial class Profile : System.Web.UI.Page
    {
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected string FailureMessage
        {
            get;
            private set;
        }
        private void LoadBankDetails()
        {
            NameoftheBank.Items.Clear();
            NameoftheBank.Items.Add("-- Select Bank --");
            var banksController = new BanksInIndiaController();
            var bankDetails = banksController.Get();
            if (null == bankDetails) return;
            foreach (var banksInIndia in bankDetails)
            {
                NameoftheBank.Items.Add(banksInIndia.BankName);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetIndianStates(State);
                if (!string.IsNullOrEmpty(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture)))
                {
                    LoadBankDetails();
                    UpdatePersonalDetails();
                    UpdateCompanyDetails();
                    UpdateBankDetails();
                    UpdateDocumentDetails();
                }
                else
                {
                    Response.Redirect("~/");
                    return;
                }
            }
            successMessage.Visible = false;
            failureMessage.Visible = false;
        }

        private void UpdateCompanyDetails()
        {
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;
            var companyController = new CompanyController();
            var companyDetail = companyController.GetByUserId(userDetail.UserId);
            if (null == companyDetail) return;
            NameoftheFirm.Text = companyDetail.Name;
            FirmType.Text = companyDetail.FirmType;
            TelephoneNumber.Text = companyDetail.TelephoneNumber;
            RegistrationNumber.Text = companyDetail.RegistrationNumber;
        }

        private void UpdateDocumentDetails()
        {
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;
            var documentController = new DocumentController();
            var documentDetail = documentController.GetByUserId(userDetail.UserId);
            if (null == documentDetail) return;
            PanCardProof.Checked = bool.Parse(documentDetail.PanCardProof);
            AadhrarCardProof.Checked = bool.Parse(documentDetail.AadharCardProof);
            CompanyRegistrationProof.Checked = bool.Parse(documentDetail.CompanyRegistration);
            CompanyAddressProof.Checked = bool.Parse(documentDetail.CompanyAddressProof);
            MobileBill.Checked = bool.Parse(documentDetail.MobileBill);
            OfficeRentalDocument.Checked = bool.Parse(documentDetail.OfficeRentalDocument);
            PartnerAddressProof.Checked = bool.Parse(documentDetail.PartnerAddressProof);
        }

        private void UpdateBankDetails()
        {
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;
            var bankController = new BankController();
            var bankDetail = bankController.GetByUserId(userDetail.UserId);
            if (null == bankDetail) return;
            AccountNumber.Text = bankDetail.AccountNumber;
            NameoftheBank.Text = bankDetail.Name;
            BankAddress.Text = bankDetail.Address;
            MICRCode.Text = bankDetail.MICRCode;
            IFSCCode.Text = bankDetail.IFSCCode;
        }

        private void UpdatePersonalDetails()
        {
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;
            var personalController = new PersonalController();
            var personalDetail = personalController.GetByUserId(userDetail.UserId);
            if (null == personalDetail) return;
            Name.Text = personalDetail.Name;
            Address.Text = personalDetail.Address;
            City.Text = personalDetail.City;
            District.Text = personalDetail.District;
            State.Text = personalDetail.State;
            Country.Text = personalDetail.Country;
            AadharCardNumber.Text = personalDetail.AadharCardNumber;
            Age.Text = personalDetail.Age.ToString();
            EmailId.Text = personalDetail.EmailId;
            MobileNumber.Text = personalDetail.MobileNumber;
            PanCard.Text = personalDetail.PanCard;
        }

        protected void OnUpdatePersonalDetails(object sender, EventArgs e)
        {
            if (!IsValid) return;
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;
            var personalController = new PersonalController();
            var personalDetail = personalController.GetByUserId(userDetail.UserId);
            if (null == personalDetail) return;
            personalDetail.Name = Name.Text;
            personalDetail.Address = Address.Text;
            personalDetail.City = City.Text;
            personalDetail.District = District.Text;
            personalDetail.State = State.Text;
            personalDetail.Country = Country.Text;
            personalDetail.AadharCardNumber = AadharCardNumber.Text;
            personalDetail.Age = int.Parse(Age.Text);
            personalDetail.EmailId = EmailId.Text;
            personalDetail.MobileNumber = MobileNumber.Text;
            personalDetail.PanCard = PanCard.Text;
            personalController.Put(personalDetail);
            SuccessMessage = "Updated successfully";
            successMessage.Visible = true;
        }

        protected void OnUpdateCompanyDetails(object sender, EventArgs e)
        {
            if (!IsValid) return;
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;
            var companyController = new CompanyController();
            var companyDetail = companyController.GetByUserId(userDetail.UserId);
            if (null == companyDetail) return;
            companyDetail.Name = NameoftheFirm.Text;
            companyDetail.FirmType = FirmType.Text;
            companyDetail.TelephoneNumber = TelephoneNumber.Text;
            companyDetail.RegistrationNumber = RegistrationNumber.Text;
            companyController.Put(companyDetail);
            SuccessMessage = "Updated successfully";
            successMessage.Visible = true;
        }
        private void GetIndianStates(DropDownList dropDownList)
        {
            dropDownList.Items.Add("--Select State--");
            dropDownList.Items.Add("Andhra Pradesh");
            dropDownList.Items.Add("Arunachal Pradesh");
            dropDownList.Items.Add("Assam");
            dropDownList.Items.Add("Bihar");
            dropDownList.Items.Add("Chhattisgarh");
            dropDownList.Items.Add("Goa");
            dropDownList.Items.Add("Gujarat");
            dropDownList.Items.Add("Haryana");
            dropDownList.Items.Add("Himachal Pradesh");
            dropDownList.Items.Add("Jammu and Kashmir");
            dropDownList.Items.Add("Jharkhand");
            dropDownList.Items.Add("Karnataka");
            dropDownList.Items.Add("Kerala");
            dropDownList.Items.Add("Madhya Pradesh");
            dropDownList.Items.Add("Maharashtra");
            dropDownList.Items.Add("Manipur");
            dropDownList.Items.Add("Meghalaya");
            dropDownList.Items.Add("Mizoram");
            dropDownList.Items.Add("Nagaland");
            dropDownList.Items.Add("Odisha");
            dropDownList.Items.Add("Punjab");
            dropDownList.Items.Add("Rajasthan");
            dropDownList.Items.Add("Sikkim");
            dropDownList.Items.Add("Tamil Nadu");
            dropDownList.Items.Add("Telangana");
            dropDownList.Items.Add("Tripura");
            dropDownList.Items.Add("Uttar Pradesh");
            dropDownList.Items.Add("Uttarakhand");
            dropDownList.Items.Add("West Bengal");
        }
        protected void OnUpdateBankDetails(object sender, EventArgs e)
        {
            if (!IsValid) return;
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;
            var bankController = new BankController();
            var bankDetail = bankController.GetByUserId(userDetail.UserId);
            if (null == bankDetail) return;
            bankDetail.AccountNumber = AccountNumber.Text;
            bankDetail.Name = NameoftheBank.Text;
            bankDetail.Address = BankAddress.Text;
            bankDetail.MICRCode = MICRCode.Text;
            bankDetail.IFSCCode = IFSCCode.Text;
            bankController.Put(bankDetail);
            SuccessMessage = "Updated successfully";
            successMessage.Visible = true;
        }

        protected void OnUpdateDocumentDetails(object sender, EventArgs e)
        {
            if (!IsValid) return;
            var userController = new UserController();
            var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
            if (null == userDetail) return;
            var documentController = new DocumentController();
            var documentDetail = documentController.GetByUserId(userDetail.UserId);
            if (null == documentDetail) return;
            documentDetail.PanCardProof = PanCardProof.Checked.ToString();
            documentDetail.AadharCardProof = AadhrarCardProof.Checked.ToString();
            documentDetail.CompanyRegistration = CompanyRegistrationProof.Checked.ToString();
            documentDetail.CompanyAddressProof = CompanyAddressProof.Checked.ToString();
            documentDetail.MobileBill = MobileBill.Checked.ToString();
            documentDetail.OfficeRentalDocument = OfficeRentalDocument.Checked.ToString();
            documentDetail.PartnerAddressProof = PartnerAddressProof.Checked.ToString();
            documentController.Put(documentDetail);
            SuccessMessage = "Updated successfully";
            successMessage.Visible = true;
        }

    }
}