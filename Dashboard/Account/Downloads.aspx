﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Downloads.aspx.cs" Inherits="Dashboard.Account.Downloads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>Downloads</h2>
    <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
        <p class="text-success"><%: SuccessMessage %></p>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="failureMessage" Visible="false" ViewStateMode="Disabled">
        <p class="text-danger"><%: FailureMessage %></p>
    </asp:PlaceHolder>
    <div class="row" id="Tabs">
        <!-- Navigation Buttons -->
        <div class="col-md-3">
<%--            <ul class="nav nav-pills nav-stacked" id="myTabs">
                <li class="active"><a  href="#BAform" runat="server" onclick="BAformdownload">BA Form</a></li>
                <li><a href="#BPform" data-toggle="tab" runat="server"  OnClientClick ="BPformdownload">BP Form</a></li>
                <li><a href="#SHform" data-toggle="tab" runat="server" onclick="SHformdownload">SH Form</a></li>
                <li><a href="#DHform" data-toggle="tab" runat="server" onclick="DHformdownload">DH Form</a></li>
            </ul>--%>
            <h4>Forms section</h4>
            <asp:LinkButton ID="HypBA" runat="server" Text="Download BA Form" CommandArgument='HypBA' OnCommand="DownloadFile_Common" />
            <br />
            <br />
            <asp:LinkButton ID="HypBP" runat="server" Text="Download BP Form" CommandArgument='HypBP' OnCommand="DownloadFile_Common" />
            <br />
            <br />
            <asp:LinkButton ID="HypSH" runat="server" Text="Download SH Form" CommandArgument='HypSH' OnCommand="DownloadFile_Common" />
            <br />
            <br />
            <asp:LinkButton ID="HypDH" runat="server" Text="Download DH Form" CommandArgument='HypDH' OnCommand="DownloadFile_Common" />
            <br />
            <br />
        </div>
        <!-- Content -->
        <div class="col-md-9">
        </div>
    </div>
    <asp:HiddenField ID="selected_tab" runat="server" />
    <!-- Enable the tabs -->
    <script type="text/javascript">
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
        $(function () {
            var tabName = $("[id*=selected_tab]").val() != "" ? $("[id*=selected_tab]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=selected_tab]").val($(this).attr("href").replace("#", ""));
            });
        });
    </script>
</asp:Content>
