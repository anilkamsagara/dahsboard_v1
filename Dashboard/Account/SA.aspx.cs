﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Dashboard.Controllers;
using Dashboard.Models;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.Diagnostics;
using OpenPop.Pop3;
using OpenPop.Mime;
using System.Net.Mail;
using System.Web.UI;

namespace Dashboard.Account
{
    public class ViaNettSMS
    {
        // Declarations
        private string username;
        private string password;

        /// <summary>
        /// Constructor with username and password to ViaNett gateway. 
        /// </summary>
        public ViaNettSMS(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
        /// <summary>
        /// Send SMS message through the ViaNett HTTP API.
        /// </summary>
        /// <returns>Returns an object with the following parameters: Success, ErrorCode, ErrorMessage</returns>
        /// <param name="msgsender">Message sender address. Mobile number or small text, e.g. company name</param>
        /// <param name="destinationaddr">Message destination address. Mobile number.</param>
        /// <param name="message">Text message</param>
        public Result sendSMS(string msgsender, string destinationaddr, string message)
        {
            // Declarations
            string url;
            string serverResult;
            long l;
            Result result;

            // Build the URL request for sending SMS.
            url = "http://smsc.vianett.no/ActiveServer/MT/?"
                + "username=" + HttpUtility.UrlEncode(username)
                + "&password=" + HttpUtility.UrlEncode(password)
                + "&destinationaddr=" + HttpUtility.UrlEncode(destinationaddr, System.Text.Encoding.GetEncoding("ISO-8859-1"))
                + "&message=" + HttpUtility.UrlEncode(message, System.Text.Encoding.GetEncoding("ISO-8859-1"))
                + "&refno=1";

            // Check if the message sender is numeric or alphanumeric.
            if (long.TryParse(msgsender, out l))
            {
                url = url + "&sourceAddr=" + msgsender;
            }
            else
            {
                url = url + "&fromAlpha=" + msgsender;
            }
            // Send the SMS by submitting the URL request to the server. The response is saved as an XML string.
            serverResult = DownloadString(url);
            // Converts the XML response from the server into a more structured Result object.
            result = ParseServerResult(serverResult);
            // Return the Result object.
            return result;
        }
        /// <summary>
        /// Downloads the URL from the server, and returns the response as string.
        /// </summary>
        /// <param name="URL"></param>
        /// <returns>Returns the http/xml response as string</returns>
        /// <exception cref="WebException">WebException is thrown if there is a connection problem.</exception>
        private string DownloadString(string URL)
        {
            using (System.Net.WebClient wlc = new System.Net.WebClient())
            {
                // Create WebClient instanse.
                try
                {
                    // Download and return the xml response
                    return wlc.DownloadString(URL);
                }
                catch (WebException ex)
                {
                    // Failed to connect to server. Throw an exception with a customized text.
                    throw new WebException("Error occurred while connecting to server. " + ex.Message, ex);
                }
            }
        }

        private void GetIndianStates(DropDownList dropDownList)
        {
            dropDownList.Items.Add("--Select State--");
            dropDownList.Items.Add("Andhra Pradesh");
            dropDownList.Items.Add("Arunachal Pradesh");
            dropDownList.Items.Add("Assam");
            dropDownList.Items.Add("Bihar");
            dropDownList.Items.Add("Chhattisgarh");
            dropDownList.Items.Add("Goa");
            dropDownList.Items.Add("Gujarat");
            dropDownList.Items.Add("Haryana");
            dropDownList.Items.Add("Himachal Pradesh");
            dropDownList.Items.Add("Jammu and Kashmir");
            dropDownList.Items.Add("Jharkhand");
            dropDownList.Items.Add("Karnataka");
            dropDownList.Items.Add("Kerala");
            dropDownList.Items.Add("Madhya Pradesh");
            dropDownList.Items.Add("Maharashtra");
            dropDownList.Items.Add("Manipur");
            dropDownList.Items.Add("Meghalaya");
            dropDownList.Items.Add("Mizoram");
            dropDownList.Items.Add("Nagaland");
            dropDownList.Items.Add("Odisha");
            dropDownList.Items.Add("Punjab");
            dropDownList.Items.Add("Rajasthan");
            dropDownList.Items.Add("Sikkim");
            dropDownList.Items.Add("Tamil Nadu");
            dropDownList.Items.Add("Telangana");
            dropDownList.Items.Add("Tripura");
            dropDownList.Items.Add("Uttar Pradesh");
            dropDownList.Items.Add("Uttarakhand");
            dropDownList.Items.Add("West Bengal");
        }
        /// <summary>
        /// Parses the XML code and returns a Result object.
        /// </summary>
        /// <param name="ServerResult">XML data from a request through HTTP API.</param>
        /// <returns>Returns a Result object with the parsed data.</returns>
        private Result ParseServerResult(string ServerResult)
        {
            System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode ack;
            Result result = new Result();
            xDoc.LoadXml(ServerResult);
            ack = xDoc.GetElementsByTagName("ack")[0];
            result.ErrorCode = int.Parse(ack.Attributes["errorcode"].Value);
            result.ErrorMessage = ack.InnerText;
            result.Success = (result.ErrorCode == 0);
            return result;
        }

        /// <summary>
        /// The Result object from the SendSMS function, which returns Success(Boolean), ErrorCode(Integer), ErrorMessage(String).
        /// </summary>
        public class Result
        {
            public bool Success;
            public int ErrorCode;
            public string ErrorMessage;
        }
    }

    [Serializable]
    public class Email
    {
        public Email()
        {
            this.Attachments = new List<Attachment>();
        }
        public int MessageNumber { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime DateSent { get; set; }
        public List<Attachment> Attachments { get; set; }
    }

    [Serializable]
    public class Attachment
    {
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
    }

    public partial class SA : System.Web.UI.Page
    {
        private const string TonnageTemplate = "{0} - {1} kg";

        private void LoadBankDetails()
        {
            NameoftheBank.Items.Clear();
            NameoftheBank.Items.Add("-- Select Bank --");
            var banksController = new BanksInIndiaController();
            var bankDetails = banksController.Get();
            if (null == bankDetails) return;
            foreach (var banksInIndia in bankDetails)
            {
                NameoftheBank.Items.Add(banksInIndia.BankName);
            }
        }
        private void LoadTonnageDetails()
        {
            tonnageDropdown.Items.Clear();
            tonnageDropdown.Items.Add("-- Select Range --");
            var tonnageController = new TonnageController();
            var tonnageDetails = tonnageController.Get();
            if (null == tonnageDetails) return;
            foreach (var tonnageDetail in tonnageDetails)
            {
                tonnageDropdown.Items.Add(string.Format(CultureInfo.InvariantCulture, TonnageTemplate, tonnageDetail.TonnageFrom, tonnageDetail.TonnageTo));
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //
        }
        //private const string VehicleFileName = "VehicleFileName";
        //private const string VehicleFileBytes = "VehicleFileBytes";

        protected void OnApproveClick(object sender, EventArgs e)
        {
            OnApproveVehicleTicket();
            TicketReject.Visible = false;

        }
        protected void ExportCurrentpage(object sender, CommandEventArgs e)
        {
            if (_commands.ContainsKey(e.CommandArgument.ToString()))
            {
                Exportpage(_commands[e.CommandArgument.ToString()] as GridView);
            }
        }


        private void Exportpage(GridView gridview)
        {
            try
            {
                if (gridview.Rows.Count <= 0) return;
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=GridViewExport.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                using (var sw = new StringWriter())
                {
                    var hw = new HtmlTextWriter(sw);

                    //To Export all pages
                    gridview.AllowPaging = false;
                    gridview.DataBind();

                    gridview.HeaderRow.BackColor = Color.White;
                    foreach (TableCell cell in gridview.HeaderRow.Cells)
                    {
                        cell.BackColor = gridview.HeaderStyle.BackColor;
                    }
                    foreach (GridViewRow row in gridview.Rows)
                    {
                        row.BackColor = Color.White;
                        foreach (TableCell cell in row.Cells)
                        {
                            if (row.RowIndex % 2 == 0)
                            {
                                cell.BackColor = gridview.AlternatingRowStyle.BackColor;
                            }
                            else
                            {
                                cell.BackColor = gridview.RowStyle.BackColor;
                            }
                            cell.CssClass = "textmode";
                        }
                    }

                    gridview.RenderControl(hw);

                    //style to format numbers to string
                    string style = @"<style> .textmode { } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                //throw new Exception(ex.Message);
            }
        }
        private Dictionary<string, object> _commands = new Dictionary<string, object>();
        private void AddCommands()
        {
            _commands.Add("TicketGridView", TicketGridView);
            _commands.Add("pendingGridView", pendingGridView);
        }
        protected void PrintCurrentPage(object sender, CommandEventArgs e)
        {
            if (_commands.ContainsKey(e.CommandArgument.ToString()))
            {
                PrintPage(_commands[e.CommandArgument.ToString()] as GridView);
            }
        }

        private void PrintPage(GridView gridview)
        {
            if (gridview.Rows.Count <= 0) return;
            gridview.PagerSettings.Visible = false;
            gridview.DataBind();
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gridview.RenderControl(hw);
            string gridHTML = sw.ToString().Replace("\"", "'")
                .Replace(System.Environment.NewLine, "");
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            sb.Append(gridHTML);
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            gridview.PagerSettings.Visible = true;
            gridview.DataBind();
        }
        protected void OnRejectClick(object sender, EventArgs e)
        {
            OnApproveVehicleTicket();
            TicketApprove.Visible = false;

        }

        private void OnApproveVehicleTicket()
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected void OnPendingGridChanged(object sender, EventArgs e)
        {
            selected_tab.Value = "pendingTicketList";
            TicketIdHiddenField.Value = pendingGridView.SelectedRow.Cells[1].Text;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalPendingYesNo", "$('#myModalPendingYesNo').modal();", true);
        }

        protected void OnPendingTicketConfirmation(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TicketIdHiddenField.Value)) return;
            var ticketController = new TicketController();
            var ticketDetail = ticketController.Get(TicketIdHiddenField.Value);
            if (null == ticketDetail) return;
            var accountController = new AccountController();
            var accountDetail = accountController.GetByUserId(ticketDetail.UserId);
            var tonnageController = new TonnageController();
            var tonnageDetail = tonnageController.GetByUserId(ticketDetail.UserId);
            if (null == tonnageDetail) return;

            if (accountDetail == null || float.Parse(accountDetail.Funds) <= tonnageDetail.TonnageCost)
            {
                FailureMessage = "Insufficient funds. Please inform respective BA";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            ticketDetail.TicketStatus = Global.UserStatus.Approved.ToString();
            ticketDetail.CreatedDate = (DateTime)(ticketDetail.TicketStartDate = DateTime.Today);
            ticketDetail.TicketEndDate = DateTime.Today.AddMonths(1).AddDays(-1);
            ticketDetail.RenewalCounter = ticketDetail.RenewalCounter == null ? 1 : ticketDetail.RenewalCounter + 1;
            ticketController.Put(ticketDetail);
            if (null == accountDetail.Funds) return;
            var funds = double.Parse(accountDetail.Funds) - tonnageDetail.TonnageCost;
            accountDetail.Funds = funds.ToString();
            accountController.Put(accountDetail);
            TicketIdHiddenField.Value = string.Empty;
            SuccessMessage = "Updated ticket details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            Session.Add("Update", true);
        }

        protected void OnTicketNotificationGridChanged(object sender, EventArgs e)
        {
            var tonnageController = new TonnageController();
            var ticketController = new TicketController();
            var ticketDetail = ticketController.Get(TicketGridView.SelectedRow.Cells[1].Text);
            if (null == ticketDetail) return;
            TicketId.Text = ticketDetail.TicketId;
            TicketGoodsType.Text = ticketDetail.GoodsType;
            var tonnageDetail = tonnageController.Get(ticketDetail.TonnageId);
            TicketTonnage.Text = string.Format(TonnageTemplate, tonnageDetail.TonnageFrom, tonnageDetail.TonnageTo);
            TicketMakeOftheVehicle.Text = ticketDetail.Make;
            TicketModelOftheVehicle.Text = ticketDetail.Model;
            TicketCubicCapacity.Text = ticketDetail.CubicCapacity;
            TicketLengthoftheVehicle.Text = ticketDetail.Length;
            TicketWidthoftheVehicle.Text = ticketDetail.Width;
            TicketBreadthoftheVehicle.Text = ticketDetail.Breadth;
            TicketHeightoftheVehicle.Text = ticketDetail.Height;
            TicketRegistrationNumber.Text = ticketDetail.Registration;
            TicketEngineNumber.Text = ticketDetail.EngineNumber;
            TicketChasisNumber.Text = ticketDetail.ChasisNumber;
            TicketNOCRTO.Checked = bool.Parse(ticketDetail.NOCRTO);
            TicketNOCPolice.Checked = bool.Parse(ticketDetail.NOCPolicy);
            TicketVehiclePhotoFront.ImageUrl =
                string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                ticketDetail.VehiclePhotoFront);
            TicketVehiclePhotoBack.ImageUrl =
                string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                ticketDetail.VehiclePhotoBack);
            TicketVehiclePhotoLeft.ImageUrl =
                string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                ticketDetail.VehiclePhotoLeft);
            TicketVehiclePhotoRight.ImageUrl =
                string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
                ticketDetail.VehiclePhotoRight);
            TicketFCValidityFrom.Text = ticketDetail.FCValidityFrom;
            TicketFCValidity.Text = ticketDetail.FCValidity;
            TicketVehicleOwnerName.Text = ticketDetail.Name;
            TicketAddress.Text = ticketDetail.Address;
            TicketCity.Text = ticketDetail.City;
            TicketDistrict.Text = ticketDetail.District;
            TicketState.Text = ticketDetail.State;
            TicketCountry.Text = ticketDetail.Country;
            TicketInsuranceType.Text = ticketDetail.InsuranceType;
            TicketInsuranceCompany.Text = ticketDetail.InsuranceCompany;
            TicketValidityFrom.Text = ticketDetail.ValidityFrom;
            TicketValidityTo.Text = ticketDetail.Validity;
            TicketDriverName.Text = ticketDetail.DriverName;
            TicketDriverAge.Text = ticketDetail.DriverAge.ToString();
            TicketDriverAddress.Text = ticketDetail.DriverAddress;
            TicketDriverCity.Text = ticketDetail.DriverCity;
            TicketDriverDistrict.Text = ticketDetail.DriverDistrict;
            TicketDriverState.Text = ticketDetail.DriverState;
            TicketDriverCountry.Text = ticketDetail.DriverCountry;
            TicketDriverDLNumber.Text = ticketDetail.DriverDLNumber;
            TicketDriverPhoto.ImageUrl =
                string.Format("~/{0}/{1}/{2}", "VehicleImages", ticketDetail.Registration,
            ticketDetail.DriverPhoto);
            TicketDriverMobileNumber.Text = ticketDetail.DriverMobileNumber;
            TicketDriverEmailId.Text = ticketDetail.DriverEmail;
            TicketDriverPanNumber.Text = ticketDetail.DriverPanCard;
            TicketDriverAadharCardNumber.Text = ticketDetail.DriverAadharCardNumber;
            TicketApprove.Visible = true;
            TicketReject.Visible = true;
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ticketModel", "$('#ticketModel').modal();", true);
        }
        protected string SuccessMessage
        {
            get;
            private set;
        }

        protected string FailureMessage
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture)))
                {
                    var userController = new UserController();
                    var userDetail = userController.Get(User.Identity.Name.ToUpper(CultureInfo.InvariantCulture));
                    if (userDetail.Role != Global.EmployeeType.SuperAdmin)
                        Response.Redirect("~/");
                }
                else
                {
                    Response.Redirect("~/");
                }

                Comments.Text = string.Empty;
                Commisionupdate();
                GetEmployeeTypes();
                GetIndianStates(State);
                LoadPinCodeDetails();
                LoadRTODetails(BARTODropdownList);
                LoadRTODetails(DHRTODropdownList);
                LoadStateDetails();
                LoadDistrictDetails(DHDHDropdownList);
                LoadDistrictDetails(BADHDropdownList);
                LoadTonnageDetails();
                LoadBankDetails();
                if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday || DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                    BAcomission.Enabled = BAtdsCommission.Enabled = false;
                if (DateTime.Today.DayOfWeek.GetHashCode() >= 1 && DateTime.Today.DayOfWeek.GetHashCode() <= 5)
                    DHcomission.Enabled = SHcomission.Enabled = DHtdsCommission.Enabled = SHtdsCommission.Enabled = false;
            }
            else
            {
                selected_tab.Value = Request.Form[selected_tab.UniqueID];
            }

            Title = "Super admin";
            AddCommands();
        }

        private List<Email> Emails
        {
            get { return (List<Email>)ViewState["Emails"]; }
            set { ViewState["Emails"] = value; }
        }

        private void Read_Emails()
        {
            Pop3Client pop3Client;
            if (Session["Pop3Client"] == null)
            {
                pop3Client = new Pop3Client();
                pop3Client.Connect("pop.gmail.com", 995, true);
                pop3Client.Authenticate("nbmanas88@gmail.com", "white_base55");
                Session["Pop3Client"] = pop3Client;
            }
            else
            {
                pop3Client = (Pop3Client)Session["Pop3Client"];
            }
            int count = pop3Client.GetMessageCount();
            Emails = new List<Email>();
            int counter = 0;
            for (int i = count; i >= 1; i--)
            {
                Message message = pop3Client.GetMessage(i);
                Email email = new Email()
                {
                    MessageNumber = i,
                    Subject = message.Headers.Subject,
                    DateSent = message.Headers.DateSent,
                    From = string.Format("<a href = 'mailto:{1}'>{0}</a>", message.Headers.From.DisplayName, message.Headers.From.Address),
                };
                MessagePart body = message.FindFirstHtmlVersion();
                if (body != null)
                {
                    email.Body = body.GetBodyAsText();
                }
                else
                {
                    body = message.FindFirstPlainTextVersion();
                    if (body != null)
                    {
                        email.Body = body.GetBodyAsText();
                    }
                }
                List<MessagePart> attachments = message.FindAllAttachments();

                foreach (MessagePart attachment in attachments)
                {
                    email.Attachments.Add(new Attachment
                    {
                        FileName = attachment.FileName,
                        ContentType = attachment.ContentType.MediaType,
                        Content = attachment.Body
                    });
                }
                Emails.Add(email);
                counter++;
                if (counter > 2)
                {
                    break;
                }
            }
        }


        protected void btnSend_Click(object sender, EventArgs e)
        {
            //Set parameters
            string destinationaddr = txtRecepientNumberc.Text;
            string message = txtMessage.Text;

            CoreSMS(destinationaddr, message);
        }

        private static void CoreSMS(string destinationaddr, string message)
        {
            string msgsender = "+919916226744";
            const string username = "manas.nb@lnttechservices.com";
            string password = "2ttr8";
            // Create ViaNettSMS object with username and password
            var s = new ViaNettSMS(username, password);
            // Declare Result object returned by the SendSMS function
            ViaNettSMS.Result result;
            try
            {
                // Send SMS through HTTP API
                result = s.sendSMS(msgsender, destinationaddr, message);
                // Show Send SMS response
                if (result.Success)
                {
                    Debug.WriteLine("Message successfully sent");
                }
                else
                {
                    Debug.WriteLine("Received error: " + result.ErrorCode + " " + result.ErrorMessage);
                }
            }
            catch (System.Net.WebException ex)
            {
                //Catch error occurred while connecting to server.
                Debug.WriteLine(ex.Message);
            }
        }


        private void UpdateTicketPermissions(bool isRejected, bool isadded = false)
        {
            var ticketController = new TicketController();
            var ticketDetail = ticketController.Get(TicketId.Text);
            ticketDetail.SAComments = Comments.Text;
            if (isRejected)
            {
                ticketDetail.SAPermission = Global.UserStatus.Rejected.ToString();
            }
            else if (isadded)
            {
                var accountController = new AccountController();
                var accountDetail = accountController.GetByUserId(ticketDetail.UserId);
                var tonnageController = new TonnageController();
                var tonnageDetail = tonnageController.GetByUserId(ticketDetail.UserId);
                if (null == tonnageDetail) return;

                if (accountDetail == null || float.Parse(accountDetail.Funds) <= tonnageDetail.TonnageCost)
                {
                    FailureMessage = "Insufficient funds. Please inform respective BA";
                    failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                    return;
                }
                if (null == accountDetail.Funds) return;
                var funds = double.Parse(accountDetail.Funds) - tonnageDetail.TonnageCost;
                accountDetail.Funds = funds.ToString();
                accountController.Put(accountDetail);
                
                ticketDetail.SAPermission = Global.UserStatus.Approved.ToString();
                ticketDetail.TicketStatus = Global.UserStatus.Approved.ToString();
                ticketDetail.CreatedDate = (DateTime)(ticketDetail.TicketStartDate = DateTime.Today);
                ticketDetail.TicketEndDate = DateTime.Today.AddMonths(1).AddDays(-1);
                ticketDetail.RenewalCounter = ticketDetail.RenewalCounter == null ? 1 : ticketDetail.RenewalCounter + 1;
            }
            else
            {
                ticketDetail.SAPermission = Global.UserStatus.Approved.ToString();
                ticketDetail.TicketStatus = Global.UserStatus.Pending.ToString();
            }

            ticketController.Put(ticketDetail);
            SuccessMessage = "Updated ticket details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            Session.Add("Update", true);
        }

        protected void ApprovalComments(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Comments.Text))
            {
                FailureMessage = "Please enter comments";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }

            if (TicketApprove.Visible)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalYesNo", "$('#myModalYesNo').modal();", true);
            }
            else if (TicketReject.Visible)
            {
                UpdateTicketPermissions(true);

            }

            Comments.Text = string.Empty;
        }

        protected void OnConfirmationYesClick(object sender, EventArgs e)
        {
            UpdateTicketPermissions(false, true);
        }

        protected void OnConfirmationNoClick(object sender, EventArgs e)
        {
            UpdateTicketPermissions(false);
        }

        private void Commisionupdate()
        {
            var commissiondetails = new CommissionController();

            var comissionfulDetails = commissiondetails.Get();
            var commissionDetail = comissionfulDetails.FirstOrDefault();

            UpdateBA.Text = commissionDetail.CommissionBA;
            UpdateDH.Text = commissionDetail.CommissionDH;
            UpdateSH.Text = commissionDetail.CommissionSH;
        }

        protected void MessagePanelClick(object sender, EventArgs e)
        {
            ErrorMessage.Text = SuccessMessage = FailureMessage = string.Empty;
            successMessage.Visible = failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
            if (null == Session["Update"] || !bool.Parse(Session["Update"].ToString())) return;
            Session["Update"] = false;
            Response.Redirect("~/Account/SA");
        }

        protected void OnUpdateCommissionDetails(object sender, EventArgs e)
        {

            var commissiondetailscontroller = new CommissionController();
            var vcomissionupDetails1 = commissiondetailscontroller.Get();
            var vcomissionupDetails = vcomissionupDetails1.First();

            vcomissionupDetails.CommissionBA = UpdateBA.Text;
            vcomissionupDetails.CommissionDH = UpdateDH.Text;
            vcomissionupDetails.CommissionSH = UpdateSH.Text;


            commissiondetailscontroller.Put(vcomissionupDetails);
            SuccessMessage = "Updated Commission details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);

        }

        protected void OnUpdaterenewalCost(object sender, EventArgs e)
        {
            if (tonnageDropdown.SelectedIndex <= 0)
            {
                FailureMessage = "Please select range";
                failureMessage.Visible = !string.IsNullOrEmpty(FailureMessage);
            }
            if (string.IsNullOrEmpty(tonnageTextbox.Text))
            {
                FailureMessage = "Please enter amount";
                failureMessage.Visible = !string.IsNullOrEmpty(FailureMessage);
            }
            var tonnageController = new TonnageController();
            var tonnageDetails = tonnageController.Get();
            if (null == tonnageDetails) return;
            var tonnagerange = tonnageDropdown.SelectedValue.Split('-');
            if (tonnagerange.Length > 0)
            {
                var tonnageDetail = tonnageDetails.FirstOrDefault(x => x.TonnageFrom == tonnagerange[0].Trim());
                if (null != tonnageDetail)
                {
                    tonnageDetail.TonnageCost = float.Parse(tonnageTextbox.Text);
                    tonnageController.Put(tonnageDetail);
                }
            }
            SuccessMessage = "Updated tonnage details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
        }

        private void GetEmployeeTypes()
        {
            EmployeeType.Items.Add("--Select Employee Type--");
            EmployeeType.Items.Add(Global.EmployeeType.BusinessAssociate.ToString());
            EmployeeType.Items.Add(Global.EmployeeType.DistrictHead.ToString());
            EmployeeType.Items.Add(Global.EmployeeType.StatetHead.ToString());
        }

        private void GetIndianStates(DropDownList dropDownList)
        {
            dropDownList.Items.Add("--Select State--");
            dropDownList.Items.Add("Andhra Pradesh");
            dropDownList.Items.Add("Arunachal Pradesh");
            dropDownList.Items.Add("Assam");
            dropDownList.Items.Add("Bihar");
            dropDownList.Items.Add("Chhattisgarh");
            dropDownList.Items.Add("Goa");
            dropDownList.Items.Add("Gujarat");
            dropDownList.Items.Add("Haryana");
            dropDownList.Items.Add("Himachal Pradesh");
            dropDownList.Items.Add("Jammu and Kashmir");
            dropDownList.Items.Add("Jharkhand");
            dropDownList.Items.Add("Karnataka");
            dropDownList.Items.Add("Kerala");
            dropDownList.Items.Add("Madhya Pradesh");
            dropDownList.Items.Add("Maharashtra");
            dropDownList.Items.Add("Manipur");
            dropDownList.Items.Add("Meghalaya");
            dropDownList.Items.Add("Mizoram");
            dropDownList.Items.Add("Nagaland");
            dropDownList.Items.Add("Odisha");
            dropDownList.Items.Add("Punjab");
            dropDownList.Items.Add("Rajasthan");
            dropDownList.Items.Add("Sikkim");
            dropDownList.Items.Add("Tamil Nadu");
            dropDownList.Items.Add("Telangana");
            dropDownList.Items.Add("Tripura");
            dropDownList.Items.Add("Uttar Pradesh");
            dropDownList.Items.Add("Uttarakhand");
            dropDownList.Items.Add("West Bengal");
        }

        protected void OnRegister(object sender, EventArgs e)
        {
            var userRepository = new UserRepository();
            var result = userRepository.Get(UserName.Text.ToUpper(CultureInfo.InvariantCulture));
            if (null != result)
            {
                ErrorMessage.Text = "This user name is already taken";
                return;
            }
            PasswordField.Value = Password.Text;
            RegisterPanel.Visible = false;
            EmployeeTypePanel.Visible = true;
        }

        protected void OnAddClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalPartner", "$('#myModalPartner').modal();", true);
        }
        protected void OnDocumentDetailsBack(object sender, EventArgs e)
        {
            DocumentDetailsPanel.Visible = false;
            BankDetailsPanel.Visible = true;
        }
        protected void OnCompanyDetailsBack(object sender, EventArgs e)
        {
            CompanyDetailsPanel.Visible = false;
            PersonalDetailsPanel.Visible = true;
        }
        protected void OnPersonalDetailsBack(object sender, EventArgs e)
        {
            PersonalDetailsPanel.Visible = false;
            EmployeeTypePanel.Visible = true;
        }
        protected void OnBankBack(object sender, EventArgs e)
        {
            BankDetailsPanel.Visible = false;
            CompanyDetailsPanel.Visible = true;
        }
        protected void OnBankNext(object sender, EventArgs e)
        {
            BankDetailsPanel.Visible = false;
            DocumentDetailsPanel.Visible = true;
        }

        protected void OnCompanyDetailsNext(object sender, EventArgs e)
        {
            CompanyDetailsPanel.Visible = false;
            BankDetailsPanel.Visible = true;
        }
        protected void OnDocumentDetailsNext(object sender, EventArgs e)
        {
            Comments.Text = string.Empty;
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalComments", "$('#myModalComments').modal();", true);
        }

        protected void OnRTODetails(object sender, EventArgs e)
        {
            if (BADHDropdownList.SelectedIndex != 0 || BAPinCodeDropdownlist.SelectedIndex != 0 || BARTODropdownList.SelectedIndex != 0) return;
            FailureMessage = "Please select RTO";
            failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        }

        protected void OnDistrictDetails(object sender, EventArgs e)
        {
            if (DHDHDropdownList.SelectedIndex != 0 && DHRTODropdownList.SelectedIndex != 0) return;
            FailureMessage = "Please select district details";
            failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        }
        protected void OnStateDetails(object sender, EventArgs e)
        {
            if (SHDropdownList.SelectedIndex != 0) return;
            FailureMessage = "Please select state";
            failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        }
        protected void OnPartnerDetails(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(PartnerName.Text))
            {
                FailureMessage = "Please enter partner name";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            if (string.IsNullOrEmpty(PartnerAge.Text))
            {
                FailureMessage = "Please enter partner age";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            if (!string.IsNullOrEmpty(PartnerAddress.Text)) return;
            FailureMessage = "Please enter partner address";
            failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        }

        protected void OnApprovalComments(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(userComments.Text))
            {
                FailureMessage = "Please enter comments";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }

            var userDetail = new UserDetail
            {
                Name = UserName.Text.ToUpper(CultureInfo.InvariantCulture),
                Password = PasswordField.Value,
                Status = Global.UserStatus.Approved.ToString()
            };

            var personalDetail = new PersonalDetail
            {
                Name = Name.Text,
                Address = PersonalAddress.Text,
                City = PersonalCity.Text,
                District = PersonalDistrict.Text,
                State = State.Text,
                Country = PersonalCountry.Text,
                Age = Convert.ToInt32(Age.Text),
                EmailId = EmailId.Text,
                MobileNumber = MobileNumber.Text,
                PanCard = PanCard.Text,
                AadharCardNumber = AadharCardNumber.Text,
                EmployeeType = EmployeeType.Text
            };
            var userController = new UserController();
            userDetail.Role = personalDetail.EmployeeType;
            switch (personalDetail.EmployeeType)
            {
                case Global.EmployeeType.BusinessAssociate:
                    {
                        var districtController = new DistrictController();
                        var districtDetails = districtController.Get();
                        if (null != districtDetails)
                        {
                            var districtDetail = districtDetails.Single(x => x.DistrictName == BADHDropdownList.SelectedValue);
                            if (null != districtDetail)
                            {
                                userDetail.DistrictId = districtDetail.DistrictId;
                            }
                        }

                        var rtoController = new RTOController();
                        var rtoDetails = rtoController.Get();
                        if (null != rtoDetails)
                        {
                            var rtoDetail = rtoDetails.Single(x => x.RTOCode == BARTODropdownList.SelectedValue);
                            if (null != rtoDetail)
                            {
                                userDetail.RTOId = rtoDetail.RTOId;
                            }
                        }

                        var pinCodeController = new PinCodeController();
                        var pinCodeDetails = pinCodeController.Get();
                        if (null != pinCodeDetails)
                        {
                            var pinCodeDetail = pinCodeDetails.Single(x => x.PinCode == BAPinCodeDropdownlist.SelectedValue);
                            if (null != pinCodeDetail)
                            {
                                userDetail.PinCodeId = pinCodeDetail.PinCodeId;
                            }
                        }
                        break;
                    }
                case Global.EmployeeType.DistrictHead:
                    {

                        var districtController = new DistrictController();
                        var districtDetails = districtController.Get();
                        if (null != districtDetails)
                        {
                            var districtDetail = districtDetails.Single(x => x.DistrictName == DHDHDropdownList.SelectedValue);
                            if (null != districtDetail)
                            {
                                userDetail.DistrictId = districtDetail.DistrictId;
                            }
                        }

                        var rtoController = new RTOController();
                        var rtoDetails = rtoController.Get();
                        if (null != rtoDetails)
                        {
                            var rtoDetail = rtoDetails.Single(x => x.RTOCode == DHRTODropdownList.SelectedValue);
                            if (null != rtoDetail)
                            {
                                userDetail.RTOId = rtoDetail.RTOId;
                            }
                        }
                        break;
                    }
                case Global.EmployeeType.StatetHead:
                    {

                        var stateController = new StateController();
                        var stateDetails = stateController.Get();
                        if (null != stateDetails)
                        {
                            var districtDetail = stateDetails.Single(x => x.StateName == SHDropdownList.SelectedValue);
                            if (null != districtDetail)
                            {
                                userDetail.StateId = districtDetail.StateId;
                            }
                        }
                        break;
                    }
            }
            userController.Post(userDetail);
            var newUserDetail = userController.Get(userDetail.Name);
            var userId = newUserDetail.UserId;

            var personalController = new PersonalController();
            personalDetail.UserId = userId;
            personalController.Post(personalDetail);
            var companyDetail = new CompanyDetail
            {
                Name = NameoftheFirm.Text,
                FirmType = FirmType.Text,
                TelephoneNumber = TelephoneNumber.Text,
                RegistrationNumber = CompanyRegistrationNumber.Text,
                UserId = userId
            };
            var companyController = new CompanyController();
            companyController.Post(companyDetail);

            if (!string.IsNullOrEmpty(PartnerAddress.Text) || !string.IsNullOrEmpty(PartnerAge.Text) ||
                !string.IsNullOrEmpty(PartnerName.Text))
            {
                var partnerController = new PartnerController();
                var partnerDetail = new PartnerDetail
                {
                    Age = PartnerAge.Text,
                    Name = PartnerName.Text,
                    Address = PartnerAddress.Text,
                    UserId = userDetail.UserId,
                    CompanyId = companyController.CompanyId,
                };
                partnerController.Post(partnerDetail);
                PartnerAddress.Text = PartnerAge.Text = PartnerName.Text = string.Empty;
            }
            var bankDetail = new BankDetail
            {
                Name = NameoftheBank.Text,
                AccountNumber = AccountNumber.Text,
                Address = BankAddress.Text,
                MICRCode = MICRCode.Text,
                IFSCCode = IFSCCode.Text,
                UserId = userId
            };
            var bankController = new BankController();
            bankController.Post(bankDetail);

            var documentDetail = new DocumentDetail
            {
                AadharCardProof = AadhrarCardProof.Checked.ToString(),
                CompanyAddressProof = CompanyAddressProof.Checked.ToString(),
                CompanyRegistration = CompanyRegistrationProof.Checked.ToString(),
                MobileBill = MobileBill.Checked.ToString(),
                OfficeRentalDocument = OfficeRentalDocument.Checked.ToString(),
                PanCardProof = PanCardProof.Checked.ToString(),
                PartnerAddressProof = PartnerAddressProof.Checked.ToString(),
                UserId = userId
            };
            var documentController = new DocumentController();
            documentController.Post(documentDetail);

            AadhrarCardProof.Checked = CompanyAddressProof.Checked = CompanyRegistrationProof.Checked =
                MobileBill.Checked = OfficeRentalDocument.Checked = PanCardProof.Checked = PartnerAddressProof.Checked = false;
            EmployeeType.SelectedIndex = State.SelectedIndex = NameoftheBank.SelectedIndex = 0;
            UserName.Text = PasswordField.Value = Name.Text = PersonalAddress.Text = PersonalCity.Text = PersonalDistrict.Text = PersonalCountry.Text =
                Age.Text = EmailId.Text = MobileNumber.Text = PanCard.Text = AadharCardNumber.Text = NameoftheFirm.Text =
                FirmType.Text = TelephoneNumber.Text = CompanyRegistrationNumber.Text = BankAddress.Text = MICRCode.Text = IFSCCode.Text = string.Empty;
            SuccessMessage = "Updated user details successfully";
            successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            Session.Add("Update", true);
        }

        protected void OnPersonal(object sender, EventArgs e)
        {
            PersonalDetailsPanel.Visible = false;
            CompanyDetailsPanel.Visible = true;
        }

        protected void OnEmployee(object sender, EventArgs e)
        {
            switch (EmployeeType.SelectedValue)
            {
                case Global.EmployeeType.BusinessAssociate:
                    {
                        if (BAPinCodeDropdownlist.SelectedIndex <= 0 || BADHDropdownList.SelectedIndex <= 0 || BARTODropdownList.SelectedIndex <= 0)
                        {
                            FailureMessage = "Please select business associate details";
                            failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalPinCode", "$('#myModalPinCode').modal();", true);
                            return;
                        }
                        break;
                    }
                case Global.EmployeeType.DistrictHead:
                    {
                        if (DHDHDropdownList.SelectedIndex <= 0 || DHRTODropdownList.SelectedIndex <= 0)
                        {
                            FailureMessage = "Please select district details";
                            failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalDistrict", "$('#myModalDistrict').modal();", true);
                            return;
                        }
                        var districtController = new DistrictController();
                        var districtDetail =
                            districtController.Get()
                                .ToList()
                                .FirstOrDefault(x => x.DistrictName == DHDHDropdownList.SelectedValue);
                        if (null != districtDetail)
                        {
                            var rtoController = new RTOController();
                            var rtoDetail =
                                rtoController.Get()
                                    .ToList()
                                    .FirstOrDefault(x => x.RTOCode == DHRTODropdownList.SelectedValue);
                            if (null != rtoDetail)
                            {
                                var userController = new UserController();
                                var userDetail =
                                    userController.Get()
                                        .ToList()
                                        .FirstOrDefault(x => x.DistrictId == districtDetail.DistrictId && x.RTOId == rtoDetail.RTOId);
                                if (null != userDetail)
                                {
                                    FailureMessage = "Already district head available for the selected area";
                                    failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalDistrict", "$('#myModalDistrict').modal();", true);
                                    return;
                                }
                            }
                        }
                        break;
                    }
                case Global.EmployeeType.StatetHead:
                    {
                        if (SHDropdownList.SelectedIndex <= 0)
                        {
                            FailureMessage = "Please select state details";
                            failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalState", "$('#myModalState').modal();", true);
                            return;
                        }
                        var stateController = new StateController();
                        var stateDetail =
                            stateController.Get()
                                .ToList()
                                .FirstOrDefault(x => x.StateName == SHDropdownList.SelectedValue);
                        if (null != stateDetail)
                        {
                            var userController = new UserController();
                            var userDetail =
                                userController.Get()
                                    .ToList()
                                    .FirstOrDefault(x => x.StateId == stateDetail.StateId);
                            if (null != userDetail)
                            {
                                FailureMessage = "Already state head available for the selected state";
                                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalState", "$('#myModalState').modal();", true);
                                return;
                            }
                        }
                        break;
                    }
            }
            PersonalDetailsPanel.Visible = true;
            EmployeeTypePanel.Visible = false;
        }

        #region Create Ticket
        //protected void OnBackInsuranceTicket(object sender, EventArgs e)
        //{
        //    VehicleDetailsPanel.Visible = true;
        //    VehicleInsurancePanel.Visible = false;
        //}
        //protected void OnBackDriverTicket(object sender, EventArgs e)
        //{
        //    VehicleInsurancePanel.Visible = true;
        //    VehicleDriverPanel.Visible = false;
        //}
        //protected void OnCreateDriverTicket(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(DriverPhotoUpload.PostedFile.FileName))
        //    {
        //        ApprovalTicketComments();
        //        return;
        //    }
        //    FailureMessage = "Please upload photo";
        //    failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        //}

        //private void ApprovalTicketComments()
        //{
        //    if (!CanUploadSelectedPhoto(DriverPhotoUpload.PostedFile.FileName))
        //    {
        //        FailureMessage = "Selected photo format is not supported";
        //        failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        //        return;
        //    }
        //    var userController = new UserController();
        //    var newUserDetail = userController.Get(User.Identity.Name);
        //    var userId = newUserDetail.UserId;
        //    var ticketDetail = new TicketDetail
        //    {
        //        UserId = userId,
        //        Make = MakeOftheVehicle.Text,
        //        Model = ModelOftheVehicle.Text,
        //        CubicCapacity = CubicCapacity.Text,
        //        Dimension = DimensionsoftheVehicle.Text,
        //        Registration = RegistrationNumber.Text,
        //        EngineNumber = EngineNumber.Text,
        //        ChasisNumber = ChasisNumber.Text,
        //        NOCRTO = NOCRTO.Checked.ToString(CultureInfo.CurrentUICulture),
        //        NOCPolicy = NOCPolice.Checked.ToString(CultureInfo.CurrentUICulture),
        //        Photo = true.ToString(CultureInfo.InvariantCulture),
        //        PhotoFileName = Session[VehicleFileName] as string,
        //        PhotoType = SelectedPhotoContentType(Session[VehicleFileName] as string),
        //        PhotoContent = Session[VehicleFileBytes] as byte[],
        //        FCValidity = FCValidity.Text,
        //        FCValidityFrom = FCValidityFrom.Text,
        //        Name = VehicleOwnerName.Text,
        //        Address = Address.Text,
        //        City = City.Text,
        //        District = District.Text,
        //        State = VehicleState.Text,
        //        Country = Country.Text,
        //        InsuranceType = InsuranceType.Text,
        //        InsuranceCompany = InsuranceCompany.Text,
        //        Validity = ValidityTo.Text,
        //        ValidityFrom = ValidityFrom.Text,
        //        DriverName = DriverName.Text,
        //        DriverAge = int.Parse(DriverAge.Text),
        //        DriverAddress = DriverAddress.Text,
        //        DriverCity = DriverCity.Text,
        //        DriverDistrict = DriverDistrict.Text,
        //        DriverState = DriverState.Text,
        //        DriverCountry = DriverCountry.Text,
        //        DriverDLNumber = DriverDLNumber.Text,
        //        DriverPhoto = true.ToString(CultureInfo.InvariantCulture),
        //        DriverPhotoFileName = DriverPhotoUpload.PostedFile.FileName,
        //        DriverPhotoType = SelectedPhotoContentType(DriverPhotoUpload.PostedFile.FileName),
        //        DriverPhotoContent = DriverPhotoUpload.FileBytes,
        //        DriverMobileNumber = DriverMobileNumber.Text,
        //        DriverEmail = DriverEmailId.Text,
        //        DriverPanCard = DriverPanNumber.Text,
        //        DriverAadharCardNumber = DriverAadharCardNumber.Text,
        //        VehicleStatus = Global.VehicleStatus.Active.ToString()
        //    };

        //    switch (newUserDetail.Role)
        //    {
        //        case Global.EmployeeType.SuperAdmin:
        //            {
        //                ticketDetail.SAPermission = Global.UserStatus.Approved.ToString();
        //                break;
        //            }
        //    }

        //    var ticketController = new TicketController();
        //    ticketController.Post(ticketDetail);
        //    SuccessMessage = "Ticket submitted successfully";
        //    successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
        //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + "Your Ticket Id is :" + ticketController.TicketId + "');", true);
        //    VehicleDriverPanel.Visible = false;
        //    VehicleDetailsPanel.Visible = true;
        //    //Email-Notiification/

        //    const string email = "itsupport@ltlindia.com";
        //    const string password = "ltlindia@2015";
        //    var txtTo = DriverEmailId.Text;
        //    try
        //    {
        //        using (var mailMessage = new MailMessage(email, txtTo))
        //        {
        //            const string subject = "LTL Message from Admin";
        //            const string body = "Hello customer,\r\n Your vehicle has been disabled\r\n Regards LTL India";
        //            mailMessage.Subject = subject;
        //            mailMessage.Body = body;
        //            mailMessage.IsBodyHtml = false;
        //            var smtp = new SmtpClient { Host = "mail.ltlindia.com", EnableSsl = true };
        //            var networkCred = new NetworkCredential(email, password);
        //            smtp.UseDefaultCredentials = false;
        //            smtp.Credentials = networkCred;
        //            smtp.Port = 465;
        //            smtp.Send(mailMessage);
        //            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        Console.Write("Exception inside the Email Notification is ", ex.Message);

        //    }

        //    VehicleState.SelectedIndex = DriverState.SelectedIndex = 0;
        //    NOCRTO.Checked = NOCPolice.Checked = false;
        //    MakeOftheVehicle.Text = ModelOftheVehicle.Text = CubicCapacity.Text = DimensionsoftheVehicle.Text = RegistrationNumber.Text = EngineNumber.Text = ChasisNumber.Text = FCValidity.Text =
        //        FCValidityFrom.Text = VehicleOwnerName.Text = Address.Text = City.Text = District.Text = Country.Text = InsuranceType.Text =
        //        RegistrationNumber.Text =
        //        InsuranceCompany.Text = ValidityTo.Text = ValidityFrom.Text = DriverName.Text = DriverAge.Text = DriverAddress.Text = DriverCity.Text = DriverDistrict.Text =
        //        DriverCountry.Text = DriverDLNumber.Text = DriverMobileNumber.Text = DriverEmailId.Text = DriverPanNumber.Text = DriverAadharCardNumber.Text = string.Empty;
        //    Session.Add("Update", true);
        //}

        private bool CanUploadSelectedPhoto(string filePath)
        {
            // Read the file and convert it to Byte Array
            var filename = Path.GetFileName(filePath);
            var ext = Path.GetExtension(filename);
            //Set the content type based on File Extension
            switch (ext)
            {
                case ".jpg":
                case ".jpeg":
                case ".png":
                    return true;
            }
            return false;
        }

        private string SelectedPhotoContentType(string filePath)
        {
            // Read the file and convert it to Byte Array
            var filename = Path.GetFileName(filePath);
            var ext = Path.GetExtension(filename);
            //Set the content type based on File Extension
            switch (ext)
            {
                case ".jpg":
                    return ".jpg";
                case ".jpeg":
                    return ".jpeg";
                case ".png":
                    return ".png";
            }
            return string.Empty;
        }

        //protected void OnCreateVehicleTicket(object sender, EventArgs e)
        //{
        //    if (!VehiclePhotoUpload.HasFile)
        //    {
        //        FailureMessage = "Please upload photo";
        //        failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        //        return;
        //    }
        //    if (!CanUploadSelectedPhoto(VehiclePhotoUpload.PostedFile.FileName))
        //    {
        //        FailureMessage = "Selected photo format is not supported";
        //        failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
        //        return;
        //    }
        //    Session.Add(VehicleFileName, VehiclePhotoUpload.PostedFile.FileName);
        //    Session.Add(VehicleFileBytes, VehiclePhotoUpload.FileBytes);
        //    VehicleInsurancePanel.Visible = true;
        //    VehicleDetailsPanel.Visible = false;
        //}

        //protected void OnCreateInsuranceTicket(object sender, EventArgs e)
        //{
        //    VehicleInsurancePanel.Visible = false;
        //    VehicleDriverPanel.Visible = true;
        //}

        #endregion

        private void LoadDistrictDetails(DropDownList dropDownList)
        {
            dropDownList.Items.Clear();
            var districtController = new DistrictController();
            var districtDetails = districtController.Get();
            if (null == districtDetails) return;
            dropDownList.Items.Add("-- Select District --");
            foreach (var districtDetail in districtDetails)
            {
                dropDownList.Items.Add(districtDetail.DistrictName);
            }
        }

        private void LoadRTODetails(DropDownList dropDownList)
        {
            dropDownList.Items.Clear();
            dropDownList.Items.Add("-- Select RTO --");
        }

        private void LoadStateDetails()
        {
            SHDropdownList.Items.Clear();
            var stateController = new StateController();
            var stateDetails = stateController.Get();
            if (null == stateDetails) return;
            SHDropdownList.Items.Add("-- Select State --");
            foreach (var districtDetail in stateDetails)
            {
                SHDropdownList.Items.Add(districtDetail.StateName);
            }
        }

        private void LoadPinCodeDetails()
        {
            BAPinCodeDropdownlist.Items.Clear();
            BAPinCodeDropdownlist.Items.Add("-- Select PinCode --");
        }
        protected void OnMapEmployee(object sender, EventArgs e)
        {
            switch (EmployeeType.SelectedValue)
            {
                case Global.EmployeeType.BusinessAssociate:
                    {
                        BusinessEmployeePanel.Visible = true;
                        DistrictEmployeePanel.Visible = StateEmployeePanel.Visible = false;
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalPinCode", "$('#myModalPinCode').modal();", true);
                        break;
                    }
                case Global.EmployeeType.DistrictHead:
                    {
                        DistrictEmployeePanel.Visible = true;
                        BusinessEmployeePanel.Visible = StateEmployeePanel.Visible = false;
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalDistrict", "$('#myModalDistrict').modal();", true);
                        break;
                    }
                case Global.EmployeeType.StatetHead:
                    {
                        StateEmployeePanel.Visible = true;
                        BusinessEmployeePanel.Visible = DistrictEmployeePanel.Visible = false;
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalState", "$('#myModalState').modal();", true);
                        break;
                    }
                default:
                    {
                        FailureMessage = "Please select employee type and then map";
                        failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                        break;
                    }
            }
        }

        protected void TicketEntityDataSource_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var orders = e.Query.Cast<TicketDetail>();
            var query = from order in orders
                        where order.SAPermission == "Pending"
                        select order;
            e.Query = query;
            if (!query.Any())
            {
                pendingNotificationPanel.Visible = true;
                PrintLinkButton.Visible = false;
                ExportLinkButton.Visible = false;
            }
            else
            {
                pendingNotificationPanel.Visible = false;
            }
        }

        protected void pendingEntityDataSource_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            var orders = e.Query.Cast<TicketDetail>();
            var query = from order in orders
                        where order.TicketStatus == "Pending"
                        select order;
            e.Query = query;
            if (query.Any()) return;
            PrintLinkPendingButton.Visible = false;
            ExportLinkPendingButton.Visible = false;
        }

        protected void DHDHDropdownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRTODetails(DHRTODropdownList);
            var districtController = new DistrictController();
            var districtDetail =
                districtController.Get().ToList().FirstOrDefault(x => x.DistrictName == DHDHDropdownList.SelectedValue);
            if (null == districtDetail) return;
            var rtoController = new RTOController();
            var rtoDetails = rtoController.Get().ToList().Where(x => x.DistrictId == districtDetail.DistrictId);
            if (null == rtoDetails) return;
            foreach (var rtoDetail in rtoDetails)
            {
                DHRTODropdownList.Items.Add(rtoDetail.RTOCode);
            }
        }

        protected void BADHDropdownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRTODetails(BARTODropdownList);
            var districtController = new DistrictController();
            var districtDetail =
                districtController.Get().ToList().FirstOrDefault(x => x.DistrictName == BADHDropdownList.SelectedValue);
            if (null == districtDetail) return;
            var rtoController = new RTOController();
            var rtoDetails = rtoController.Get().ToList().Where(x => x.DistrictId == districtDetail.DistrictId);
            if (null == rtoDetails) return;
            foreach (var rtoDetail in rtoDetails)
            {
                BARTODropdownList.Items.Add(rtoDetail.RTOCode);
            }
        }

        protected void BARTODropdownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var rtoController = new RTOController();
            var rtoDetails = rtoController.Get().ToList().FirstOrDefault(x => x.RTOCode == BARTODropdownList.SelectedValue);
            if (null == rtoDetails) return;
            var pinCodeController = new PinCodeController();
            var pinCodeDetails = pinCodeController.Get().ToList().Where(x => x.RTOId == rtoDetails.RTOId);
            if (null == pinCodeDetails) return;
            foreach (var pinCodeDetail in pinCodeDetails)
            {
                BAPinCodeDropdownlist.Items.Add(pinCodeDetail.PinCode);
            }
        }

        protected void tonnageDropdown_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (tonnageDropdown.SelectedIndex <= 0)
            {
                FailureMessage = "Please select range";
                failureMessage.Visible = !string.IsNullOrEmpty(FailureMessage);
                tonnageTextbox.Text = string.Empty;
                return;
            }
            var tonnageController = new TonnageController();
            var tonnageDetails = tonnageController.Get();
            if (null == tonnageDetails) return;
            var tonnagerange = tonnageDropdown.SelectedValue.Split('-');
            if (tonnagerange.Length <= 0) return;
            var tonnageDetail = tonnageDetails.FirstOrDefault(x => x.TonnageFrom == tonnagerange[0].Trim());
            if (null != tonnageDetail)
            {
                tonnageTextbox.Text = tonnageDetail.TonnageCost.ToString();
            }
        }

        protected void GenerateSHcomission(object sender, EventArgs e)
        {
            var commissionControllers = new CommissionController();
            var commissionDetails = commissionControllers.Get();
            if (null == commissionDetails) return;
            var dhCommission = string.Empty;
            foreach (var commissionDetail in commissionDetails)
            {
                dhCommission = commissionDetail.CommissionSH;
            }
            var ticketController = new TicketController();
            var ticketDetails = ticketController.Get();
            if (null == ticketDetails) return;
            var filename = "SH Settlement.txt";
            var filePath = HttpContext.Current.Server.MapPath("~");
            filename = filePath + filename;
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            var counter = 1;
            var isCommission = false;
            var header = false;
            var headerline = "HEADERFILE";
            headerline = headerline + "|" + Global.EmployeeType.DistrictHead + "|" + counter + "|" + "Y" + "|" + "SHSettlement";
            foreach (var ticketDetail in ticketDetails)
            {
                var dhDay = new DateTime();
                if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                    dhDay = DateTime.Today.AddDays(-7);
                if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                    dhDay = DateTime.Today.AddDays(-8);
                if (ticketDetail.TicketStartDate == null) continue;
                if (ticketDetail.TicketStartDate.Value < dhDay) continue;
                var tonnageController = new TonnageController();
                var tonnageDetails = tonnageController.Get();
                if (null == tonnageDetails) continue;
                foreach (var tonnageDetail in tonnageDetails)
                {
                    if (tonnageDetail.TonnageId != ticketDetail.TonnageId) continue;
                    counter += counter;
                    var bankcontroller = new BankController();
                    var bankdetails = bankcontroller.Get();
                    var enumerable = bankdetails as BankDetail[] ?? bankdetails.ToArray();
                    var detail = ticketDetail;
                    foreach (var bankdetail in enumerable.Where(bankdetail => bankdetail.UserId == detail.UserId))
                    {
                        using (var file = new StreamWriter(filename, true))
                        {
                            isCommission = true;
                            if (!header)
                            {
                                header = true;
                                file.WriteLine(headerline);
                            }
                            var transferamount = tonnageDetail.TonnageCost * double.Parse(dhCommission);
                            transferamount = transferamount * 0.9;
                            const string currency = "INR";
                            const string tranremarks = "Transaction";
                            //var payifsc = bankdetail.IFSCCode;
                            //var payeeaccoutno = bankdetail.AccountNumber;
                            var ifsccode = bankdetail.IFSCCode;
                            var accountnumber = bankdetail.AccountNumber;
                            var personalcontroller = new PersonalController();
                            var personaldetails = personalcontroller.GetByUserId(bankdetail.UserId);
                            var beneficaryname = personaldetails.Name;
                            var useremailid = personaldetails.EmailId;
                            var userphone = personaldetails.MobileNumber;
                            var line = "" + "NEFT" + "|" + payifsc + "|" + payeeaccoutno + "|" + ifsccode + "|" + accountnumber + "|" +
                                       currency + "|" + transferamount + "|" + tranremarks + "|" + beneficaryname + "|" + useremailid + "|" + userphone;
                            file.WriteLine(line);
                        }
                    }
                }
            }
            if (!isCommission)
            {
                FailureMessage = "No commission available";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            Response.ContentType = "text/plain";
            Response.AppendHeader("Content-Disposition", "attachment; filename=SH Settlement.txt");
            // specify the path of the file that has to be saved in client machine.
            Response.TransmitFile(filename);
            Response.End();
        }
        private const string payifsc = "LTLIFSCCODE";
        private const string payeeaccoutno = "1234567890123456";

        protected void GenerateDHcomission(object sender, EventArgs e)
        {
            var commissionControllers = new CommissionController();
            var commissionDetails = commissionControllers.Get();
            if (null == commissionDetails) return;
            var dhCommission = string.Empty;
            foreach (var commissionDetail in commissionDetails)
            {
                dhCommission = commissionDetail.CommissionDH;
            }
            var ticketController = new TicketController();
            var ticketDetails = ticketController.Get();
            if (null == ticketDetails) return;
            var filename = "DH Settlement.txt";
            var filePath = HttpContext.Current.Server.MapPath("~");
            filename = filePath + filename;
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            var counter = 1;
            var isCommission = false;
            var header = false;
            var headerline = "HEADERFILE";
            headerline = headerline + "|" + Global.EmployeeType.DistrictHead + "|" + counter + "|" + "Y" + "|" + "DHSettlement";
            foreach (var ticketDetail in ticketDetails)
            {
                var dhDay = new DateTime();
                if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                    dhDay = DateTime.Today.AddDays(-7);
                if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                    dhDay = DateTime.Today.AddDays(-8);
                if (ticketDetail.TicketStartDate == null) continue;
                if (ticketDetail.TicketStartDate.Value < dhDay) continue;
                var tonnageController = new TonnageController();
                var tonnageDetails = tonnageController.Get();
                if (null == tonnageDetails) continue;
                
                foreach (var tonnageDetail in tonnageDetails)
                {
                    if (tonnageDetail.TonnageId != ticketDetail.TonnageId) continue;

                    counter += counter;
                    var bankcontroller = new BankController();
                    var bankdetails = bankcontroller.Get();
                    var enumerable = bankdetails as BankDetail[] ?? bankdetails.ToArray();
                    var detail = ticketDetail;
                    foreach (var bankdetail in enumerable.Where(bankdetail => bankdetail.UserId == detail.UserId))
                    {
                        using (var file = new StreamWriter(filename, true))
                        {
                            isCommission = true;
                            if (!header)
                            {
                                header = true;
                                file.WriteLine(headerline);
                            }
                            var transferamount = tonnageDetail.TonnageCost * double.Parse(dhCommission);
                            transferamount = transferamount * 0.9;
                            const string currency = "INR";
                            const string tranremarks = "Transaction";
                            //var payifsc = bankdetail.IFSCCode;
                            //var payeeaccoutno = bankdetail.AccountNumber;
                            var ifsccode = bankdetail.IFSCCode;
                            var accountnumber = bankdetail.AccountNumber;
                            var personalcontroller = new PersonalController();
                            var personaldetails = personalcontroller.GetByUserId(bankdetail.UserId);
                            var beneficaryname = personaldetails.Name;
                            var useremailid = personaldetails.EmailId;
                            var userphone = personaldetails.MobileNumber;
                            var line = "" + "NEFT" + "|" + payifsc + "|" + payeeaccoutno + "|" + ifsccode + "|" + accountnumber + "|" +
                                       currency + "|" + transferamount + "|" + tranremarks + "|" + beneficaryname + "|" + useremailid + "|" + userphone;
                            file.WriteLine(line);
                        }
                    }
                }
            }
            if (!isCommission)
            {
                FailureMessage = "No commission available";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            Response.ContentType = "text/plain";
            Response.AppendHeader("Content-Disposition", "attachment; filename=DH Settlement.txt");
            // specify the path of the file that has to be saved in client machine.
            Response.TransmitFile(filename);
            Response.End();
        }

        protected void GenerateBAcomission(object sender, EventArgs e)
        {
            var commissionControllers = new CommissionController();
            var commissionDetails = commissionControllers.Get();
            if (null == commissionDetails) return;
            var baCommission = string.Empty;
            foreach (var commissionDetail in commissionDetails)
            {
                baCommission = commissionDetail.CommissionBA;
            }
            var ticketController = new TicketController();
            var ticketDetails = ticketController.Get();
            if (null == ticketDetails) return;
            var filename = "BA Settlement.txt";
            var filePath = HttpContext.Current.Server.MapPath("~");
            filename = filePath + filename;
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            var counter = 1;
            var isCommission = false;
            var header = false;
            var headerline = "HEADERFILE";
            headerline = headerline + "|" + Global.EmployeeType.DistrictHead + "|" + counter + "|" + "Y" + "|" + "BASettlement";
            foreach (var ticketDetail in ticketDetails)
            {
                DateTime yesterday = DateTime.Today.AddDays(-1);
                if (ticketDetail.TicketStartDate == null) continue;
                if (ticketDetail.TicketStartDate.Value != yesterday) continue;
                var tonnageController = new TonnageController();
                var tonnageDetails = tonnageController.Get();
                if (null == tonnageDetails) continue;
                foreach (var tonnageDetail in tonnageDetails)
                {
                    if (tonnageDetail.TonnageId != ticketDetail.TonnageId) continue;
                     counter += counter;
                    var bankcontroller = new BankController();
                    var bankdetails = bankcontroller.Get();
                    var enumerable = bankdetails as BankDetail[] ?? bankdetails.ToArray();
                    var detail = ticketDetail;
                    foreach (var bankdetail in enumerable.Where(bankdetail => bankdetail.UserId == detail.UserId))
                    {
                        using (var file = new StreamWriter(filename, true))
                        {
                            isCommission = true;
                            if (!header)
                            {
                                header = true;
                                file.WriteLine(headerline);
                            }
                            var transferamount = tonnageDetail.TonnageCost * double.Parse(baCommission);
                            transferamount = transferamount * 0.9;
                            const string currency = "INR";
                            const string tranremarks = "Transaction";
                            //var payifsc = bankdetail.IFSCCode;
                            //var payeeaccoutno = bankdetail.AccountNumber;
                            var ifsccode = bankdetail.IFSCCode;
                            var accountnumber = bankdetail.AccountNumber;
                            var personalcontroller = new PersonalController();
                            var personaldetails = personalcontroller.GetByUserId(bankdetail.UserId);
                            var beneficaryname = personaldetails.Name;
                            var useremailid = personaldetails.EmailId;
                            var userphone = personaldetails.MobileNumber;
                            var line = "" + "NEFT" + "|" + payifsc + "|" + payeeaccoutno + "|" + ifsccode + "|" + accountnumber + "|" +
                                       currency + "|" + transferamount + "|" + tranremarks + "|" + beneficaryname + "|" + useremailid + "|" + userphone;
                            file.WriteLine(line);
                        }
                    }
                }
            }
            if (!isCommission)
            {
                FailureMessage = "No commission available";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            Response.ContentType = "text/plain";
            Response.AppendHeader("Content-Disposition", "attachment; filename=BA Settlement.txt");
            // specify the path of the file that has to be saved in client machine.
            Response.TransmitFile(filename);
            Response.End();
        }

        protected void GenerateBATDScomission(object sender, EventArgs e)
        {
            var commissionControllers = new CommissionController();
            var commissionDetails = commissionControllers.Get();
            if (null == commissionDetails) return;

            var baCommission = string.Empty;
            foreach (var commissionDetail in commissionDetails)
            {
                baCommission = commissionDetail.CommissionBA;
            }
            var ticketController = new TicketController();
            var ticketDetails = ticketController.Get();
            if (null == ticketDetails) return;
            var filename = "BA TDS.txt";
            var filePath = HttpContext.Current.Server.MapPath("~");
            filename = filePath + filename;
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            var counter = 1;
            var isCommission = false;
            foreach (var ticketDetail in ticketDetails)
            {
                var personalController = new PersonalController();
                var personalDetail = personalController.GetByUserId(ticketDetail.UserId);
                DateTime yesterday = DateTime.Today.AddDays(-1);
                if (ticketDetail.TicketStartDate == null) continue;
                if (ticketDetail.TicketStartDate.Value != yesterday) continue;
                var tonnageController = new TonnageController();
                var tonnageDetails = tonnageController.Get();
                if (null == tonnageDetails) continue;
                foreach (var tonnageDetail in tonnageDetails)
                {
                    if (tonnageDetail.TonnageId != ticketDetail.TonnageId) continue;
                    var headerline = personalDetail.Name;
                    headerline = headerline + "|" + Global.EmployeeType.BusinessAssociate + "|" + "Amount" + "|";
                    counter += counter;

                    using (var file = new StreamWriter(filename, true))
                    {
                        isCommission = true;
                        file.WriteLine(headerline);
                        var transferamount = tonnageDetail.TonnageCost * double.Parse(baCommission);
                        transferamount = transferamount * 0.1;
                        file.WriteLine(headerline + transferamount);
                    }

                }
            }
            if (!isCommission)
            {
                FailureMessage = "No commission available";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            Response.ContentType = "text/plain";
            Response.AppendHeader("Content-Disposition", "attachment; filename=BA TDS.txt");
            // specify the path of the file that has to be saved in client machine.
            Response.TransmitFile(filename);
            Response.End();
        }

        protected void GenerateDHTDScomission(object sender, EventArgs e)
        {
            var commissionControllers = new CommissionController();
            var commissionDetails = commissionControllers.Get();
            if (null == commissionDetails) return;

            var baCommission = string.Empty;
            foreach (var commissionDetail in commissionDetails)
            {
                baCommission = commissionDetail.CommissionDH;
            }
            var ticketController = new TicketController();
            var ticketDetails = ticketController.Get();
            if (null == ticketDetails) return;
            var filename = "DH TDS.txt";
            var filePath = HttpContext.Current.Server.MapPath("~");
            filename = filePath + filename;
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            var isCommission = false;
            var counter = 1;
            foreach (var ticketDetail in ticketDetails)
            {
                var personalController = new PersonalController();
                var personalDetail = personalController.GetByUserId(ticketDetail.UserId);
                var dhDay = new DateTime();
                if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                    dhDay = DateTime.Today.AddDays(-7);
                if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                    dhDay = DateTime.Today.AddDays(-8);
                if (ticketDetail.TicketStartDate == null) continue;
                if (ticketDetail.TicketStartDate.Value < dhDay) continue;
                var tonnageController = new TonnageController();
                var tonnageDetails = tonnageController.Get();
                if (null == tonnageDetails) continue;
                foreach (var tonnageDetail in tonnageDetails)
                {
                    if (tonnageDetail.TonnageId != ticketDetail.TonnageId) continue;
                    var headerline = personalDetail.Name;
                    headerline = headerline + "|" + Global.EmployeeType.DistrictHead + "|" + "Amount" + "|";
                    counter += counter;
                    using (var file = new StreamWriter(filename, true))
                    {
                        isCommission = true;
                        file.WriteLine(headerline);
                        var transferamount = tonnageDetail.TonnageCost * double.Parse(baCommission);
                        transferamount = transferamount * 0.1;
                        file.WriteLine(headerline + transferamount);
                    }

                }
            }
            if (!isCommission)
            {
                FailureMessage = "No commission available";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            Response.ContentType = "text/plain";
            Response.AppendHeader("Content-Disposition", "attachment; filename=DH TDS.txt");
            // specify the path of the file that has to be saved in client machine.
            Response.TransmitFile(filename);
            Response.End();
        }

        protected void GenerateSHTDScomission(object sender, EventArgs e)
        {
            var commissionControllers = new CommissionController();
            var commissionDetails = commissionControllers.Get();
            if (null == commissionDetails) return;

            var baCommission = string.Empty;
            foreach (var commissionDetail in commissionDetails)
            {
                baCommission = commissionDetail.CommissionDH;
            }
            var ticketController = new TicketController();
            var ticketDetails = ticketController.Get();
            if (null == ticketDetails) return;
            var filename = "SH TDS.txt";
            var filePath = HttpContext.Current.Server.MapPath("~");
            filename = filePath + filename;
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            var counter = 1;
            var isCommission = false;
            foreach (var ticketDetail in ticketDetails)
            {
                var personalController = new PersonalController();
                var personalDetail = personalController.GetByUserId(ticketDetail.UserId);
                var dhDay = new DateTime();
                if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                    dhDay = DateTime.Today.AddDays(-7);
                if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                    dhDay = DateTime.Today.AddDays(-8);
                if (ticketDetail.TicketStartDate == null) continue;
                if (ticketDetail.TicketStartDate.Value < dhDay) continue;
                var tonnageController = new TonnageController();
                var tonnageDetails = tonnageController.Get();
                if (null == tonnageDetails) continue;
                foreach (var tonnageDetail in tonnageDetails)
                {
                    if (tonnageDetail.TonnageId != ticketDetail.TonnageId) continue;
                    var headerline = personalDetail.Name;
                    headerline = headerline + "|" + Global.EmployeeType.StatetHead + "|" + "Amount" + "|";
                    counter += counter;
                    using (var file = new StreamWriter(filename, true))
                    {
                        isCommission = true;
                        file.WriteLine(headerline);
                        var transferamount = tonnageDetail.TonnageCost * double.Parse(baCommission);
                        transferamount = transferamount * 0.1;
                        file.WriteLine(headerline + transferamount);
                    }

                }
            }
            if (!isCommission)
            {
                FailureMessage = "No commission available";
                failureMessage.Visible = !String.IsNullOrEmpty(FailureMessage);
                return;
            }
            Response.ContentType = "text/plain";
            Response.AppendHeader("Content-Disposition", "attachment; filename=SH TDS.txt");
            // specify the path of the file that has to be saved in client machine.
            Response.TransmitFile(filename);
            Response.End();
        }
    }
}