﻿using System;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;

namespace Dashboard
{
    public class Global : HttpApplication
    {
        public enum UserStatus
        {
            Pending,
            Approved,
            Rejected
        }
        public struct EmployeeType
        {
            public const string BusinessAssociate = "Business Associate";
            public const string DistrictHead = "District Head";
            public const string StatetHead = "State Head";
            public const string SuperAdmin = "Super Admin";
        }
        public enum VehicleStatus
        {
            Active,
            Disabled,
        }
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //RouteTable.Routes.MapHttpRoute(
            //name: "DefaultApi",
            //routeTemplate: "api/{controller}/{id}",
            //defaults: new { id = System.Web.Http.RouteParameter.Optional }
            //);
        }
    }
}